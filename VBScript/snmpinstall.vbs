================
:: READ THIS FIRST
:: ================
:: * To run this script you must have domain administrators rights.
:: * This script require "PSExec.exe" file (comes with PSTools) on C: drive root
:: * This script require "Computers.txt" file on C: drive root from where it will pick computer names.
:: * To run this script you need to share Windows installation CD.
:: Insert Windows CD -> My Computer -> Right click on CD Drive -> Sharing and Security -> New Share -> Give share name like: W2K3 -> OK -> OK
:: * Following inputs are required to run the script successfully
:: SET i386Share=\\SERVER\W2K3
:: SET DriveLetter=W: <- Make sure that on remote system this drive letter should not be already in use
:: SET userName=MyDomain\Administrator *OR* MachineName\Administrator *OR* Administrator
:: SET password=P@ssW0rd
:: SET snmpContact=Farhan Kazi <- Contact Name
:: SET snmpLocation=IT,Building II <- Location on system
:: SET snmpCString=public <-- SNMP Community string
:: SET snmpTDst=172.16.96.11 <-- SNMP Trap Destination IP
:: * No restart required on remote system
:: * Successful run will generate "C:\SNMPReport.txt" file on C: drive root.
:: * Copy and Paste following script into notepad and save it with any name having .bat extension.
:: Batch Script Start

@Echo Off
SETLOCAL
SET i386Share=\\SERVER\W2K3
SET driveLetter=W:
SET userName=MyDomain\Administrator
SET password=P@ssW0rd
SET snmpContact=Farhan Kazi
SET snmpLocation=IT,Building II
SET snmpCString=public
SET snmpTDst=172.16.96.11

IF NOT EXIST C:\Computers.txt Goto ShowErr
FOR %%R IN (C:\Computers.txt) Do IF %%~zR EQU 0 Goto ShowErr
IF EXIST C:\SNMPReport.txt DEL /F /Q C:\SNMPReport.txt

(
Echo Windows Registry Editor Version 5.00
Echo.
Echo [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\Curr entVersion\Setup]
Echo "SourcePath"="%DriveLetter%\\"
Echo.
Echo [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Servic es\SNMP\Parameters\TrapConfiguration\%snmpCString%]
Echo "1"="%snmpTDst%"
Echo.
Echo [HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\SNMP\Parameters\ValidCommunities]
Echo "%snmpCString%"=dword:00000004
Echo.
Echo [HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Services\SNMP\Parameters\RFC1156Agent]
Echo "sysContact"="%snmpContact%"
Echo "sysLocation"="%snmpLocation%"
Echo "sysServices"=dword:0000004d
)>C:\SNMPSetup.reg

IF NOT EXIST C:\SNMPSetup.reg (
Echo Unable to create Proxy.reg file on C: drive root. 
Goto :EndScript
)

(
Echo ;SetupMgrTag
Echo [NetOptionalComponents]
Echo SNMP=1
Echo [SNMP]
Echo Any_Host=YES
)> C:\SNMP.ua

IF NOT EXIST C:\SNMP.ua (
Echo Unable to create SNMP.ua file on C: drive root.
Goto :EndScript
)

FOR /F "delims=#" %%c IN ('Type Computers.txt') Do (
Echo Processing: %%c
Echo Please wait....
Echo.
Echo ---------------------------- >>C:\SNMPReport.txt
Echo Computer Name: %%c >>C:\SNMPReport.txt
Echo ---------------------------- >>C:\SNMPReport.txt
Copy /Y C:\SNMPSetup.reg \\%%c\C$\ >>C:\SNMPReport.txt
Copy /Y C:\SNMP.ua \\%%c\C$\ >>C:\SNMPReport.txt
PSExec \\%%c -s Net Use %driveLetter% %i386Share% /USER:%userName% %password% /PERSISTENT:NO
PSExec \\%%c -s -i Regedit /S C:\SNMPSetup.reg >>C:\SNMPReport.txt
PSExec \\%%c -s -i sysocmgr /i:%windir%\inf\sysoc.inf /u:C:\SNMP.ua /x /q >>C:\SNMPReport.txt
PSExec \\%%c -s Net Use %driveLetter% /DELETE
DEL /Q /F \\%%c\C$\SNMPSetup.reg
DEL /Q /F \\%%c\C$\SNMP.ua
) 

Goto EndScript
:ShowErr
Echo "C:\Computers.txt" file does not exist or file is empty!
:EndScript
IF EXIST C:\SNMPSetup.reg DEL /F /Q C:\SNMPSetup.reg
IF EXIST C:\SNMP.ua DEL /F /Q C:\SNMP.ua
ENDLOCAL
:: Batch Script End