

'******************************************************************************
'Constant variables
'******************************************************************************
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNTDISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32
Const ADS_PROPERTY_CLEAR = 1
Const ADS_SECURE_AUTHENTICATION = 1
Const ADS_USE_ENCRYPTION = 2


'******************************************************************************
'Global variables
'******************************************************************************
Dim strPassword
Dim RndPassword
Dim userObj
Dim duplicateEmail
Dim sDN
Dim sRoot
Dim sUser
Dim dso
Dim oAuth
Dim oConn
Dim usrObj


duplicateEmail = 0
Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1



'Set InPutFile = FileSystem.OpenTextFile("C:\Scratch\VBScript\delegates.csv",ForReading, False)
'Set outputFile = FileSystem.CreateTextFile("C:\Scratch\VBScript\ldapQuery.txt",True)


'******************************************************************************
'User credentials
'******************************************************************************
sUser = "us\mchirchir_eu"
'ldapmasterQAT sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
sDN = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://ldapmaster.umuc.edu/dc=umuc,dc=edu"
Password("Password:")

'******************************************************************************
'Get User Input
'******************************************************************************
main()
WScript.Echo "Finished"

'******************************************************************************
'Main Function
'******************************************************************************
Function main()

Set dso = GetObject("LDAP:")
Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword

Set ofs = createobject("scripting.filesystemobject")
Set inputFile = ofs.opentextfile("c:\Scratch\VBScript\umucguid.txt" , 1, false)

'Run the main logic
While not inputFile.AtEndOfStream
	useriden = inputFile.ReadLine()

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & useriden & ";uid,umucEmplID,umucGUID,eduPersonPrimaryAffiliation;subtree")


For Each Element In oRS.Fields(0).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(1).Value

usrObj.ldapGUID = oRS.Fields(2).Value


usrObj.personPrimaryAffiliation = oRS.Fields(3).Value


Wend

End Function

'******************************************************************************
'Get User Password
'******************************************************************************
Function Password(myPrompt)
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = Nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = Nothing
End Function

'******************************************************************************
'Populate user object class
'******************************************************************************
Function ldapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucGUID,eduPersonPrimaryAffiliation;subtree")


For Each Element In oRS.Fields(0).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(1).Value
'WScript.Echo usrObj.emplID

usrObj.ldapGUID = oRS.Fields(2).Value
'WScript.Echo usrObj.ldapGUID

usrObj.personPrimaryAffiliation = oRS.Fields(3).Value
'WScript.Echo usrObj.personPrimaryAffiliation

End Function