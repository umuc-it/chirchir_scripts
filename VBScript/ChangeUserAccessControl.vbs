' List userAccountControl Values for an Active Directory User Account
Const ADS_UF_ACCOUNT_DISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32

Set objHash = CreateObject("Scripting.Dictionary") 

objHash.Add "PASSWD_NOTREQD", &h20 
objHash.Add "PASSWD_CANT_CHANGE", &h40 
objHash.Add "NORMAL_ACCOUNT", &h200 
objHash.Add "DONT_EXPIRE_PASSWORD", &h10000 
objHash.Add "ADS_UF_SMARTCARD_REQUIRED", &h40000  
objHash.Add "ADS_UF_TRUSTED_FOR_DELEGATION", &h80000  
objHash.Add "ADS_UF_NOT_DELEGATED", &h100000  
objHash.Add "ADS_UF_USE_DES_KEY_ONLY", &h200000  
objHash.Add "ADS_UF_DONT_REQUIRE_PREAUTH", &h400000  
  
  
Set ofs = createobject("scripting.filesystemobject")

Set inputFile = ofs.opentextfile("C:\Scratch\VBScript\user.txt" , 1, false)

While not inputFile.atEndOfStream 
  userName= inputFile.ReadLine()

Set objUser = GetObject _ 
    ("LDAP://adedcns02.us.umuc.edu/CN="& userName &",OU=People,DC=us,DC=umuc,DC=edu")
intUAC = objUser.Get("userAccountControl")
 
If intUAC = 544 Then
	intUAC = 512
	Wscript.Echo username
	objUser.put "userAccountControl", intUAC And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
	objUser.SetInfo
Else
    Wscript.Echo "Normal Account: " &intUAC
End If
Wend

