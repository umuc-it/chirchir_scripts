'ldapauth.vbs
'Version: 1.0
'Author: Joe Gasper
'Use:  c:\>cscript ldapauth.vbs GatorLinkName [GatorLinkPassword]
'Ex - Anonymous:       c:\>cscript ldapauth.vbs gasperj
'Ex - Authenticated:   c:\>cscript ldapauth.vbs gasperj mypassword

Dim oUser      'LDAP object holding user info
Dim oDSP       'Directory Service Provider
Dim oArgs      'Command line arguments
Dim sCN        'search parameter - LDAP attribute: CN
Dim sPWD       'CN's password parameter
Dim sRoot      'Holds the root of the LDAP object
Dim sDN        'Distinguished Name of authenticating account
Dim sLDAPsrv   'LDAP server
Dim sLDAPsb    'LDAP search base
Dim bAuthQuery 'Query Type - True=Authenticated, False=Anonymous

CONST ADS_SECURE_AUTHENTICATION = &H0001
CONST ADS_USE_ENCRYPTION = &H0002
CONST ADS_USE_SSL = &H0002
CONST ADS_READONLY_SERVER = &H0004
CONST ADS_NO_AUTHENTICATION = &H0010
CONST ADS_FAST_BIND = &H0020
CONST ADS_USE_SIGNING = &H0040
CONST ADS_USE_SEALING = &H0080
CONST ADS_USE_DELEGATION = &H0100
CONST ADS_SERVER_BIND = &H0200

sLDAPsrv = "ldap.umuc.edu"
sLDAPsb = "ou=People,dc=umuc,dc=edu"

'Get the command line args
set oArgs=WScript.Arguments

'Check command line args
On Error Resume Next
sCN = oArgs.item(0)    'UMUC username
If Err.Number <> 0 Then
    Echo ""
    Echo "**** ERROR: No UMUC username supplied."
    Echo ""
    Echo "Use: c:\>cscript ldapauth.vbs GatorLinkName [GatorLinkPassword]"
    Echo ""
    Echo "Aborting..."
    Echo ""
    WScript.Quit
End If

sRoot = "LDAP://" & sLDAPsrv & "/cn=" & sCN & "," & sLDAPsb
sDN = "cn=" & sCN & "," & sLDAPsb

On Error Resume Next
sPWD = oArgs.item(1)   'UMUC password
If Err.Number <> 0 Then   'This will be a non-authenticated query
    bAuthQuery = False
    Echo ""
    Echo "Performing anonymous LDAP query..."
    Echo ""
Else    'This will be an authenticated query
    bAuthQuery = True
    Echo ""
    Echo "Performing authenticated LDAP query..."
    Echo ""
End If
'Done checking command line args

'Set directory service provider
Set oDSP = GetObject("LDAP:")

'Perform requested type of query - anonymous or authenticated
If bAuthQuery Then   'authenticated query requested

    'Set the LDAP object query
    On Error Resume Next
    Set oUser = oDSP.OpenDSObject(sRoot,sDN,sPWD,ADS_SERVER_BIND)
    If Err.Number <> 0 Then
        If Err.Number = "-2147023570" Then
            Echo "**** ERROR: Authentication failed. Check username, password and search base."  
        ElseIf Err.Number = "-2147016646" Then
            Echo "**** ERROR: LDAP server not found."  
        Else 
            Echo "**** ERROR: Unable to bind to LDAP server. " & Err.Number
        End If
        Echo ""
        Echo "Use: c:\>cscript ldapauth.vbs GatorLinkName GatorLinkPassword"
        Echo ""
        Echo "Aborting..."
        Echo ""
        WScript.Quit
    End If

Else                 'anonymous query requested

    'Set the LDAP object query
    On Error Resume Next
    Set oUser = oDSP.OpenDSObject(sRoot,vbNullString,vbNullString,ADS_SERVER_BIND AND ADS_NO_AUTHENTICATION)
    If Err.Number <> 0 Then
        If Err.Number = "-2147016656" Then
            Echo "**** ERROR: Username not found."
        ElseIf Err.Number = "-2147016646" Then
            Echo "**** ERROR: LDAP server not found."  
        Else 
            Echo "**** ERROR: Unable to bind to LDAP server. " & Err.Number
        End If
        Echo ""
        Echo "Use: c:\>cscript ldapauth.vbs GatorLinkName [GatorLinkPassword]"
        Echo ""
        Echo "Aborting..."
        Echo ""
        WScript.Quit
    End If
End If

'Populate the user property cache
oUser.GetInfo

Echo "There are " & oUser.PropertyCount & " properties"
Echo "for " & oUser.AdsPath
Echo "in " & oUser.Schema & vbCRLF & vbCRLF

'Iterate through available user attributes
For count = 0 to (oUser.PropertyCount-1)
    sAttribName = oUser.Item(CInt(count)).Name
    sAttribVal = oUser.Get(sAttribName)

    If IsArray(sAttribVal) Then
        For Each sMultiVal in oUser.GetEx(sAttribName)
            sAttribList = sAttribList & sAttribName & Space(16-Len(sAttribName)) & ":: " & sMultiVal & vbCRLF
        Next
    Else
        sAttribList = sAttribList & sAttribName & Space(16-Len(sAttribName)) & ":  " & sAttribVal & vbCRLF
    End If
Next

Echo sAttribList

'Clean up
set oDSP=Nothing
set oUser=Nothing

wscript.Quit

Sub Echo(byref message)
   WScript.Echo message
End Sub