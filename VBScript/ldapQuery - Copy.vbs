
Dim strPassword


Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
'Set InPutFile = FileSystem.OpenTextFile("C:\Scratch\VBScript\delegates.csv",ForReading, False)
Set outputFile = FileSystem.CreateTextFile("C:\Scratch\VBScript\ldapQuery.txt",True)

sUser = mchirchir
sDN = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://ldapmaster.umuc.edu/dc=umuc,dc=edu"
Password("Password:")

'vSearch = "(umucEmplId=0072718)"

Dim dso: Set dso = GetObject("LDAP:")
Dim oAuth: Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Dim oConn: Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword


Set ou = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu",sDN ,strPassword, 0)

WScript.Echo ou.class
ou.GetInfo

WScript.Echo ou.PropertyCount
WScript.Echo ou.umucEmployeeEmail
'ou.put "umucEmployeeEmail", "Mark.Chirchir2@gmtest.umuc.edu"
'ou.setinfo

For Each obj In ou
If obj.umucEmployeeEmail="Mark.Chirchir@umuc.edu" Then
		WScript.Echo obj.uid
		WScript.Echo obj.umucemplID
		WScript.Echo obj.umucemployeeemail
End If
Next



Function Password( myPrompt )
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
'
' Written by Rob van der Woude
' http://www.robvanderwoude.com
' Error handling code written by Denis St-Pierre
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = Nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = Nothing
End Function