
Dim strPassword
Dim userObj

Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
'Set InPutFile = FileSystem.OpenTextFile("C:\Scratch\VBScript\delegates.csv",ForReading, False)
Set outputFile = FileSystem.CreateTextFile("C:\Scratch\VBScript\ldapQuery.txt",True)

'Setup admin credentials
sUser = mchirchir
sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://ldapmasterqat.umuc.edu/dc=umuc,dc=edu"
Password("Password:")

'Search for specified umucEmplID
vSearch = "(umucEmplId=0996775)"

'Bind to LDAP
Dim dso: Set dso = GetObject("LDAP:")
Dim oAuth: Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Dim oConn: Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword

'Get users umucGUID
Set oRS = oConn.Execute("<LDAP://ldapmasterqat.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucGUID;subtree")

For Each Element In oRS.Fields(0).Value
	WScript.Echo Element
	uid = Element
Next
umucEmplId = oRS.Fields(1).Value
umucGUID = oRS.Fields(2).Value

WScript.Echo uid &","& umucEmplId &","& umucGUID

ldapObj = "umucGUID="& umucGUID & ",ou=people,dc=umuc,dc=edu"

'Get user object
Set obj = dso.OpenDSObject("LDAP://ldapmasterqat.umuc.edu/"& ldapobj,sDN ,strPassword, 0)

WScript.Echo obj.class
ou.GetInfo

WScript.Echo ou.PropertyCount
WScript.Echo ou.umucEmployeeEmail

'write to user object
'ou.put "umucEmployeeEmail", "Mark.Chirchir2@gmtest.umuc.edu"
'ou.setinfo

'userObj for account creation
'userObj = Array(uid,givenName,sn,umucEmplID,umucGUID,umucEmployeeEmail,eduPersonPrimaryAffiliation,umucPrefFirstName,sourceUserID)

'userObj for group memberships
'userObj = Array(Source uid,Destination uid)

Function Password( myPrompt )
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
'
' Written by Rob van der Woude
' http://www.robvanderwoude.com
' Error handling code written by Denis St-Pierre
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = Nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = Nothing
End Function


Function RndPassword(vLength)
  'This function will generate a random strong password of variable
  'length.
  'This script is provided under the Creative Commons license located
  'at http://creativecommons.org/licenses/by-nc/2.5/ . It may not
  'be used for commercial purposes with out the expressed written consent
  'of NateRice.com
 
  For x=1 To vLength
    Randomize
    vChar = Int(89*Rnd) + 33
    If vChar = 34 Then 'this is quote character, replace it with a single quote
      vChar = 39
    End if

    RndPassword = RndPassword & Chr(vChar)
  Next

End Function

Function CreateADAccount(userObj)

Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNT_DISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32

Dim objRootLDAP, objContainer, objUser, objShell, strContainer, objUserTemplate, objUpdate
Dim uid, givenName, sn, umucEmplID, umucGUID
Dim umucEmployeeEmail, eduPersonPrimaryAffiliation
Dim umucPrefFirstName, sourceUserID, uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass

'read data from the object
uid = userObj(0)
givenName = userObj(1)
sn = userObj(2)
umucEmplID = userObj(3)
umucGUID = userObj(4)
umucEmployeeEmail = userObj(5)
eduPersonPrimaryAffiliation = userObj(6)
umucPrefFirstName = userObj(7)
sourceUserID = userObj(8)

'create variables
uPN = uid & "@us.umuc.edu"
displayName = umucPrefFirstName & " " & sn
mail = umucPrefFirstName & "." & sn & "@umuc.edu"


strContainer = "OU=People ," ' Note the comma

' Bind to Active Directory, Users container.
Set objRootLDAP = GetObject("LDAP://rootDSE")
Set objContainer = GetObject("LDAP://adeprdcns02.us.umuc.edu/" & strContainer & _
objRootLDAP.Get("defaultNamingContext"))

'Get description and department from the template
Set objUserTemplate = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & sourceUserID & ",ou=people,dc=us,dc=umuc,dc=edu")
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objUser = objContainer.Create("User", "cn=" & uid)

objUser.Put "sAMAccountName", uid
objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", givenName
objUser.Put "sn", sn
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "description", tempDesc
objUser.Put "employeeID", umucEmplID
objUser.Put "mail", mail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", "TBD"
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", eduPersonPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

Set objUpdate = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & uid & ",ou=people,dc=us,dc=umuc,dc=edu")

'set password and enable the account
intUAc = 512
randomPass = RndPassword(10)
objUpdate.SetPassword randomPass
objUpdate.AccountDisabled = False
objUpdate.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUpdate.Put "pwdLastSet", 0
objUpdate.SetInfo

End Function


Function CopyADGroups(userObj)
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3

'Set ofs = createobject("scripting.filesystemobject")
'Set ousernameFile = ofs.opentextfile("C:\Scratch\Account Creation\MembershipTemplates.txt" , 1, false)

sourceusername = userObj(0)
destinationusername = userObj(1)

wscript.echo "Copying Group Memberships"
wscript.echo
wscript.echo "Source User: " & sourceusername
wscript.echo "Destination User: " & destinationusername
wscript.echo


Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & sourceusername & ",ou=people,dc=us,dc=umuc,dc=edu")
 
arrMemberOf = objUser.GetEx("memberOf")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
		Set objGroup = GetObject("LDAP://adeprdcns02.us.umuc.edu/" & Group)
			objGroup.PutEx ADS_PROPERTY_APPEND,"member", Array("cn=" & destinationusername & ",ou=people,dc=us,dc=umuc,dc=edu")
			objGroup.SetInfo
    Next
End If

End Function
