Class usrClass
    Private givenName
    Private sn
    Private uid
    Private umucEmplId
    Private umucGUID
    Private umucPrefFirstName
    Private umucCampusEmail
    Private umucEmployeeEmail
    Private umucFacultyEmail
    Private eduPersonPrimaryAffiliation
    Private umucVPNGroup
    
    Property Get firstName()
        firstName = givenName
    End Property

    Property Get lastName()
        lastName = sn
    End Property

    Property Get userid()
        userid = uid
    End Property

    Property Get emplID()
        emplID = umucEmplId
    End Property

    Property Get ldapGUID()
        ldapGUID = umucGUID
    End Property

    Property Get prefFirstName()
        prefFirstName = umucPrefFirstName
    End Property

    Property Get campusEmail()
        campusEmail = umucCampusEmail
    End Property

    Property Get employeeEmail()
        employeeEmail = umucEmployeeEmail
    End Property
    
    Property Get facultyEmail()
        facultyEmail = umucFacultyEmail
    End Property

    Property Get personPrimaryAffiliation()
        personPrimaryAffiliation = eduPersonPrimaryAffiliation
    End Property

    Property Get vpnGroup()
        vpnGroup = umucVPNGroup
    End Property

    Property Let firstName(NewVal)
        givenName = NewVal
    End Property

    Property Let lastName(NewVal)
        sn = NewVal
    End Property

    Property Let userid(NewVal)
        uid = NewVal
    End Property

    Property Let emplID(NewVal)
        umucEmplId = NewVal
    End Property

    Property Let ldapGUID(NewVal)
        umucGUID = NewVal
    End Property

    Property Let prefFirstName(NewVal)
        umucPrefFirstName = NewVal
    End Property

    Property Let campusEmail(NewVal)
        umucCampusEmail = NewVal
    End Property

    Property Let employeeEmail(NewVal)
        umucEmployeeEmail = NewVal
    End Property
    
    Property Let facultyEmail(NewVal)
        umucFacultyEmail = NewVal
    End Property

    Property Let personPrimaryAffiliation(NewVal)
        eduPersonPrimaryAffiliation = NewVal
    End Property

    Property Let vpnGroup(NewVal)
        umucVPNGroup = NewVal
    End Property
End Class

'Set usrObj = New usrClass
'newVal = "9999999"
'usrObj.emplID = newVal
'var = usrObj.emplID
'WScript.Echo var



Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNT_DISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32
Const ADS_PROPERTY_CLEAR = 1

Dim strPassword
Dim userObj
Dim duplicateEmail
Dim sDN
Dim sRoot
Dim sUser
Dim dso
Dim oAuth
Dim oConn
Dim usrObj
Dim usrPass

duplicateEmail = 0

Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
'Set InPutFile = FileSystem.OpenTextFile("C:\Scratch\VBScript\delegates.csv",ForReading, False)
Set outputFile = FileSystem.CreateTextFile("C:\Scratch\VBScript\ldapQuery.txt",True)

sUser = mchirchir
sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
'ldapmaster sDN = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://ldapmasterqat.umuc.edu/dc=umuc,dc=edu"
'sRoot = "LDAP://ldapmaster.umuc.edu/dc=umuc,dc=edu"

Password("Password:")

'Get user input 



Function acctCreate(userInput)

Set dso = GetObject("LDAP:")
Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword

'*******************************

Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile("c:\Scratch\VBScript\emplid.txt" , 1, false)

'Run the main logic
While not ousernameFile.AtEndOfStream
	useriden = ousernameFile.ReadLine()
	'InputLine = split(ousernameFile.ReadLine(),",")
	'destinationusername = InputLine(0)
	'useriden = InputLine(1)
	
	Set usrObj = New usrClass
	vsearch = "(umucEmplid=" & useriden & ")"
	'vsearch = "(uid=" & useriden & ")"
	'Set oRS = oConn.Execute("<LDAP://ldapmaster.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucGUID,umucPrefFirstName,umucCampusEmail,umucEmployeeEmail,eduPersonPrimaryAffiliation,umucVPNGroup;subtree")
	ldapExtract(vsearch)
	usrPass = SHA(TempPW1)
	WScript.Echo usrPass
	'updatePass
	Set usrobj = Nothing

Wend
End Function

'*******************************************************************************************************
Function Password( myPrompt )
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
'
' Written by Rob van der Woude
' http://www.robvanderwoude.com
' Error handling code written by Denis St-Pierre
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = Nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = Nothing
End Function

'*******************************************************************************************************
Function ldapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucGUID,umucPrefFirstName,umucCampusEmail,umucEmployeeEmail,eduPersonPrimaryAffiliation,umucVPNGroup;subtree")


For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.lastName = var2
Next
For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

usrObj.ldapGUID = oRS.Fields(4).Value

If IsNull(oRS.Fields(7).Value) Then
	var1 = usrObj.firstName
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.prefFirstName = var2
Else
	var1 = oRS.Fields(5).Value
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)	
	usrObj.prefFirstName = var2
End If


If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
Else
For Each Element In oRS.Fields(6).Value
	usrObj.campusEmail = Element
Next
End If



If IsNull(oRS.Fields(7).Value) Then
	var1 = split(usrObj.campusEmail,"@")
	var2 = split(var1(0),".")
	var3 = var2(0)
	var4 = var2(1)
	var5 = ucase(mid(var3,1,1)) & mid(var3,2)
	var6 = ucase(mid(var4,1,1)) & mid(var4,2)
		
	usrObj.employeeEmail = var5 & "." & var6 & "@umuc.edu"
	
Else
	usrObj.employeeEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

If IsNull(oRS.Fields(9).Value) Then
	usrObj.vpnGroup = "Nothing"
else
	For Each Element In oRS.Fields(9).Value
		usrObj.vpnGroup = Element
	Next
End If

End Function


'*******************************************************************************************************

Function facultyLdapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucGUID,umucPrefFirstName,umucCampusEmail,umucFacultyEmail,eduPersonPrimaryAffiliation,umucVPNGroup;subtree")


For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.lastName = var2
Next
For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

usrObj.ldapGUID = oRS.Fields(4).Value

If IsNull(oRS.Fields(7).Value) Then
	var1 = usrObj.firstName
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.prefFirstName = var2
Else
	var1 = oRS.Fields(5).Value
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)	
	usrObj.prefFirstName = var2
End If


If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
Else
For Each Element In oRS.Fields(6).Value
	usrObj.campusEmail = Element
Next
End If



If IsNull(oRS.Fields(7).Value) Then
	var1 = split(usrObj.campusEmail,"@")
	var2 = split(var1(0),".")
	var3 = var2(0)
	var4 = var2(1)
	var5 = ucase(mid(var3,1,1)) & mid(var3,2)
	var6 = ucase(mid(var4,1,1)) & mid(var4,2)
		
	usrObj.facultyEmail = var5 & "." & var6 & "@faculty.umuc.edu"
	
Else
	usrObj.facultyEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

If IsNull(oRS.Fields(9).Value) Then
	usrObj.vpnGroup = "Nothing"
else
	For Each Element In oRS.Fields(9).Value
		usrObj.vpnGroup = Element
	Next
End If

End Function

'*******************************************************************************************************
Function RndPassword(vLength)
  'This function will generate a random strong password of variable
  'length.
  'This script is provided under the Creative Commons license located
  'at http://creativecommons.org/licenses/by-nc/2.5/ . It may not
  'be used for commercial purposes with out the expressed written consent
  'of NateRice.com
 
  For x=1 To vLength
    Randomize
    vChar = Int(89*Rnd) + 33
    If vChar = 34 Then 'this is quote character, replace it with a single quote
      vChar = 39
    End if

    RndPassword = RndPassword & Chr(vChar)
  Next

End Function

'*******************************************************************************************************

Function updatePass()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
'usrObj.employeeEmail = usrObj.prefFirstName &"."& usrObj.lastName &"@umuc.edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "userPassword", usrPass
obj.setinfo

End Function

'*******************************************************************************************************
Function SHA(ByVal sMessage)
    Dim i, result(32), temp(8) As Double, fraccubeprimes, hashValues
    Dim done512, index512, words(64) As Double, index32, mask(4)
    Dim s0, s1, t1, t2, maj, ch, strLen
 
    mask(0) = 4294967296#
    mask(1) = 16777216
    mask(2) = 65536
    mask(3) = 256
 
    hashValues = Array( _
        1779033703, 3144134277#, 1013904242, 2773480762#, _
        1359893119, 2600822924#, 528734635, 1541459225)
 
    fraccubeprimes = Array( _
        1116352408, 1899447441, 3049323471#, 3921009573#, 961987163, 1508970993, 2453635748#, 2870763221#, _
        3624381080#, 310598401, 607225278, 1426881987, 1925078388, 2162078206#, 2614888103#, 3248222580#, _
        3835390401#, 4022224774#, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, _
        2554220882#, 2821834349#, 2952996808#, 3210313671#, 3336571891#, 3584528711#, 113926993, 338241895, _
        666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350#, 2456956037#, _
        2730485921#, 2820302411#, 3259730800#, 3345764771#, 3516065817#, 3600352804#, 4094571909#, 275423344, _
        430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, _
        1955562222, 2024104815, 2227730452#, 2361852424#, 2428436474#, 2756734187#, 3204031479#, 3329325298#)
 
    sMessage = Nz(sMessage, "")
    strLen = Len(sMessage) * 8
    sMessage = sMessage & Chr(128)
    done512 = False
    index512 = 0
 
    If (Len(sMessage) Mod 64) < 60 Then
        sMessage = sMessage & String(60 - (Len(sMessage) Mod 64), Chr(0))
    ElseIf (Len(sMessage) Mod 64) > 60 Then
        sMessage = sMessage & String(124 - (Len(sMessage) Mod 64), Chr(0))
    End If
 
    sMessage = sMessage & Chr(Int((strLen / mask(0) - Int(strLen / mask(0))) * 256))
    sMessage = sMessage & Chr(Int((strLen / mask(1) - Int(strLen / mask(1))) * 256))
    sMessage = sMessage & Chr(Int((strLen / mask(2) - Int(strLen / mask(2))) * 256))
    sMessage = sMessage & Chr(Int((strLen / mask(3) - Int(strLen / mask(3))) * 256))
 
    Do Until done512
        For i = 0 To 15
            words(i) = Asc(Mid(sMessage, index512 * 64 + i * 4 + 1, 1)) * mask(1) + Asc(Mid(sMessage, index512 * 64 + i * 4 + 2, 1)) * mask(2) + Asc(Mid(sMessage, index512 * 64 + i * 4 + 3, 1)) * mask(3) + Asc(Mid(sMessage, index512 * 64 + i * 4 + 4, 1))
        Next
 
        For i = 16 To 63
            s0 = largeXor(largeXor(rightRotate(words(i - 15), 7, 32), rightRotate(words(i - 15), 18, 32), 32), Int(words(i - 15) / 8), 32)
            s1 = largeXor(largeXor(rightRotate(words(i - 2), 17, 32), rightRotate(words(i - 2), 19, 32), 32), Int(words(i - 2) / 1024), 32)
            words(i) = Mod32Bit(words(i - 16) + s0 + words(i - 7) + s1)
        Next
 
        For i = 0 To 7
            temp(i) = hashValues(i)
        Next
 
        For i = 0 To 63
            s0 = largeXor(largeXor(rightRotate(temp(0), 2, 32), rightRotate(temp(0), 13, 32), 32), rightRotate(temp(0), 22, 32), 32)
            maj = largeXor(largeXor(largeAnd(temp(0), temp(1), 32), largeAnd(temp(0), temp(2), 32), 32), largeAnd(temp(1), temp(2), 32), 32)
            t2 = Mod32Bit(s0 + maj)
            s1 = largeXor(largeXor(rightRotate(temp(4), 6, 32), rightRotate(temp(4), 11, 32), 32), rightRotate(temp(4), 25, 32), 32)
            ch = largeXor(largeAnd(temp(4), temp(5), 32), largeAnd(largeNot(temp(4), 32), temp(6), 32), 32)
            t1 = Mod32Bit(temp(7) + s1 + ch + fraccubeprimes(i) + words(i))
 
            temp(7) = temp(6)
            temp(6) = temp(5)
            temp(5) = temp(4)
            temp(4) = Mod32Bit(temp(3) + t1)
            temp(3) = temp(2)
            temp(2) = temp(1)
            temp(1) = temp(0)
            temp(0) = Mod32Bit(t1 + t2)
        Next
 
        For i = 0 To 7
            hashValues(i) = Mod32Bit(hashValues(i) + temp(i))
        Next
 
        If (index512 + 1) * 64 >= Len(sMessage) Then done512 = True
        index512 = index512 + 1
    Loop
 
    For i = 0 To 31
        result(i) = Int((hashValues(i \ 4) / mask(i Mod 4) - Int(hashValues(i \ 4) / mask(i Mod 4))) * 256)
    Next
 
    SHA = result
End Function
 
Function Mod32Bit(value)
    Mod32Bit = Int((value / 4294967296# - Int(value / 4294967296#)) * 4294967296#)
End Function
 
Function rightRotate(value, amount, totalBits)
    'To leftRotate, make amount = totalBits - amount
    Dim i
    rightRotate = 0
 
    For i = 0 To (totalBits - 1)
        If i >= amount Then
            rightRotate = rightRotate + (Int((value / (2 ^ (i + 1)) - Int(value / (2 ^ (i + 1)))) * 2)) * 2 ^ (i - amount)
        Else
            rightRotate = rightRotate + (Int((value / (2 ^ (i + 1)) - Int(value / (2 ^ (i + 1)))) * 2)) * 2 ^ (totalBits - amount + i)
        End If
    Next
End Function
 
Function largeXor(value, xorValue, totalBits)
    Dim i, a, b
    largeXor = 0
 
    For i = 0 To (totalBits - 1)
        a = (Int((value / (2 ^ (i + 1)) - Int(value / (2 ^ (i + 1)))) * 2))
        b = (Int((xorValue / (2 ^ (i + 1)) - Int(xorValue / (2 ^ (i + 1)))) * 2))
        If a <> b Then
            largeXor = largeXor + 2 ^ i
        End If
    Next
End Function
 
Function largeNot(value, totalBits)
    Dim i, a
    largeNot = 0
 
    For i = 0 To (totalBits - 1)
        a = Int((value / (2 ^ (i + 1)) - Int(value / (2 ^ (i + 1)))) * 2)
        If a = 0 Then
            largeNot = largeNot + 2 ^ i
        End If
    Next
End Function
 
Function largeAnd(value, andValue, totalBits)
    Dim i, a, b
    largeAnd = 0
 
    For i = 0 To (totalBits - 1)
        a = Int((value / (2 ^ (i + 1)) - Int(value / (2 ^ (i + 1)))) * 2)
        b = (Int((andValue / (2 ^ (i + 1)) - Int(andValue / (2 ^ (i + 1)))) * 2))
        If a = 1 And b = 1 Then
            largeAnd = largeAnd + 2 ^ i
        End If
    Next
End Function