
Option Explicit
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNT_DISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32

Dim objRootLDAP, objContainer, objUser, objShell, strContainer, objUserTemplate, objUpdate
Dim uid, givenName, sn, umucEmplID, umucGUID
Dim umucEmployeeEmail, eduPersonPrimaryAffiliation
Dim umucPrefFirstName, sourceUserID, uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass

'read data from the object
uid = "mchirchir2"
givenName = "Mark"
sn = "Chirchir"
umucEmplID = "99999999"
umucGUID = "umuc-Guid"
umucEmployeeEmail = "Mark.Chirchir2@umuc.edu"
eduPersonPrimaryAffiliation = "employee"
umucPrefFirstName = "Mark"
sourceUserID = "mchirchir"

'create variables
uPN = uid & "@us.umuc.edu"
displayName = umucPrefFirstName & " " & sn
mail = umucPrefFirstName & "." & sn & "2@umuc.edu"


strContainer = "OU=People ," ' Note the comma

' Bind to Active Directory, Users container.
Set objRootLDAP = GetObject("LDAP://rootDSE")
Set objContainer = GetObject("LDAP://adeprdcns02.us.umuc.edu/" & strContainer & _
objRootLDAP.Get("defaultNamingContext"))

'Get description and department from the template
Set objUserTemplate = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & sourceUserID & ",ou=people,dc=us,dc=umuc,dc=edu")
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")


' Build the actual User.
Set objUser = objContainer.Create("User", "cn=" & uid)

objUser.Put "sAMAccountName", uid
objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", givenName
objUser.Put "sn", sn
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "description", tempDesc
objUser.Put "employeeID", umucEmplID
objUser.Put "mail", mail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", "TBD"
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", eduPersonPrimaryAffiliation
objUser.Put "streetAddress", "TBD"

objUser.SetInfo
Set objUpdate = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & uid & ",ou=people,dc=us,dc=umuc,dc=edu")
	
'set password and enable the account
intUAc = 512
randomPass = RndPassword(10)
objUpdate.SetPassword randomPass
'objUpdate.Put "PasswordExpired", CLng(1)
objUpdate.AccountDisabled = False
objUpdate.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUpdate.Put "pwdLastSet", 0

objUpdate.SetInfo


Function RndPassword(vLength)
  'This function will generate a random strong password of variable
  'length.
  'This script is provided under the Creative Commons license located
  'at http://creativecommons.org/licenses/by-nc/2.5/ . It may not
  'be used for commercial purposes with out the expressed written consent
  'of NateRice.com
 Dim x, vChar
  For x=1 To vLength
    Randomize
    vChar = Int(89*Rnd) + 33
    If vChar = 34 Then 'this is quote character, replace it with a single quote
      vChar = 39
    End if

    RndPassword = RndPassword & Chr(vChar)
  Next

End Function
