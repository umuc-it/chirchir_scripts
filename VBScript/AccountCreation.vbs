'******************************************************************************
'Account Creation
'Created by Mark Chirchir
'Version 2.0
'******************************************************************************
'******************************************************************************
'Class Declarations
'******************************************************************************
Class usrClass
    Private givenName
    Private sn
    Private uid
    Private umucEmplId
    Private umucGUID
    Private umucPrefFirstName
    Private umucCampusEmail
    Private umucEmployeeEmail
    Private umucFacultyEmail
    Private eduPersonPrimaryAffiliation
    Private umucVPNGroup
    Private title
    Private description
    Private department
    
    Property Get firstName()
        firstName = givenName
    End Property

    Property Get lastName()
        lastName = sn
    End Property

    Property Get userid()
        userid = uid
    End Property

    Property Get emplID()
        emplID = umucEmplId
    End Property

    Property Get ldapGUID()
        ldapGUID = umucGUID
    End Property

    Property Get prefFirstName()
        prefFirstName = umucPrefFirstName
    End Property

    Property Get campusEmail()
        campusEmail = umucCampusEmail
    End Property

    Property Get employeeEmail()
        employeeEmail = umucEmployeeEmail
    End Property
    
    Property Get facultyEmail()
        facultyEmail = umucFacultyEmail
    End Property

    Property Get personPrimaryAffiliation()
        personPrimaryAffiliation = eduPersonPrimaryAffiliation
    End Property

    Property Get vpnGroup()
        vpnGroup = umucVPNGroup
    End Property

    Property Get userTitle()
        userTitle = title
    End Property

    Property Get userdescription()
        userdescription = description
    End Property

    Property Get userdepartment()
        userdepartment = department
    End Property
            
    Property Let firstName(NewVal)
        givenName = NewVal
    End Property

    Property Let lastName(NewVal)
        sn = NewVal
    End Property

    Property Let userid(NewVal)
        uid = NewVal
    End Property

    Property Let emplID(NewVal)
        umucEmplId = NewVal
    End Property

    Property Let ldapGUID(NewVal)
        umucGUID = NewVal
    End Property

    Property Let prefFirstName(NewVal)
        umucPrefFirstName = NewVal
    End Property

    Property Let campusEmail(NewVal)
        umucCampusEmail = NewVal
    End Property

    Property Let employeeEmail(NewVal)
        umucEmployeeEmail = NewVal
    End Property
    
    Property Let facultyEmail(NewVal)
        umucFacultyEmail = NewVal
    End Property

    Property Let personPrimaryAffiliation(NewVal)
        eduPersonPrimaryAffiliation = NewVal
    End Property

    Property Let vpnGroup(NewVal)
        umucVPNGroup = NewVal
    End Property
    
    Property Let userTitle(NewVal)
        title = NewVal	
    End Property
    
    Property Let userdescription(NewVal)
        description = NewVal
    End Property

    Property Let userdepartment(NewVal)
        department = NewVal
    End Property
    
End Class
	
'******************************************************************************
'Constant variables
'******************************************************************************
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNTDISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32
Const ADS_PROPERTY_CLEAR = 1
Const ADS_SECURE_AUTHENTICATION = 1
Const ADS_USE_ENCRYPTION = 2


'******************************************************************************
'Global variables
'******************************************************************************
Dim strPassword
Dim userObj
Dim duplicateEmail
Dim sDN
Dim sRoot
Dim sUser
Dim dso
Dim oAuth
Dim oConn
Dim usrObj
Dim usrObjClone
Dim acctIsThere
acctIsThere = 0
duplicateEmail = 0

Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1


'Set outputFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\output.txt",2,true)
'outputFile.WriteLine("Username, Password, Email Address")

'Set delegateFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\updates.csv",2,true)
'delegateFile.WriteLine("Template,Target")

'******************************************************************************
'User credentials
'******************************************************************************
sUser = "us\mchirchir_eu"
'ldapmasterQAT sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
sDN = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://ldapmaster.umuc.edu/dc=umuc,dc=edu"
Password("Password:")

'******************************************************************************
'Get User Input
'******************************************************************************
Input = 0
While Input < 6
	
	WScript.Echo ""
	WScript.Echo "Please choose from the following list"
	WScript.Echo "1. Create User Email Account"
	WScript.Echo "2. Create Faculty Email Account"
	WScript.Echo "3. Delete User Mailbox and Print Separation Template"
	WScript.Echo "4. Print Faculty email"
	WScript.Echo "5. Create User Email Account"
	WScript.Echo "6. End Program"
	WScript.Echo ""
	
	WScript.StdIn.Read(0)
	Input = WScript.StdIn.ReadLine

'Call the main logic	
	If Input < 6 then
		main(Input)
	End if

Wend

'******************************************************************************
'Main Function
'******************************************************************************
Function main(userInput)

Set dso = GetObject("LDAP:")
Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword

Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile("c:\Scratch\VBScript\emplid.txt" , 1, false)

'Run the main logic
While not ousernameFile.AtEndOfStream
	useriden = ousernameFile.ReadLine()
	
	'Create new User	
	Set usrObj = New usrClass
	Set usrObjClone = New usrClass
	vsearch = "(umucemplid=" & useriden & ")"

	Select Case userInput
  		Case 1
  			'Update Staff
	    	ldapExtract(vsearch)
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
				WScript.Echo usrObj.lastName & "|" & usrObj.firstName & "|" & usrObj.userid & "|SSN|" & usrObj.emplID & "|"
			End If
			duplicateEmail = 0
						
  		Case 2
  			'Update Faculty
	    	facultyLdapExtract(vsearch)
			checkFacultyEmail
			If duplicateEmail = 0 Then
				updateFacultyEmail
				WScript.Echo usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail
			End If
			duplicateEmail = 0
  		Case 3
  			'Separate Account
	    	ldapExtract(vsearch)
			disableAccount
			printSeparation
			
  		Case 4
  			'Print Faculty email
			facultyLdapExtract(vsearch)
			WScript.Echo usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail

  		Case 5
  			'Update Staff
  			
  			InputLine = split(useriden,",")
			newEmployee = InputLine(0)
			oldEmployee = InputLine(1)
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
			If IsNumeric(oldEmployee) Then
				oldemp = "(umucemplid=" & oldEmployee & ")"
			Else
				oldemp = "(uid=" & oldEmployee & ")"
			End If
			
	    	ldapExtract(newemp)  	
	    	ldapExtractClone(oldemp)
	    	
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
			End If

			findAccount
			If acctIsThere = 0 Then
				CreateADAccount
				WScript.Sleep 5000
				CopyADGroups
			End If
			
			'WScript.Echo usrObj.lastName & "|" & usrObj.firstName & "|" & usrObj.userid & "|SSN|" & usrObj.emplID & "|"& usrObj.vpnGroup
			'WScript.Echo usrObj.userTitle
			'WScript.Echo usrObj.lastName & "|" & usrObj.firstName & "|" & usrObj.userid & "|SSN|" & usrObj.emplID & "|"& usrObj.vpnGroup & "|"& usrObj.userTitle & "|"
			'WScript.Echo usrObjClone.lastName & "|" & usrObjClone.firstName & "|" & usrObjClone.userid & "|SSN|" & usrObjClone.emplID & "|"& usrObjClone.vpnGroup & "|"
			acctIsThere =0
			duplicateEmail = 0
							    	   	
	End Select
	
	Set usrobj = Nothing
	Set usrObjClone = Nothing
	
Wend

outputFile.Close
Set outputFile = Nothing

End Function

'******************************************************************************
'Get User Password
'******************************************************************************
Function Password(myPrompt)
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = Nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = Nothing
End Function

'******************************************************************************
'Populate user object class
'******************************************************************************
Function ldapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,title,umucPrefFirstName,umucCampusEmail,umucEmployeeEmail,eduPersonPrimaryAffiliation,umucGUID;subtree")

For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.lastName = var2
Next
For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

If IsNull(oRS.Fields(4).Value) Then
	usrObj.userTitle = "TBD"
Else
For Each Element In oRS.Fields(4).Value
	usrObj.userTitle = Element
Next
End If

If IsNull(oRS.Fields(5).Value) Then
	var1 = usrObj.firstName
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.prefFirstName = var2
Else
	var1 = oRS.Fields(5).Value
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)	
	usrObj.prefFirstName = var2
End If

If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
Else
For Each Element In oRS.Fields(6).Value
	usrObj.campusEmail = Element
Next
End If

If IsNull(oRS.Fields(7).Value) Then
	var1 = split(usrObj.campusEmail,"@")
	var2 = split(var1(0),".")
	var3 = var2(0)
	var4 = var2(1)
	var5 = ucase(mid(var3,1,1)) & mid(var3,2)
	var6 = ucase(mid(var4,1,1)) & mid(var4,2)
		
	usrObj.employeeEmail = var5 & "." & var6 & "@umuc.edu"
	
Else
	usrObj.employeeEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

usrObj.ldapGUID = oRS.Fields(9).Value

End Function

'******************************************************************************
'Populate faculty user object class
'******************************************************************************
Function facultyLdapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucGUID,umucPrefFirstName,umucCampusEmail,umucFacultyEmail,eduPersonPrimaryAffiliation,umucVPNGroup;subtree")

For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.lastName = var2
Next
For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

usrObj.ldapGUID = oRS.Fields(4).Value

If IsNull(oRS.Fields(7).Value) Then
	var1 = usrObj.firstName
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.prefFirstName = var2
Else
	var1 = oRS.Fields(5).Value
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)	
	usrObj.prefFirstName = var2
End If


If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
Else
For Each Element In oRS.Fields(6).Value
	usrObj.campusEmail = Element
Next
End If

If IsNull(oRS.Fields(7).Value) Then
	var1 = split(usrObj.campusEmail,"@")
	var2 = split(var1(0),".")
	var3 = var2(0)
	var4 = var2(1)
	var5 = ucase(mid(var3,1,1)) & mid(var3,2)
	var6 = ucase(mid(var4,1,1)) & mid(var4,2)
		
	usrObj.facultyEmail = var5 & "." & var6 & "@faculty.umuc.edu"
	
Else
	usrObj.facultyEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

If IsNull(oRS.Fields(9).Value) Then
	usrObj.vpnGroup = "Nothing"
else
	For Each Element In oRS.Fields(9).Value
		usrObj.vpnGroup = Element
	Next
End If

End Function

'******************************************************************************
'Populate user object class
'******************************************************************************
Function ldapExtractClone(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucEmployeeEmail,umucVPNGroup;subtree")


For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObjClone.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObjClone.lastName = var2
Next

For Each Element In oRS.Fields(2).Value
	usrObjClone.userid = Element
Next

usrObjClone.emplID = oRS.Fields(3).Value

usrObjClone.employeeEmail = oRS.Fields(4).Value

If IsNull(oRS.Fields(5).Value) Then
	usrObjClone.vpnGroup = "Nothing"
else
	For Each Element In oRS.Fields(5).Value
		usrObjClone.vpnGroup = Element
	Next
End If

End Function

'******************************************************************************
'Generate random password
'******************************************************************************
Function RndPassword()
  'This function will generate a random strong password of variable
  'length.
  'This script is provided under the Creative Commons license located
  'at http://creativecommons.org/licenses/by-nc/2.5/ . It may not
  'be used for commercial purposes with out the expressed written consent
  'of NateRice.com
 
  For x = 1 To 12
    Randomize
    vChar = Int(89*Rnd) + 33
    If vChar = 34 Then
      vChar = 33
    ElseIf vChar >= 39 And vchar <= 49 Then
      vChar = 35
    ElseIf vChar >= 58 And vchar <= 64 Then
      vChar = 37
    ElseIf vChar = 73 Or vChar = 79 Or vChar = 108 Or vChar = 111 Then
      vChar = 38
    ElseIf vChar >= 91 And vchar <= 96 Then
      vChar = 35
    ElseIf vChar > 122 Then
      vChar = 33
    End if

    RndPassword = RndPassword & Chr(vChar)
  Next

End Function

'******************************************************************************
' Search for a User Account in Active Directory
'******************************************************************************
Function findAccount

dtStart = TimeValue(Now())

Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = _
    "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & _
         "(samAccountName=" & usrObj.userID & "));samAccountName;subtree"
  
Set objRecordSet = objCommand.Execute
 
If objRecordset.RecordCount = 0 Then
    'do nothing
Else
    WScript.Echo usrObj.userID & " Already Exists in Active Directory."
    acctIsThere = 1
End If
 
objConnection.Close

End Function

'******************************************************************************
'Create user account in AD
'******************************************************************************
Function CreateADAccount()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@us.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function CopyADGroups()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=us,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://adeprdcns02.us.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If
End Function

'******************************************************************************
'Check LDAP for email duplicates
'******************************************************************************
Function checkEmail()

vsearch = "(umucEmployeeEmail=" & usrObj.employeeEmail & ")"

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Check LDAP for faculty email duplicates
'******************************************************************************
Function checkFacultyEmail()

vsearch = "(umucFacultyEmail=" & usrObj.facultyEmail & ")"

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Faculty Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Populate faculty email
'******************************************************************************
Function updateFacultyEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucFacultyEmail", usrObj.facultyEmail
obj.setinfo

End Function

'******************************************************************************
'Populate staff email
'******************************************************************************
Function updateEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
'usrObj.employeeEmail = usrObj.prefFirstName &"."& usrObj.lastName &"@umuc.edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucEmployeeEmail", usrObj.employeeEmail
If usrObjClone.vpngroup = "Nothing" Then
	'do nothing
Else
	obj.put "umucVPNGroup", usrObjClone.vpnGroup
End If

obj.setinfo

WScript.Echo "Email address created in LDAP: " & usrObj.employeeEmail
End Function

'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccount()
'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
obj.setinfo


'Ad Disable account
userDN = "CN=" & usrObj.userID & ",OU=People,dc=us,dc=umuc,dc=edu"

Set objDSO = GetObject("LDAP:")
Set objDisable = objDSO.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& userDN,sUser,strPassword, &H0200)

	intUAC = objDisable.Get("userAccountControl") 
  	objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
	objDisable.SetInfo 

End Function

'******************************************************************************
'Print template for HR submission
'******************************************************************************
Function printSeparation()

WScript.Echo "Name: " & usrObj.firstName & " " & usrObj.lastName
WScript.Echo "Login ID: " & usrObj.userID
WScript.Echo "EmplID: " & usrObj.emplID
WScript.Echo

End Function

'******************************************************************************
'END
'******************************************************************************