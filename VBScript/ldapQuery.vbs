Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNT_DISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32

Dim strPassword
Dim userObj

Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set InPutFile = FileSystem.OpenTextFile("C:\Scratch\VBScript\ldapQuery.txt",ForReading, False)
'Set outputFile = FileSystem.CreateTextFile("C:\Scratch\VBScript\ldapQuery.txt",True)

sUser = mchirchir
'ldapmasterQAT sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
sDN = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://ldap.umuc.edu/dc=umuc,dc=edu"
Password("Password:")


Dim dso: Set dso = GetObject("LDAP:")
Dim oAuth: Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Dim oConn: Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword

'*******************************


While not InPutFile.AtEndOfStream
	vSearch = InPutFile.ReadLine()

'vSearch = "(umucEmplID=0132661)"
'vSearch = "(uid=mchirchir)"
Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

For Each Element In oRS.Fields(0).Value
	givenName = Element
Next
For Each Element In oRS.Fields(1).Value
	sn = Element
Next
For Each Element In oRS.Fields(2).Value
	uid = Element
Next

umucEmplId = oRS.Fields(3).Value

WScript.Echo sn &","& givenName &","& uid &","& umucEmplID

Wend




Function Password( myPrompt )
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
'
' Written by Rob van der Woude
' http://www.robvanderwoude.com
' Error handling code written by Denis St-Pierre
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = Nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = Nothing
End Function


Function RndPassword(vLength)
  'This function will generate a random strong password of variable
  'length.
  'This script is provided under the Creative Commons license located
  'at http://creativecommons.org/licenses/by-nc/2.5/ . It may not
  'be used for commercial purposes with out the expressed written consent
  'of NateRice.com
 
  For x=1 To vLength
    Randomize
    vChar = Int(89*Rnd) + 33
    If vChar = 34 Then 'this is quote character, replace it with a single quote
      vChar = 39
    End if

    RndPassword = RndPassword & Chr(vChar)
  Next

End Function