'This script will take input from a comma delimited file (specified in the InputFile variable) in the form:
' firstName,lastname,displayName,Email,userName
'and updates the email address, firstname, lastname and displayname

'Global variables
Const ADS_PROPERTY_UPDATE = 2

Dim DomainArray (2)
Dim inputFile
Dim fileSystem
Dim getUser
Dim objectList

'Initialize global variables
Set fileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set inputFile = FileSystem.OpenTextFile("C:\Scratch\PrimarySMTP\userList.txt",ForReading, False)


DomainArray (0) = "dc=asia,dc=umuc,dc=edu"
DomainArray (1) = "dc=europe,dc=umuc,dc=edu"
DomainArray (2) = "dc=us,dc=umuc,dc=edu"

'Check and update the persistent info

Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.Properties("Size Limit") = 64000
objCommand.Properties("Page Size") = 64000
objCommand.Properties("Cache Results") = False

checkUpdateEmail

'Clean up
inputFile.Close
Set fileSystem=Nothing

Sub checkUpdateEmail
	While Not inputFile.AtEndOfStream
		getUser = inputFile.ReadLine()
		userArray = Split(getUser,",")
		firstName = userArray(0)
		lastName = userArray(1)
		displayName = userArray(2)
		emailAddress = userArray(3)
		userName = userArray(4)
		email = "smtp:" & emailAddress
 		blnUserFound = False
 		blnUser = 0
 	
		For Each strDomain In DomainArray
			' WScript.Echo "Searching: " & strDomain
	
			objCommand.CommandText = _
		    	"<GC://" & strDomain & ">;(&(objectCategory=User)" & _
		        	 "(proxyaddresses=" & email & "));distinguishedName,displayname,samAccountName,mail,proxyaddresses;subtree"
		  
			Set objRecordSet = objCommand.Execute
		 	
			WScript.Echo "BOF: " & objRecordSet.BOF
			WScript.Echo "EOF: " & objRecordSet.EOF
			WScript.Echo "Records Found: " & objRecordSet.RecordCount & vbCRLF
			If (objRecordSet.BOF = True And objRecordSet.EOF = True) Then
				'do nothing
				'If objRecordSet.RecordCount = 0 Then
			ElseIf objRecordSet.RecordCount = 1 Then
				If objRecordSet.Fields("saMAccountName") = userName Then
					blnUserFound = True
					Set objUser = GetObject ("LDAP://yokdcns01.asia.umuc.edu/CN="& userName &",OU=People,DC=asia,DC=umuc,DC=edu")
				Else
					blnUserFound = False
					WScript.Echo "Email used by: " & objRecordSet.Fields("saMAccountName")
			 	End If
			End If
			blnUser = objRecordSet.RecordCount + blnUser
			objRecordSet.Close
		Next
		WScript.Echo email& ", "&blnUserFound& ", "&blnUser
	
		If blnUserFound Then
				vProxyAddresses = objUser.ProxyAddresses
       			nProxyAddresses = UBound(vProxyAddresses)
       			i = 0
       			Do While i <= nProxyAddresses
          			email = vProxyAddresses(i)
					If Left (email,5) = "smtp:" Then
						If Mid (email,6) = emailAddress Then
							email = cstr("SMTP:" & Mid (email,6))
							vProxyAddresses(i) = email
						Else
							'do nothing
						End If
					ElseIf Left (email,5) = "SMTP:" Then
						If Mid (email,6) = emailAddress Then
							'do nothing
						Else
							email = cstr("smtp:" & Mid (email,6))
							vProxyAddresses(i) = email
						End If
					Else
						'do nothing
					End If
					i = i + 1
          		Loop
          		objUser.ProxyAddresses = vProxyAddresses
          		objUser.displayName = displayName
				objUser.givenName = firstName
				objUser.sn = lastName
				objUser.put "mail", emailAddress
				objUser.PutEx ADS_PROPERTY_UPDATE, "msExchPoliciesExcluded", Array("{26491CFC-9E50-4857-861B-0CB8DF22B5D7}")
				objUser.SetInfo
		Else If blnUser = 0 then
			Set objUser = GetObject ("LDAP://adedcns01.us.umuc.edu/CN="& userName &",OU=People,DC=us,DC=umuc,DC=edu")
			vProxyAddresses = objUser.ProxyAddresses
       		nProxyAddresses = UBound(vProxyAddresses)
			ReDim Preserve vProxyAddresses(nProxyAddresses + 1)
			
			Do While i <= nProxyAddresses
          		email = vProxyAddresses(i)
          		If Left (email,5) = "SMTP:" Then
                	email = cstr("smtp:" & Mid (email,6))
					vProxyAddresses(i) = email
          		End If
          			i = i + 1
          	Loop

          	email = cstr("SMTP:" & emailAddress)
			vProxyAddresses(nProxyAddresses + 1) = email
			objUser.ProxyAddresses = vProxyAddresses
			objUser.displayName = displayName
			objUser.givenName = firstName
			objUser.sn = lastName
			objUser.put "mail", emailAddress
			objUser.PutEx ADS_PROPERTY_UPDATE, "msExchPoliciesExcluded", Array("{26491CFC-9E50-4857-861B-0CB8DF22B5D7}")
			objUser.SetInfo
		Else
			'do nothing
		End If
		End if

Wend

End Sub