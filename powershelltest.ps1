[System.Reflection.Assembly]::LoadWithPartialName("System.DirectoryServices.Protocols")
[System.Reflection.Assembly]::LoadWithPartialName("System.Net")

$c = New-Object -TypeName System.DirectoryServices.Protocols.LdapConnection -ArgumentList "ldapmaster.umuc.edu:636"
$c.SessionOptions.SecureSocketLayer = $true;
$c.SessionOptions.ProtocolVersion = 3
$c.AuthType = [System.DirectoryServices.Protocols.AuthType]::Basic
if ([string]::IsNullOrWhiteSpace($ConnectWithUser))
{
  $ConnectWithUser = Read-Host -Prompt "User:"
}
if ([string]::IsNullOrWhiteSpace($ConnectWithPassword))
{
  $ConnectWithPassword = Read-Host -Prompt "Password:" -AsSecureString
}

$ConnectWithUser = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"

#$ConnectWithUser = "uid="+$ConnectWithUser+",OU=people,dc=emuc,dc=edu"

$credentials = New-Object -TypeName System.Net.NetworkCredential -ArgumentList $ConnectWithUser,$ConnectWithPassword
$c.Bind($credentials)