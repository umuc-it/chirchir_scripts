$users = import-csv "C:\Scratch\test.txt"
$container = [ADSI] "LDAP://OU=Email Access Only,OU=Exchange,dc=us,dc=umuc,dc=edu"
$users | foreach {
    $UserName = $_.name
    $firstname = $_.firstname
    $lastname = $_.lastname
    $samaccountname = $_.samaccountname
    $displayname = $_.displayname
    $description = $_.description

    $newUser = $container.Create("User", "cn=" + $UserName)

    $newUser.Put("firstname", $firstname)
    $newUser.Put("lastname", $lastname)
    $newUser.Put("sAMAccountName", $samaccountname)
    $newUser.Put("displayname", $displayname)
    $newUser.Put("description", $description)

    $newUser.SetInfo
}
