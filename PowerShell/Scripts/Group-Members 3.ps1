﻿$myCol = @()
ForEach ($Group in (Get-QADGroup -SearchRoot "OU=Security Groups,OU=Exchange,DC=us,DC=umuc,DC=edu" -SizeLimit 0))
   {
   ForEach ($Member in (Get-QADGroupMember $Group))
      { 
      $myObj = "" | Select Group, Member
      $myObj.Group = $Group.Name
      $myObj.Member = $Member.Name
      $myCol += $myObj
      }
   }
$myCol | Export-Csv -Path "C:\scratch\securityOU.csv" -NoTypeInformation