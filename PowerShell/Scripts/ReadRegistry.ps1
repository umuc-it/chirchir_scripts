$ComputerName = "adedcns01.us.umuc.edu"
$RegistryKey = "SYSTEM\CurrentControlSet\Services\DNS\parameters"
$RegistryHive = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::LocalMachine, $ComputerName)
$regKey = $RegistryHive.OpenSubKey($RegistryKey)
Write-Host "Sub Keys"
Write-Host "--------"
Foreach($sub in $regKey.GetSubKeyNames()){$sub}
Write-Host
Write-Host "Values"
Write-Host "------"
Foreach($val in $regKey.GetValueNames()){$val}