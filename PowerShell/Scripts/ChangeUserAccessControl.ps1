# Variable Declarations
$ADDomainController = "adedcns02.us.umuc.edu"
$ADPrivilegedUserAccount = "US\mchirchir_eu"

If ((get-pssnapin -Name Quest.ActiveRoles.ADMAnagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

# Establish Active Directory Connection
$ADCredentials = Get-Credential $ADPrivilegedUserAccount
$ADConnection = Connect-QADService -Service $ADDomainController -Credential $ADCredentials

$InputFile = "C:\Scratch\user.txt"

$UserFile = Get-Content $InputFile

ForEach ($User In $UserFile)
{
	$ADUser = Get-QADUser -ObjectAttributes @{samAccountName="$User"}
	Write-Host $ADUser.distinguishedName
	
    # Retrieve values.
    #$DN = $ADUser.properties.Item("distinguishedName")
    #$Flag = $ADUser.properties.Item("userAccountControl")
    # Bind to user object.
    #$ADUser = [ADSI]"LDAP://$DN"
    # Toggle bit off for "Password not required".
    #$Flag = $Flag -bxor 32
    #$ADUser.userAccountControl = $Flag
    # Save updated user object.
    #$ADUser.SetInfo()
}