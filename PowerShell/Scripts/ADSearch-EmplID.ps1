If ((get-pssnapin -Name Quest.ActiveRoles.ADMAnagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

$InputFile = "C:\Scratch\empl.txt"
$OutputFile = "C:\Scratch\ADSearch-EmplID-Output.csv"

$EMPLFile = Get-Content $InputFile

Set-Content $OutputFile "SurName,GivenName,DisplayName,SamAccountName,EmployeeID,Email,AccountStatus,Location,LastLogonTimestamp"

ForEach ($EMPLID in $EMPLFile) {
	$ADUser = Get-QADUser -ObjectAttributes @{employeeID="$EMPLID"}
	If ($ADUser -ne $null) {
		#Write-Host $ADUser.sn
		#Write-Host $ADUSer.givenname
		#Write-Host $ADUser.displayname
		#Write-Host $ADUser.samaccountname
		#Write-Host $ADUSer.employeeid
		#Write-Host $ADUSer.mail
		$ResultSN = $ADUser.sn
		$ResultGivenName = $ADUSer.givenname
		$ResultDisplayName = $ADUser.displayname
		$ResultSamAccountName = $ADUser.samaccountname
		$ResultEmployeeID = $ADUSer.employeeid
		$ResultMail = $ADUSer.mail
		$ResultPC = $ADUser.ParentContainer
		$TempStatus = $ADUser.AccountIsDisabled
		If ($TempStatus -eq $True) {
			$ResultStatus = "Disabled"
			}
		Else {
			$ResultStatus = "Enabled"
			}
		If ($ADUser.lastlogontimestamp -ne $NULL) {
			$ResultLLD = "{0:G}" -f [DateTime]$ADUser.lastlogontimestamp
		}
		Add-Content $OutputFile "$ResultSN,$ResultGivenName,$ResultDisplayName,$ResultSamAccountName,$ResultEmployeeID,$ResultMail,$ResultStatus,$ResultPC,$ResultLLD"
	}
}