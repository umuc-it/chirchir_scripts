[reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null
if (!$wsus) {
        $wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer('adema03','true',8531);
}

"Server name: " +$wsus.Name;
"Using secure connection: " + $wsus.IsConnectionSecureForRemoting;
"Port number: " + $wsus.PortNumber;

$updateScope = new-object Microsoft.UpdateServices.Administration.UpdateScope;
$updateScope.UpdateApprovalActions = [Microsoft.UpdateServices.Administration.UpdateApprovalActions]::Install `
 -bor [Microsoft.UpdateServices.Administration.UpdateApprovalActions]::Uninstall

$wsus.GetUpdates($updateScope) | foreach {
     $update = $_          #$_ is a shorthand variable for the current item in a loop

     "--------------------------------------------------"     
     "Update: " + $update.Title
     "Update Id: " + $update.Id.UpdateId
     "--------------------------------------------------"     

     $wsus.GetComputerTargetGroups() | foreach {
          $group = $_
          if ($update.GetUpdateApprovals($group).Count -ne 0)
          {
               #`t is equivalent to \t in C#/C++
               "`t Computer Group: " + $group.Name
               "`t ComputerTargetGroupId: " + $group.Id
               "`t Approval action: " + $update.GetUpdateApprovals($group)[0].Action
            
               $summary = $update.GetSummaryForComputerTargetGroup($group)

               "`t Installed:" + ($Summary.InstalledCount + $summary.NotApplicableCount)

               $neededCount = ($summary.InstalledPendingRebootCount + $summary.NotInstalledCount)
               if ($neededCount -gt 0)
               {
                    Write-Host ("`t Needed: $neededCount") -foregroundColor Yellow -backgroundColor black
               } else {
                    "`t Needed: $neededCount"
               }

               if ($summary.FailedCount -gt 0)
               {
                    Write-Host ("`t Failed: " + $summary.FailedCount) -foregroundColor Red -backgroundColor black
               } else {
                    "`t Failed: " + $summary.FailedCount
               }
               
               "`t Unknown: " + $summary.UnknownCount
               "`t ------------------------------------------"
          }
     }
     "--------------------------------------------------"
}