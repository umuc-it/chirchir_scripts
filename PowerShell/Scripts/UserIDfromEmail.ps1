# Variable Declarations
$ADDomainController = "adedcns02.us.umuc.edu"
$ADPrivilegedUserAccount = "US\mchirchir_eu"

If ((get-pssnapin -Name Quest.ActiveRoles.ADMAnagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

# Establish Active Directory Connection
$ADCredentials = Get-Credential $ADPrivilegedUserAccount
$ADConnection = Connect-QADService -Service $ADDomainController -Credential $ADCredentials

$InputFile = "C:\Scratch\user.txt"
$OutputFile = "C:\Scratch\userID.txt"
$UserFile = Get-Content $InputFile
#$UserFile = Get-Qadgroupmember test-contact@origin.umuc.edu

ForEach ($samID in $UserFile) {
	$contactInfo = Get-QADObject -Identity $samID -IncludeAllProperties
	$contactInfo.employeeID | Out-File 	$OutputFile -append
	#$contactInfo.samAccountName | Out-File 	$OutputFile -append
	#$contactInfo.distinguishedName | Out-File 	$OutputFile -append
	#$GroupMembers = Get-QADGroup -IncludeAllProperties -objectattributes @{mail=$samID} | Get-QADGroupMember | Select-Object
	#foreach($member in $GroupMembers)
	#{
	#	Write-Host $members.distinguishedName
	#}
	#$Groupinfo = Get-QADGroup -IncludeAllProperties -objectattributes @{mail=$samID}
	#Write-Host $Groupinfo.distinguishedName
}