Import-Module AWSPowerShell

$outputFile = 'C:\Scratch\PowerShell\Scripts\Amazon\awsInstances.csv'

Set-Content $OutputFile 'Region,InstanceName,InstanceDomain,InstanceDesc,InstanceEnv,InstanceApp,InstanceRole,InstanceDept,InstancePOC,InstanceReservationID,InstanceId,InstanceType,LaunchTime,KernelId,Platform,SubnetId,VpcId,PrivateIpAddress,PublicIpAddress'

Get-AWSRegion | ForEach {
	Write-Host "Checking Region:" $_.Region
	$CurrentRegion = $_.Region
	Get-EC2Instance -Region $CurrentRegion | select -Expand Instances | ForEach {
		$CurrentInstance = $_.InstanceId
		$InstanceTags = $Null
		$InstanceTags = Get-EC2Instance -Region $CurrentRegion | select -Expand Instances | Where-Object {$_.InstanceId -eq $CurrentInstance} | select -Expand Tags
		$InstanceTags | ForEach {
			If ($_.Key -eq "Name") {
				$InstanceName = $_.Value
			}
			If ($_.Key -eq "Domain") {
				$InstanceDomain = $_.Value
			}
			If ($_.Key -eq "Description") {
				$InstanceDesc = $_.Value
			}
			If ($_.Key -eq "Environment") {
				$InstanceEnv = $_.Value
			}
			If ($_.Key -eq "Application") {
				$InstanceApp = $_.Value
			}
			If ($_.Key -eq "Role") {
				$InstanceRole = $_.Value
			}
			If ($_.Key -eq "Department") {
				$InstanceDept = $_.Value
			}
			If ($_.Key -eq "POC") {
				$InstancePOC = $_.Value
			}
			If ($_.Key -eq "ReservationID") {
				$InstanceReservationID = $_.Value
			}
		}
		Write-Host "   Found Instance:" ($InstanceName + " (" + $CurrentInstance + ")")
		Add-Content $Outputfile ($CurrentRegion + "," + $InstanceName + "," + $InstanceDomain + "," + $InstanceDesc + "," + $InstanceEnv + "," + $InstanceApp + "," + $InstanceRole + "," + $InstancePOC + "," + $InstanceDept + "," + $InstanceReservationID + "," + $_.InstanceId + "," + $_.InstanceType + "," + $_.LaunchTime + "," + $_.KernelId + "," + $_.Platform + "," + $_.SubnetId + "," + $_.VpcId + "," + $_.PrivateIpAddress + "," + $_.PublicIpAddress)
	}
}