$ServerName = "ADEMA03"
$ReportDate = Get-Date
$ReportFile = "C:\Scratch\" + $ServerName + " WSUS Report - Missing Patches - " + (get-date -uformat "%Y%m%d") + ".txt"

[reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null

if (!$wsus) {
        $wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer($ServerName,'true',8531);
}

$computerScope = new-object Microsoft.UpdateServices.Administration.ComputerTargetScope;
$computerScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::NotInstalled;

$updateScope = new-object Microsoft.UpdateServices.Administration.UpdateScope;
$updateScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::NotInstalled;

$computers = $wsus.GetComputerTargets($computerScope);

Set-Content -path $ReportFile -value "ADEMA03 WSUS Report - Missing Patches"
Add-Content -path $ReportFile -value "Report Generated: $ReportDate"
Add-Content -path $ReportFile -value "`n"

$computers |Sort FullDomainName| foreach-object {
                Add-Content -path $ReportFile -value $_.FullDomainName;

                $updatesForReboot = $_.GetUpdateInstallationInfoPerUpdate($updateScope);
                $updatesForReboot | foreach-object {
                $neededUpdate = $wsus.GetUpdate($_.UpdateId);
                Add-Content -path $ReportFile -value ("   " + $neededUpdate.Title);
                # Remove previous lines to only show computers that need updates
     }
}

$SmtpClient = new-object system.net.mail.smtpClient
$MailMessage = New-Object system.net.mail.MailMessage
$SmtpClient.Host = "bulkmail.umuc.edu"
$MailMessage.from = "PowerShell@umuc-23465.us.umuc.edu"
$MailMessage.To.add("jtaylor@umuc.edu")
$MailMessage.Subject = "ADEMA03 WSUS Report"
$MailMessage.Body = "The attached report lists each server that is missing patches followed by the "
$MailMessage.priority = "normal"
$MailMessage.Attachments.Add($ReportFile)
$MailMessage.IsBodyHtml = 0
$Smtpclient.Send($MailMessage)
