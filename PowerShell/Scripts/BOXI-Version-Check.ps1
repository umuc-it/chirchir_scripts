$BOBJServices = get-wmiobject win32_service -computer adedvws05 | where-object { $_.Name -match "^BOBJ.*" }
foreach ($Service in $BOBJServices) {
	Write-Host $Service.Name
	Write-Host $Service.Caption
	Write-Host $Service.PathName
}
