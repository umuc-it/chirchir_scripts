[reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null

if (!$wsus) {
        $wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer('adema03','true',8531);
}

$ComputerScope = new-object Microsoft.UpdateServices.Administration.ComputerTargetScope;
$ComputerScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::NotInstalled `
	-bor [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::Downloaded `
	-bor [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::InstalledPendingReboot;

$UpdateScope = new-object Microsoft.UpdateServices.Administration.UpdateScope;
$UpdateScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::NotInstalled `
	-bor [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::Downloaded `
	-bor [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::InstalledPendingReboot;
$UpdateScope.ApprovedStates = [Microsoft.UpdateServices.Administration.ApprovedStates]::LatestRevisionApproved `
	-bor [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::HasStaleUpdateApprovals;

$Computers = $wsus.GetComputerTargets($ComputerScope);

$Computers | Sort-Object $_.FullDomainName | ForEach-Object {
	Write-Host $_.FullDomainName
	$UpdatesForReboot = $_.GetUpdateInstallationInfoPerUpdate($UpdateScope);
	$UpdatesForReboot | foreach-object {
		Write-Host ($wsus.GetUpdate($_.UpdateId)).Title;
		$wsus.GetUpdate($_.UpdateId)|select *|write-host;		
	}
}

