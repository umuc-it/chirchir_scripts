$PRDDomainController = "heidcns01.europe.umuc.edu"
$PRDUserAccount = "jtaylor9@us.umuc.edu"
$PRDPeopleRoot = "europe.umuc.edu/People"
$PRDInactiveRoot = "europe.umuc.edu/Inactive"
$PeopleFile = "C:\IdM-Users-Europe-People.xml"
$InactiveFile = "C:\IdM-Users-Europe-Inactive.xml"

$StartTime = Get-Date

$PRDCredentials = Get-Credential $PRDUserAccount
$PRDConnection = Connect-QADService -Service $PRDDomainController -Credential $PRDCredentials

#$ADSearchResults = Get-QADUser -Connection $PRDConnection -LdapFilter 'sn=z*' -SizeLimit 10000 -SearchRoot $PRDPeopleRoot -SerializeValues -IncludedProperties cn,sn,title,givenName,displayName,employeeID,sAMAccountName,userPrincipalName,description,physicalDeliveryOfficeName,scriptPath
$ADSearchResults = Get-QADUser -Connection $PRDConnection -SizeLimit 10000 -SearchRoot $PRDPeopleRoot -SerializeValues -IncludedProperties cn,sn,title,givenName,displayName,employeeID,sAMAccountName,userPrincipalName,description,physicalDeliveryOfficeName,scriptPath
$ADSearchResults | Export-CliXML $PeopleFile
Remove-Variable ADSearchResults

#$ADSearchResults = Get-QADUser -Connection $PRDConnection -LdapFilter 'sn=z*' -SizeLimit 10000 -SearchRoot $PRDInactiveRoot -SerializeValues -IncludedProperties cn,sn,title,givenName,displayName,employeeID,sAMAccountName,userPrincipalName,description,physicalDeliveryOfficeName,scriptPath
$ADSearchResults = Get-QADUser -Connection $PRDConnection -SizeLimit 10000 -SearchRoot $PRDInactiveRoot -SerializeValues -IncludedProperties cn,sn,title,givenName,displayName,employeeID,sAMAccountName,userPrincipalName,description,physicalDeliveryOfficeName,scriptPath
$ADSearchResults | Export-CliXML $InactiveFile
Remove-Variable ADSearchResults

Disconnect-QADService -Connection $PRDConnection

$EndTime = Get-Date
$ElapsedTime = New-TimeSpan -Start $StartTime -End $EndTime

Write-Host "Start Time: " $StartTime
Write-Host "End Time: " $EndTime
Write-Host "Elapsed Time: " $ElapsedTime

Remove-Variable PRDDomainController
Remove-Variable PRDUSerAccount
Remove-Variable PRDPeopleRoot
Remove-Variable PRDInactiveRoot
Remove-Variable PeopleFile
Remove-Variable InactiveFile
Remove-Variable StartTime
Remove-Variable EndTime
Remove-Variable ElapsedTime