$ComputerList = get-content C:\Blah.txt
foreach ($ComputerName in $ComputerList) {
	$ComputerName = $ComputerName.replace(" ", "")
	get-wmiobject win32_localtime -computer adedcns01,adedcns02,adedcns97,$ComputerName|select-object __server, Hour, Minute, Second|export-csv c:\timeout.csv
}