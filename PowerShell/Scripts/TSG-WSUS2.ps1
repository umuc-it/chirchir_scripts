[reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null

if (!$wsus) {
        $wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer('adema03','true',8531);
}

$computerScope = new-object Microsoft.UpdateServices.Administration.ComputerTargetScope;
$computerScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::InstalledPendingReboot;

$updateScope = new-object Microsoft.UpdateServices.Administration.UpdateScope;
$updateScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::InstalledPendingReboot;

$computers = $wsus.GetComputerTargets($computerScope);

$computers | foreach-object {
                $_.FullDomainName | write-host;

                # Show which updates are causing the reboot required for the computer.  Remove the next 5 lines to only generate a list of computers.
                $updatesForReboot = $_.GetUpdateInstallationInfoPerUpdate($updateScope);
                $updatesForReboot | foreach-object {
                $neededUpdate = $wsus.GetUpdate($_.UpdateId);
                "   "+$neededUpdate.Title | write-host;
                # Remove previous lines to only show computers that need updates
     }
}