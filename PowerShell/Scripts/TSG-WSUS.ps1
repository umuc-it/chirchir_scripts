$ReportDate = Get-Date

Set-Content $ENV:SystemDrive\Scratch\MissingUpdatesBrief.csv """Server"",""Patches Needed"""

[reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null

if (!$wsus) {
        $wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer('adema03','true',8531);
}

$ComputerScope = new-object Microsoft.UpdateServices.Administration.ComputerTargetScope;
$ComputerScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::NotInstalled;

$UpdateScope = new-object Microsoft.UpdateServices.Administration.UpdateScope;
$UpdateScope.IncludedInstallationStates = [Microsoft.UpdateServices.Administration.UpdateInstallationStates]::NotInstalled;

$Computers = $wsus.GetComputerTargets($ComputerScope);

$Computers | ForEach-Object {
$WsusComputerName = $_.FullDomainName
$UpdatesForReboot = $_.GetUpdateInstallationInfoPerUpdate($UpdateScope);
$UpdatesForReboot | foreach-object {
$NeededUpdate = $wsus.GetUpdate($_.UpdateId);
$WsusUpdateCount = $UpdatesForReboot.Count
}
Write-Output """$WsusComputerName"",""$WsusUpdateCount""" | Out-File $ENV:SystemDrive\Scratch\MissingUpdatesBrief.csv -encoding ASCII -append -width 512

}

$ReportSorted = import-csv $ENV:SystemDrive\Scratch\MissingUpdatesBrief.csv | Sort Server
$ReportSorted | export-csv $ENV:SystemDrive\Scratch\MissingUpdatesBrief.csv -notype -force

$SmtpClient = new-object system.net.mail.smtpClient
$MailMessage = New-Object system.net.mail.MailMessage
$SmtpClient.Host = "bulkmail.umuc.edu"
$MailMessage.from = "PowerShell@umuc-23465.us.umuc.edu"
$MailMessage.To.add("jtaylor@umuc.edu")
# $MailMessage.Bcc.add("SOMEONE@YOURDOMAIN.COM")
$MailMessage.Subject = "Computer Status Brief Report"

$MailMessage.Body = "
<html><body>
<dl>
  <dd>
    <em>Please find the attached report detailing the status of all servers requiring updates.<br>
    <br>
        Servers that are up to date will not be listed on this report.
    <br>
    <br>
    <br>
    <br>
    <br>
        Report time: $ReportDate
    <em>
  </dd>
</dl>
</body></html>
"

$MailMessage.priority = "high"
$MailMessage.IsBodyHtml = 1
$MailMessage.Attachments.Add("$ENV:SystemDrive\Scratch\MissingUpdatesBrief.csv")
$Smtpclient.Send($MailMessage)

exit