$PSGroups = get-qadgroup ps_*
foreach ($GRPName in $PSGroups) {
	get-qadgroupmember $GRPName|sort-object lastname|ft displayname, name|out-file -filepath C:\Scratch\PSGroups\$GRPName.txt
	}