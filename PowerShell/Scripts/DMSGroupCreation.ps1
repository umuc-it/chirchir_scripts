$ADCred = get-credential jtaylor9da@us.umuc.edu

new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'domainlocal' -Name 'Singularity-EDSA' -description 'Singularity Europe Transcripts Scan/Index/Verify Users' -samaccountname 'Singularity-EDSA'
new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'global' -Name 'Singularity-EDSA-US' -description 'Singularity Europe Transcripts Scan/Index/Verify Users' -samaccountname 'Singularity-EDSA-US'

new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'domainlocal' -Name 'Singularity-EDSA-QAT' -description 'Singularity Europe Transcripts Scan/Index/Verify Users (QAT)' -samaccountname 'Singularity-EDSA-QAT'
new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'global' -Name 'Singularity-EDSA-QAT-US' -description 'Singularity Europe Transcripts Scan/Index/Verify Users (QAT)' -samaccountname 'Singularity-EDSA-QAT-US'

new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'domainlocal' -Name 'Singularity-EDSA-Development' -description 'Singularity Europe Transcripts Scan/Index/Verify Users (Development)' -samaccountname 'Singularity-EDSA-Development'
new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'global' -Name 'Singularity-EDSA-Development-US' -description 'Singularity Europe Transcripts Scan/Index/Verify Users (Development)' -samaccountname 'Singularity-EDSA-Development-US'

new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'domainlocal' -Name 'Singularity-ADSA' -description 'Singularity Asia Transcripts Scan/Index/Verify Users' -samaccountname 'Singularity-ADSA'
new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'global' -Name 'Singularity-ADSA-US' -description 'Singularity Asia Transcripts Scan/Index/Verify Users' -samaccountname 'Singularity-ADSA-US'

new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'domainlocal' -Name 'Singularity-ADSA-QAT' -description 'Singularity Asia Transcripts Scan/Index/Verify Users (QAT)' -samaccountname 'Singularity-ADSA-QAT'
new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'global' -Name 'Singularity-ADSA-QAT-US' -description 'Singularity Asia Transcripts Scan/Index/Verify Users (QAT)' -samaccountname 'Singularity-ADSA-QAT-US'

new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'domainlocal' -Name 'Singularity-ADSA-Development' -description 'Singularity Asia Transcripts Scan/Index/Verify Users (Development)' -samaccountname 'Singularity-ADSA-Development'
new-qadgroup -service "us.umuc.edu" -credential $ADCred -parentcontainer 'ou=people,dc=us,dc=umuc,dc=edu' -grouptype 'security' -groupscope 'global' -Name 'Singularity-ADSA-Development-US' -description 'Singularity Asia Transcripts Scan/Index/Verify Users (Development)' -samaccountname 'Singularity-ADSA-Development-US'

