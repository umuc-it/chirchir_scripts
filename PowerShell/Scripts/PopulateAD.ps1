#requires -version 2.0
#requires -PSSnapin Quest.ActiveRoles.ADManagement

# Jeffery Hicks
# http://jdhitsolutions.com/blog
# follow on Twitter: http://twitter.com/JeffHicks
#
# Learn more Windows PowerShell with a copy of Windows PowerShell 2.0: TFM (SAPIEN Press 2010)
#
# "Those who forget to script are doomed to repeat their work."

#  ****************************************************************
#  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
#  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
#  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
#  * DO NOT USE IT OUTSIDE OF A SECURE, TEST SETTING.             *
#  ****************************************************************

#this script uses the CSV file created with ProvisionAD.ps1

[cmdletBinding(SupportsShouldProcess=$True)]

Param(
    [Parameter(Position=0,ValueFromPipeline=$False,Mandatory=$True,
    HelpMessage="Enter the full filename and path to a CSV file.")]
    [ValidateScript({Test-Path -Path $_})]
    [string]$File,
    [Parameter(Position=1,ValueFromPipeline=$False,Mandatory=$False,
    HelpMessage="Enter the parent container distinguished name")]
    [string]$Parent="OU=Email Access Only,OU=Exchange,DC=us,DC=umuc,DC=edu",
    [Parameter(ValueFromPipeline=$False,Mandatory=$False,
    HelpMessage="Enter a default password for the new account")]
    [string]$Password="Password99",
    [switch]$UserMustChangePassword,
    [switch]$Disable
)

#####################################################################
#Main code
#####################################################################

Try {
    Write-Verbose "Validating $Parent"
    $OU=Get-QADObject -identity $parent
}

Catch {
    Write-Warning "Failed to find or verify $parent"
}

if ($UserMustChangePassword) {
    $change=$True
    Write-Verbose "New accounts must change password at next logon"
}
else {
    $change=$false
}

if ($Disable) {
    Write-Verbose "New accounts will be disabled"
}
#proceed only after the parent container has been validated
$total=0

if ($OU) {
    Write-Verbose "Importing users from $file"
    Write-Verbose "Creating new user accounts in $Parent"

    Import-Csv -Path $file | foreach { 
        #test if account already exists
        if (Get-QADuser -Identity $_.samaccountname) {
            Write-Warning "Skipping $($_.Samaccountname)"
        }
        else {   
            $total++ 
            New-QADuser -ParentContainer $parent `
			-name $_.name -firstname $_.firstname -lastname $_.lastname `
            -samaccountname $_.samaccountname `
            -displayname $_.Displayname -description $_.description -userpassword $Password  `
            -outvariable newUser | Set-QADUser -UserMustChangePassword $change 
            
             if ($Disable) {
                Disable-QADuser -identity $newUser[0].DN
              }
        } #end Else
 } #end foreach
} #end if

Write-Host -Object "Created $total new accounts in $Parent" -ForegroundColor Green
Write-Verbose "Script complete."