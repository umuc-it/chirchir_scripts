$SMTPClient = new-object system.net.mail.smtpClient
$MailMessage = New-Object system.net.mail.mailmessage
$SMTPClient.Host = "bulkmail.umuc.edu"
$MailMessage.from = ("powershell@umuc-23465.us.umuc.edu")
$MailMessage.To.add("tsg@umuc.edu")

$ServerArray = "adeprws12.us.umuc.edu", "adedvws05.us.umuc.edu"
$ServiceArray = "BOBJCentralMS"

$CurrentDate = get-date

foreach ($ServerName in $ServerArray) {
	foreach ($ServiceName in $ServiceArray) {
		if ((get-wmiobject win32_service -computer $ServerName -filter "name='$ServiceName'").State -eq "Stopped") {
			#write-host "Service, $ServiceName, on $ServerName is stopped.  Restarting."
			$MailMessage.Subject = “BusinessObjects XI Service Check - Service Failure”
			$MailMessage.Body = “$CurrentDate`n$ServiceName on $ServerName is stopped.  Attempting restart.”
			$SMTPClient.Send($MailMessage)
			(get-wmiobject win32_service -computer $ServerName -filter "name='$ServiceName'").StartService()
			
		} else {
			#write-host "Service, $ServiceName, on $ServerName is running."
			#$MailMessage.Subject = “BusinessObjects XI Service Check - Service Running”
			#$MailMessage.Body = “$CurrentDate`n$ServiceName on $ServerName is running.”
			#$SMTPClient.Send($MailMessage)
		}
   	Start-Sleep 5
   }
  	Start-Sleep 5
}
