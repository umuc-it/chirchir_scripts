$QATDomainController = "heiqadcns01.qat-europe.umuc.edu"
$QATUserAccount = "jtaylor9da@qat-europe.umuc.edu"
$QATDomainName = "qat-europe.umuc.edu"
$QATPeopleRoot = "qat-europe.umuc.edu/People"
$QATPeopleContainer = "OU=People,DC=qat-europe,DC=umuc,dc=edu"
$QATInactiveRoot = "qat-europe.umuc.edu/Inactive"
$QATInactiveContainer = "OU=Inactive,DC=qat-europe,DC=umuc,dc=edu"
$PeopleFile = "C:\IdM-Users-Europe-People.xml"
$InactiveFile = "C:\IdM-Users-Europe-Inactive.xml"

$StartTime = Get-Date

$QATCredentials = Get-Credential $QATUserAccount
$QATConnection = Connect-QADService -Service $QATDomainController -Credential $QATCredentials

$PeopleAccounts = Import-CliXML $PeopleFile

ForEach ($User in $PeopleAccounts) {
	$UserFQDN = "CN=" + $User.cn + "," + $QATPeopleContainer
	Write-Host "Creating Account: " $UserFQDN
	New-QADUser -Connection $QATConnection -name $User.cn -ParentContainer $QATPeopleContainer -SamAccountName $User.samaccountname -UserPassword 'P@ssword1' | Out-Null
	While ((Get-QADUser -Identity $UserFQDN | Measure-Object).Count -ne '1') {
		Write-Host "Account Does Not Exist Yet, Pausing and Retrying."
		Start-Sleep 1
	}
	Write-Host "Setting Attributes on Account: " $UserFQDN
	Set-QADUser -Connection $QATConnection -Identity $UserFQDN -ObjectAttributes @{sn=$User.sn;title=$User.title;givenName=$User.givenName;displayName=$User.displayName;employeeID=$User.employeeID;userPrincipalName=($user.cn + "@" + $QATDomainName);description=$User.description;physicalDeliveryOfficeName=$User.physicalDeliveryOfficeName;scriptPath=$User.scriptPath} | Out-Null
	Write-Host "Enabling Account: " $UserFQDN
	Enable-QADUser -Connection $QATConnection -Identity $UserFQDN | Out-Null
	Write-Host
	Remove-Variable UserFQDN
}

Remove-Variable PeopleAccounts

$InactiveAccounts = Import-CliXML $InactiveFile

ForEach ($User in $InactiveAccounts) {
	$UserFQDN = "CN=" + $User.cn + "," + $QATInactiveContainer
	Write-Host "Creating Account: " $UserFQDN
	New-QADUser -Connection $QATConnection -name $User.cn -ParentContainer $QATInactiveContainer -SamAccountName $User.samaccountname -UserPassword 'P@ssword1' | Out-Null
	While ((Get-QADUser -Identity $UserFQDN | Measure-Object).Count -ne '1') {
		Write-Host "Account Does Not Exist Yet, Pausing and Retrying."
		Start-Sleep 1
	}
	Write-Host "Setting Attributes on Account: " $UserFQDN
	Set-QADUser -Connection $QATConnection -Identity $UserFQDN -ObjectAttributes @{sn=$User.sn;title=$User.title;givenName=$User.givenName;displayName=$User.displayName;employeeID=$User.employeeID;userPrincipalName=($user.cn + "@" + $QATDomainName);description=$User.description;physicalDeliveryOfficeName=$User.physicalDeliveryOfficeName;scriptPath=$User.scriptPath} | Out-Null
	Write-Host
	Remove-Variable UserFQDN
}

Remove-Variable InactiveAccounts

Disconnect-QADService -Connection $QATConnection

$EndTime = Get-Date
$ElapsedTime = New-TimeSpan -Start $StartTime -End $EndTime

Write-Host "Start Time: " $StartTime
Write-Host "End Time: " $EndTime
Write-Host "Elapsed Time: " $ElapsedTime

Remove-Variable QATDomainController
Remove-Variable QATUSerAccount
Remove-Variable QATDomainName
Remove-Variable QATPeopleRoot
Remove-Variable QATPeopleContainer
Remove-Variable QATInactiveRoot
Remove-Variable QATInactiveContainer
Remove-Variable PeopleFile
Remove-Variable InactiveFile
Remove-Variable StartTime
Remove-Variable EndTime
Remove-Variable ElapsedTime