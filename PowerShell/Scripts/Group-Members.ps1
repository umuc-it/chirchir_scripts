﻿	If ((get-pssnapin -Name Quest.ActiveRoles.ADMAnagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

$OutputFile = "C:\Scratch\output.csv"
Set-Content $OutputFile "Username,DisplayName,Email"

$identity = $args[0]
if(!$Identity)	
{
	$identity = Read-Host "Enter the Identity (Group Email Address)"
}
$groupmembers = Get-QADGroupMember $identity
$count = 0
$group = Get-QADGroup $identity
Write-host $group.DisplayName
Write-Host "-------------------------------------------------------------------------------"
foreach ($user in $groupmembers)
  { 	$count++
    	$ResultFN = $user.FirstName
	$ResultLN = $user.LastName
	$ResultDN = $user.DisplayName
	$ResultEm = $user.Email
	$ResultSam = $user.samAccountName
	Write-host $ResultLN "," $ResultFN "," $ResultEm "," $ResultSam
}