
If ((get-pssnapin -Name Quest.ActiveRoles.ADMAnagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

$InputFile = "C:\Scratch\user.txt"
$OutputFile = "C:\Scratch\ADSearch-Phonebook-UserID-Output.csv"

$UserFile = Get-Content $InputFile

Set-Content $OutputFile "SurName|GivenName|DisplayName|SamAccountName|title|department|streetAddress|ipPhone|telephoneNumber|EmployeeID|Email|AccountStatus|Location|LastLogonTimestamp"

ForEach ($UserID in $UserFile) {
	$ADUser = Get-QADUser -ObjectAttributes @{samAccountName="$UserID"} -IncludeAllProperties
	If ($ADUser -ne $null) {
		#Write-Host $ADUser.sn
		#Write-Host $ADUSer.givenname
		#Write-Host $ADUser.displayname
		#Write-Host $ADUser.samaccountname
		#Write-Host $ADUser.title
		#Write-Host $ADUser.department
		#Write-Host $ADUser.streetAddress
		#Write-Host $ADUser.telephoneNumber
		#Write-Host $ADUser.ipPhone
		#Write-Host $ADUSer.employeeid
		#Write-Host $ADUSer.mail
		$ResultSN = $ADUser.sn
		$ResultGivenName = $ADUSer.givenname
		$ResultDisplayName = $ADUser.displayname
		$ResultSamAccountName = $ADUser.samaccountname
		$Resulttitle = $ADUser.title
		$Resultdepartment = $ADUser.department
		$ResultstreetAddress = $ADUser.streetAddress
		$ResultipPhone = $ADUser.ipPhone
		$ResulttelephoneNumber = $ADUser.telephoneNumber
		[string] $ResultEmployeeID = $ADUSer.employeeid
		$ResultMail = $ADUSer.mail
		$ResultPC = $ADUser.ParentContainer
		$TempStatus = $ADUser.AccountIsDisabled
		If ($TempStatus -eq $True) {
			$ResultStatus = "Disabled"
			}
		Else {
			$ResultStatus = "Enabled"
			}
		If ($ADUser.lastlogontimestamp -ne $NULL) {
			$ResultLLD = "{0:G}" -f [DateTime]$ADUser.lastlogontimestamp
		}
		Add-Content $OutputFile "$ResultSN|$ResultGivenName|$ResultDisplayName|$ResultSamAccountName|$Resulttitle|$Resultdepartment|$ResultstreetAddress|$ResultipPhone|$ResulttelephoneNumber|$ResultEmployeeID|$ResultMail|$ResultStatus|$ResultPC|$ResultLLD"
	}
}
