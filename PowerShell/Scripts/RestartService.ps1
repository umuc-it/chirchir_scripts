#$ServerName = "adepsap01.us.umuc.edu"
#$ServiceName = "BOBJCentralMS"

$ServerName = "umuc-s004232.us.umuc.edu"
$ServiceName = "McAfeeFramework"

if ((get-wmiobject win32_service -computer $ServerName -filter "name='$ServiceName'").State -eq "Stopped") {
	write-host "Service, $ServiceName, stopped.  Restarting."
	(get-wmiobject win32_service -computer $ServerName -filter "name='$ServiceName'").StartService()
	} else {
	write-host "Service, $ServiceName, running."
	}
