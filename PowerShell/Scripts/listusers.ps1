$LDAPDomain=""

#--------------------------
#Main Variables
#--------------------------
$Domain=[ADSI]"$LDAPDomain"
$DomainName = [string]$Domain.name
$DomainName = $DomainName.ToUpper()
$nCurrDepth=0

#--------------------------
#Functions 
#--------------------------
	function Get-ScriptDirectory
	{
		$Invocation = (Get-Variable MyInvocation -Scope 1).Value
		Split-Path $Invocation.MyCommand.Path
	}
	 

	function fn_FindDomainGroup([string]$GroupName)
	{
		$searcher=new-object DirectoryServices.DirectorySearcher([ADSI]"")
		$filter="(&(objectCategory=group)(samAccountName=$GroupName))"
		$searcher.filter=$filter
		$Results = $searcher.findall()
		
		$GroupLdapPath = ""
		
		if ([int32]$Results.Count -gt [int32]0){
	
			$Result = $Results[0]
			$GroupLdapPath=$result.properties['adspath']
		}
		Return $GroupLdapPath	
	}

	function fn_FindDomainGroup([string]$GroupName)
	{
		$arStr = $GroupName.split("\")
		$sGroup = $arStr[1]	
		$sDomain = $arStr[0]	
		$searcher=new-object DirectoryServices.DirectorySearcher([ADSI]"")
		$filter="(&(objectCategory=group)(samAccountName=$sGroup))"
		$searcher.filter=$filter
		$Results = $searcher.findall()
		
		$GroupLdapPath = ""
		
		if ([int32]$Results.Count -gt [int32]0){
	
			$Result = $Results[0]
			$GroupLdapPath=$result.properties['adspath']
		}
		Return $GroupLdapPath	
	}

	function fn_GetLocalGroupMembers([string]$GroupPath)
	{
		
		$MemberNames = @()
		$GroupName = $GroupPath.Replace("\","/")
		$Group= [ADSI]"WinNT://$GroupName,group"
		$Members = @($Group.psbase.Invoke("Members"))
		$Members | ForEach-Object {
			$Member = $_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)
			$Type = $_.GetType().InvokeMember("Class", 'GetProperty', $null, $_, $null)
			$Path = $_.GetType().InvokeMember("AdsPath", 'GetProperty', $null, $_, $null)
			$arPath = $Path.Split("/")
			$Domain = $arPath[$arPath.length - 2]	
			$Domain = [string]$Domain.ToUpper()
			$Member = $Domain + "\" + $Member
			$DisplayString = $Type.ToUpper() + ":  " + $Member + ";" 
			$MemberNames += $DisplayString
		}
		Return $MemberNames


	}

	function fn_IsLocalGroup([string]$GroupName)
	{
		$blIsLocalGroup = "false"
		$hostname = hostname
		$arStr = $GroupName.split("\")
		$sGroup = $arStr[1]	
		$sDomain = $arStr[0]	

		if ($hostname.ToUpper() -eq $sGroup.ToUpper()) {
			$blisLocalGroup ="true"
			
		}
		elseif ($sDomain.ToUpper() -eq  $DomainName.ToUpper())
		{
			$blisLocalGroup ="false"
		}else
		{
			$searchresult = fn_FindDomainGroup($sGroup)
			if ($searchresult.length -gt 0) {
				$blisLocalGroup ="false"
			}else{
				$blisLocalGroup ="true"
			}
		}
		return $blIsLocalGroup
	}

	function fn_GetLDAPGroupMembers([string]$GroupLdap)
	{
		$MemberNames = @()
		$ADGroup=[ADSI]"$GroupLdap"
		$GroupSam = $ADGroup.samAccountName
		
		#Get group members		
		$Members = @($ADGroup.psbase.Invoke("Members"))
		$Members | ForEach-Object {
			$Member = $_.GetType().InvokeMember("samAccountName", 'GetProperty', $null, $_, $null)
			$Type = $_.GetType().InvokeMember("Class", 'GetProperty', $null, $_, $null)
			$Path = $_.GetType().InvokeMember("AdsPath", 'GetProperty', $null, $_, $null)
			$Member = $DomainName + "\" + $Member
			$DisplayString = $Type.ToUpper() + ":  " + $Member + ";" + $Path
			$MemberNames += $DisplayString 
		} 
		Return $MemberNames
	
	}

	function fn_GetGroupMembers([string]$GroupName, [string]$isLocalGroup, [string]$AdsPath)
	{
		$sTitle = "Members of the Group: " + $GroupName
		write-host 
		write-host $sTitle
		write-host "-----------------------------------------------" 

		""  | out-file  -append $OutFile
		$sTitle | out-file  -append $OutFile
		"-----------------------------------------------"  | out-file  -append $OutFile
		$MemberNames = @()
		if ($isLocalGroup -eq "true"){
			$MemberNames =fn_GetLocalGroupMembers($GroupName)
		}else{
			if ($ADSPath.Contains("LDAP"))
			{	
				$MemberNames = fn_GetLDAPGroupMembers($ADSPath)
			}else
			{
				if (! $GroupName.Contains($DomainName + "\")){
					$MemberNames =fn_GetLocalGroupMembers($GroupName)
				}
			}
		}
		return $MemberNames

	}

#--------------------------
#Main work
#--------------------------

function fn_Main([string]$Groupname){
		
	$sGrouplist=$GroupName + "|"
	$nCurrDepth = 1
	

	for ($nCurrDepth = 1; $nCurrDepth -le $nMaxDepth; $nCurrDepth++) {

		if ($sGroupList.Length -gt 0)
		{
			
			$arGroupList = $sGroupList.split("|")
			$Count = [int32]$arGroupList.Count
			$sGroupList = ""
			for ($a = 0; $a -lt $Count - 1; $a++)
			{

				#on the first pass only, check if the group is a local group
				if ($nCurrDepth -eq 1) 
				{	$Groupname = $arGroupList[$a]
					$blLocalGroup = fn_IsLocalGroup($Groupname)
					#if the group is not a local group, lookup the ADS path
					$ADSPath = ""
					if ($blLocalGroup -eq "false")
					{

						$ADSPath = fn_FindDomainGroup($Groupname)
					}
					$MemberNames = fn_GetGroupMembers "$Groupname" $blLocalGroup "$ADSPath"						

				}
				else
				{
					#Check if ADSPath is already resolved, if not, search for it
					$GroupString= $arGroupList[$a]
					if ($GroupString.Contains(";")){
					  $arTemp = $GroupString.Split(";")
					  $GroupName = $arTemp[0]
					  $ADSPath = $arTemp[0]
					}else{
					   $Groupname = $GroupString
					   $ADSPath = ""
					}
					if (! $ADSPath.Contains("LDAP"))
					{
						$ADSPath = fn_FindDomainGroup($Groupname)
					}
					$MemberNames = fn_GetGroupMembers "$Groupname" "false" "$ADSPath"	
				}

				
				ForEach($member in $MemberNames)
				{
					if ($member.length -gt 0){
						$arTemp = $member.split(";")
						$DisplayString = $arTemp[0]
						write-host $DisplayString
						$DisplayString  | out-file  -append $OutFile
						if ($member.ToLower().contains("group"))
						{
							$sGroup = $member.substring(8,$member.length - 8)
							$sGrouplist = $sGrouplist + $sGroup + "|"	
						}
					}
				}
		
				$MemberNames = ""
			}	
			
		}

	}




}

#Start Processing
if (!$args.length -ge 2){
	$Message = "Usage: listusers.ps1 <domain or server name>\<group name> <#maxdepth>" + [Environment]::newline
	$Message = $Message + "Example: powershell.exe listusers.ps1 `"mydomain\domain admins`" 3 " + [Environment]::newline
	$Message = $Message + "Example: powershell.exe listusers.ps1 myserver\administrators 1 " + [Environment]::newline
	$Message = $Message + "Encapsulate group names that contain spaces with quotation marks " + [Environment]::newline
	write-host $Message
}
elseif (!$args[1] -gt 0){
	$Message = "Usage: listusers.ps1 <domain or server name>\<group name> <#maxdepth>" + [Environment]::newline
	$Message = $Message + "Example: powershell.exe listusers.ps1 `"mydomain\domain admins`" 3 " + [Environment]::newline
	$Message = $Message + "Example: powershell.exe listusers.ps1 myserver\administrators 1 " + [Environment]::newline
	$Message = $Message + "Encapsulate group names that contain spaces with quotation marks " + [Environment]::newline
	write-host $Message

}else{
	$OutFile = $args[0].replace("\","_")
	$OutFile = $OutFile.replace("/","_")
	$OutPath = Get-ScriptDirectory 
	$OutFile = $OutPath + "\" + $OutFile + ".txt"
	get-date | out-file $OutFile

	$nCurrDepth=0
	$nMaxDepth=5
	$nMaxDepth=$args[1]
	"Recursion Depth: " + $nMaxDepth | out-file $OutFile -append
	fn_Main($args[0])
}		

