$RegistryKey = "SYSTEM\CurrentControlSet\Services\W32Time\parameters"
$RegistryHive = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey([Microsoft.Win32.RegistryHive]::LocalMachine, $ComputerName)
$regKey = $RegistryHive.OpenSubKey($RegistryKey)
Write-Host "W32Time Parameter Values on $ComputerName"
Write-Host
Foreach($val in $regKey.GetValueNames()){
	Write-Host $val.PadRight(3) -nonewline
	Write-Host "=" -nonewline
	Write-Host $regKey.GetValue("$val")
}