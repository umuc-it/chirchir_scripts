If ((get-pssnapin -Name Quest.ActiveRoles.ADMAnagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

$OutputFile = "C:\Scratch\ADSearch-AllUsers-Output.csv"

$UserFile = Get-QADObject -SizeLimit 0 | where {$_.Email -ne $null} | select name

Set-Content $OutputFile "SurName,GivenName,DisplayName,SamAccountName,Email,AccountStatus,Location"

ForEach ($UserID in $UserFile) {
	$ADUser = Get-QADUser -ObjectAttributes @{samAccountName="$UserID"}
	If ($ADUser -ne $null) {
		#Write-Host $ADUser.sn
		#Write-Host $ADUSer.givenname
		#Write-Host $ADUser.displayname
		#Write-Host $ADUser.samaccountname
		#Write-Host $ADUSer.mail
		$ResultSN = $ADUser.sn
		$ResultGivenName = $ADUSer.givenname
		$ResultDisplayName = $ADUser.displayname
		$ResultSamAccountName = $ADUser.samaccountname
		$ResultMail = $ADUSer.mail
		$ResultPC = $ADUser.ParentContainer
		$TempStatus = $ADUser.AccountIsDisabled

		Add-Content $OutputFile "$ResultSN,$ResultGivenName,$ResultDisplayName,$ResultSamAccountName,$ResultMail,$ResultStatus"
	}
}