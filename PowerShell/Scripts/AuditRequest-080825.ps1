set-content -path c:\AuditRequest-080825.csv "userid`|username`|status`|lastactive"
$users = get-qaduser -service "us.umuc.edu" -sizelimit 10000 -includedproperties samaccountname,useraccountcontrol,dispalyname,lastlogontimestamp
foreach ($user in $users) {
$SAM = ""
$DispalyName = ""
$UAC = ""
$lld = ""
$AccountStatus = ""
$SAM = $user.samaccountname
$DisplayName = $user.displayname
$UAC = $user.useraccountcontrol
if ($user.lastlogontimestamp -ne $NULL) {$lld = [DateTime]::FromFileTime([Int64]::Parse($user.lastlogontimestamp))}
if ($UAC -band 2) {$accountstatus = "Disabled"} Else {$accountstatus = "Enabled"}
$OutputString = $SAM + "`|" + $DisplayName + "`|" + $accountstatus + "`|" + $lld
add-content -path c:\AuditRequest-080825.csv $OutputString
}