 	'This script will take input from a comma delimited file (specified in the InputFile variable) in the form:
' firstName,lastname,displayName,Email,userName
'and updates the email address, firstname, lastname and displayname

'Global variables
Const ADS_PROPERTY_UPDATE = 2

Dim inputFile
Dim fileSystem
Dim getUser
Dim userArray

'Initialize global variables
Set fileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set inputFile = FileSystem.OpenTextFile("C:\Scratch\PrimarySMTP\userList.txt",ForReading, False)

'Check and update the persistent info
checkUpdateEmail

'Clean up
inputFile.Close
Set fileSystem=Nothing

Sub checkUpdateEmail
	While Not inputFile.AtEndOfStream
		getUser = inputFile.ReadLine()
		userArray = Split(getUser,",")
		dummy = CStr(userArray(0))
		firstName = CStr(userArray(1))
		lastName = CStr(userArray(2))
		displayName = CStr(userArray(3))
		emailAddress = userArray(4)
		uName = userArray(5)
		WScript.Echo displayName
		Set objUser = GetObject ("LDAP://heidcns01.us.umuc.edu/CN="& uName &",OU=People,DC=europe,DC=umuc,DC=edu")

		objUser.Put "givenName", firstName
		objUser.Put "sn", lastName
		objUser.Put "displayName",displayName
		objUser.SetInfo

	Wend	
End Sub