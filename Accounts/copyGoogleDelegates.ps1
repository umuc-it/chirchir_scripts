cd C:\Scratch\Accounts
$templateEmail = $args[0].ToLower()
$targetEmail = $args[1].ToLower()
$file = $args[2]

#$file = "C:\Scratch\Accounts\delegateList-01-30-2018.csv"
#$templateEmail = "jane.walker@umuc.edu".ToLower()
#$targetEmail = "mark.chirchir@umuc.edu".ToLower()

Write-Host "============== Clone =============" -ForegroundColor Cyan

$delegateFile = Get-Content $file
foreach ($line in $delegateFile) {
  $User, $delegateName, $delegateAddress, $delegationStatus = $line -split ','
  if ($delegateAddress -eq $templateEmail)
		{
			.\gam.exe user  $User delegate to $targetEmail
            #write-Host $User " added " $targetEmail " as a delegate"
}
}

Write-Host "============== Done =============" -ForegroundColor Cyan