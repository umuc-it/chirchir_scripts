cd C:\Scratch\Accounts
$targetEmail = $args[0].ToLower()
$file = $args[1]

#$file = "C:\Scratch\Accounts\delegateList-01-30-2018.csv"
#$targetEmail = "mark.chirchir@umuc.edu".ToLower()

Write-Host "============== Remove =============" -ForegroundColor Cyan

$delegateFile = Get-Content $file
foreach ($line in $delegateFile) {
  $User, $delegateName, $delegateAddress, $delegationStatus = $line -split ','
  if ($delegateAddress -eq $targetEmail)
		{
			.\gam.exe user  $User delete delegate $targetEmail
            #write-Host $User " added " $targetEmail " as a delegate"
}
}

Write-Host "============== Done =============" -ForegroundColor Cyan
