open (raw_account_datafile, "AccountSourceFile.txt");
open (password_csv, ">Passwords.csv");

<raw_account_datafile>;
while (<raw_account_datafile>) {
	($lastname, $firstname, $wtid, $ssn, $psid, $date)  = split(/\|/);
	$wtid = lc($wtid);
	$wtid =~ s/\s+//g;
	
	$userpass = generate_password();
	print password_csv $wtid, ",", $userpass, "\n";
	#print $wtid, ",", $userpass, "\n";
}

close(password_csv);
close(raw_account_datafile);

sub generate_password
{
	$passwordlength = 10;
  	
	@lowercase = (a ... z);
	@uppercase = (A ... Z);
	@numbers = (0 ... 9);
	
	$lowersize = @lowercase;
	$uppersize = @uppercase;
	$numbersize = @numbers;
	
	$lowerbound=0; 
	$upperbound=2;
	
	$verified = "";
	while ($verified ne "True") {
		$password = "";
		while (length($password) < $passwordlength) {
			$characterset = int(rand( $upperbound-$lowerbound+1 ) ) + $lowerbound; 
			
			if ($characterset == 0) {
				$characterindex = int(rand( $lowersize-$lowerbound+1 ) ) + $lowerbound; 
				$character = @lowercase[$characterindex];
			}
			elsif ($characterset == 1) {
				$characterindex = int(rand( $uppersize-$lowerbound+1 ) ) + $lowerbound; 
				$character = @uppercase[$characterindex];
			}
			else {
				$characterindex = int(rand( $numbersize-$lowerbound+1 ) ) + $lowerbound; 
				$character = @numbers[$characterindex];
			}
		
			$password .= $character;	
		#	print $character, "\n";
		#	print $password, "\n";
		}
		
		$lowercount = 0;
		$uppercount = 0;
		$numbercount = 0;
		
		$passwordindex = 0;
		
		for ($passwordindex; $passwordindex < $passwordlength; $passwordindex++) {
			$passwordcharacter = substr($password, $passwordindex, 1);
		
			$matched = 0;
			foreach $arrayvalue (@lowercase) {
		  	if  ($arrayvalue eq $passwordcharacter) {
		  		$lowercount++;
		  	}
		  }
			foreach $arrayvalue (@uppercase) {
		  	if  ($arrayvalue eq $passwordcharacter) {
		  		$uppercount++;
		  	}
		  }
			foreach $arrayvalue (@numbers) {
		  	if  ($arrayvalue eq $passwordcharacter) {
		  		$numbercount++;
		  	}
		  }
		}
		
		if ($numbercount == 0) {
			$verified = "False";
		} elsif ($uppercount == 0) {
			$verified = "False";
		} elsif ($lowercount == 0) {
			$verified = "False";
		}	else {
			$verified = "True";
		}
		
		#print "Numbers: ", $numbercount, "\n";
		#print "Upper: ", $uppercount, "\n";
		#print "Lower: ", $lowercount, "\n";
		
		#print $verified, "\n";
	}
	
	#print $password, "\n";

	return ($password);
}

