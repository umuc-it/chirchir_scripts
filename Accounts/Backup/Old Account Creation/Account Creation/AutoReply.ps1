cd C:\Scratch\Accounts
Write-Host "==============Setup AutoReply=============" -ForegroundColor Green
$email = $args[0]
$passwd = $args[1]
$managerEmail = $args[2]
$startDate = $args[3]
$endDate = $args[4]
write-host "$email $passwd $managerEmail $sartDate $endDate"
.\gam.exe update user $email suspended off 
.\gam.exe update user $email org  "Inactive Accounts" 
.\gam.exe update user $email password $passwd
.\gam.exe user $email vacation on subject "Out of Office" message "Thank you for contacting $email at University of Maryland University College. Please redirect all business-related correspondence to $managerEmail. Thank you."
.\gam.exe user $email profile unshared
Write-Host "==============Done=============" -ForegroundColor Cyan
