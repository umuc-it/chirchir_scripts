cd C:\Scratch\Accounts
$templateEmail = $args[0]
$targetEmail = $args[1]

Write-Host "============== Clone =============" -ForegroundColor Cyan
$template = .\gam.exe info user $templateEmail
$tem_chunk = $template | Select-String -Pattern "<"
if ($tem_chunk.length -gt 0)
{
	foreach ($item in $tem_chunk)
	{	
		$grpaddress=$item.tostring().split("<")[1]
		$grpaddress= $grpaddress.Trim(">")
		if ($grpaddress -eq "executivecommittee@umuc.edu")
		{
			# Do not add
		}
		elseif ($grpaddress -eq "presidentscabinet@umuc.edu")
		{
			# Do not add
		}
		elseif ($grpaddress -eq "aptdirectreports@umuc.edu")
		{
			# Do not add
		}
		elseif ($grpaddress -eq "cio@umuc.edu")
		{
			# Do not add
		}
		else
		{
			Write-Host "$grpaddress"
			.\gam.exe update group $grpaddress add member $targetEmail 
		}
	}
}

Write-Host "============== Done =============" -ForegroundColor Cyan
