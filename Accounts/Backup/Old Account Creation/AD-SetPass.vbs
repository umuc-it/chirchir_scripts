Const ADS_UF_ACCOUNT_DISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32

usernameFile = wscript.arguments(0)

Set ofs = createobject("scripting.filesystemobject")

Set ousernameFile = ofs.opentextfile(usernameFile , 1, false)

While not ousernameFile.atEndOfStream
   InputLine = split(ousernameFile.ReadLine(),",")
   username = InputLine(0)
   password = InputLine(1)
   wscript.echo "Username: " & username
   wscript.echo "Password: " & password
   wscript.echo
   Set objUser = GetObject _
      ("LDAP://adeprdcns02.us.umuc.edu/cn=" & username & ",ou=people,dc=us,dc=umuc,dc=edu")
	
	intUAc = 512

	objUser.SetPassword password
	objUser.AccountDisabled = False
	objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
	objUser.Put "pwdLastSet", 0
	objUser.SetInfo
Wend

ousernameFile.close()

