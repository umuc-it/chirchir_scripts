'On Error Resume Next
Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=mtest,ou=people,dc=us,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,"us\mchirchir_eu","1@#kiptoo",ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=mtest,ou=people,dc=us,dc=umuc,dc=edu")
    objGroup.SetInfo
Next
