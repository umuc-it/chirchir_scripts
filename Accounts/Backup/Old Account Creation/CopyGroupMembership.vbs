On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3

'usernameFile = wscript.arguments(0)
Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile("C:\Scratch\Accounts\Backup\Account Creation\MembershipTemplates.txt" , 1, false)
'Set outputFile = FileSystem.CreateTextFile("C:\Scratch\Account Creation\homedrive.txt",True)

While not ousernameFile.atEndOfStream
		InputLine = split(ousernameFile.ReadLine(),",")
		sourceusername = InputLine(0)
		destinationusername = InputLine(1)
		wscript.echo "Copying Group Memberships"
		wscript.echo
		wscript.echo "Source User: " & sourceusername
		wscript.echo "Destination User: " & destinationusername
		wscript.echo
		'outputFile.writeLine "MakeHomeDir-Generic.cmd adefp01 " & destinationusername
		
		Set objUser = GetObject _
		    ("LDAP://adeprdcns02.us.umuc.edu/cn=" & sourceusername & ",ou=Administrators,dc=us,dc=umuc,dc=edu")
		 
		arrMemberOf = objUser.GetEx("memberOf")
		 
		If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
		    WScript.Echo "Source User does not have memberOf attribute set."
		Else
		    WScript.Echo "Copying Membership in: "
		    For Each Group in arrMemberOf
		        WScript.Echo Group
						Set objGroup = GetObject _
		    				("LDAP://adeprdcns02.us.umuc.edu/" & Group)
						objGroup.PutEx ADS_PROPERTY_APPEND, _
		    				"member", Array("cn=" & destinationusername & ",ou=Administrators,dc=us,dc=umuc,dc=edu")
						objGroup.SetInfo
		    Next
		End If
Wend

ousernameFile.close()