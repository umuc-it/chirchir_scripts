'******************************************************************************
'Account Creation
'Created by Mark Chirchir
'Updated email creation logic
'WScript.Echo "Version 8.5"
'Resolved file access issues
'Update VPN logic
'Update check for spaces
'Update faculty info
'Enhancement ENHC0010197
'Enhancement ENHC0010482
'Completed Google group addition and cloning
'Include a pause after processing 5 separation accounts
'Add H drive auto creation
'Updated Europe and Asia separations to bind correctly with the resp DC
'Removed the share drive path after the share drive is created (issue with MAC
'users, Mac attempt to mount the share but occurs in an error)
'Updated ldap search with EOF and BOF
'Update Password input
'Updated AD global user search

'******************************************************************************
'******************************************************************************
'Class Declarations
'******************************************************************************
Class usrClass
    Private givenName
    Private sn
    Private uid
    Private umucEmplId
    Private umucGUID
    Private umucPrefFirstName
    Private umucCampusEmail
    Private umucEmployeeEmail
    Private umucFacultyEmail
    Private eduPersonPrimaryAffiliation
    Private umucVPNGroup
    Private umucWebexGroup
    Private title
    Private description
    Private department
	Private eduPersonPrimaryOrgUnitDN
	Private umucManagerEmail
	Private umucEmployeeAsia
	Private umucEmployeeEuro
	Private umucEmployeeUSA
	Private umucNonEmployeeUSA
	Private umucNonEmployeeAsia
	Private umucNonEmployeeEuro
    
    Property Get firstName()
        firstName = givenName
    End Property

    Property Get lastName()
        lastName = sn
    End Property

    Property Get userid()
        userid = uid
    End Property

    Property Get emplID()
        emplID = umucEmplId
    End Property

    Property Get ldapGUID()
        ldapGUID = umucGUID
    End Property

    Property Get prefFirstName()
        prefFirstName = umucPrefFirstName
    End Property

    Property Get campusEmail()
        campusEmail = umucCampusEmail
    End Property

    Property Get employeeEmail()
        employeeEmail = umucEmployeeEmail
    End Property
    
    Property Get facultyEmail()
        facultyEmail = umucFacultyEmail
    End Property

    Property Get personPrimaryAffiliation()
        personPrimaryAffiliation = eduPersonPrimaryAffiliation
    End Property

    Property Get vpnGroup()
        vpnGroup = umucVPNGroup
    End Property

    Property Get webexGroup()
        webexGroup = umucWebexGroup
    End Property
    
    Property Get userTitle()
        userTitle = title
    End Property

    Property Get userDescription()
        userdescription = description
    End Property

    Property Get userDepartment()
        userdepartment = department
    End Property
    
	Property Get personPrimaryOrgUnitDN()
        personPrimaryOrgUnitDN = eduPersonPrimaryOrgUnitDN
    End Property
    
    Property Get managerEmail()
        managerEmail = umucManagerEmail
    End Property
    
    Property Get employeeAsia()
        employeeAsia = umucEmployeeAsia
    End Property
    
    Property Get employeeEuro()
        employeeEuro = umucEmployeeEuro
    End Property
    
    Property Get employeeUSA()
        employeeUSA = umucEmployeeUSA
    End Property
    
    Property Get nonEmployeeAsia()
        nonEmployeeAsia = umucNonEmployeeAsia
    End Property
    
    Property Get nonEmployeeEuro()
        nonEmployeeEuro = umucNonEmployeeEuro
    End Property
    
    Property Get nonEmployeeUSA()
        nonEmployeeUSA = umucNonEmployeeUSA
    End Property
            
    Property Let firstName(NewVal)
        givenName = NewVal
    End Property

    Property Let lastName(NewVal)
        sn = NewVal
    End Property

    Property Let userid(NewVal)
        uid = NewVal
    End Property

    Property Let emplID(NewVal)
        umucEmplId = NewVal
    End Property

    Property Let ldapGUID(NewVal)
        umucGUID = NewVal
    End Property

    Property Let prefFirstName(NewVal)
        umucPrefFirstName = NewVal
    End Property

    Property Let campusEmail(NewVal)
        umucCampusEmail = NewVal
    End Property

    Property Let employeeEmail(NewVal)
        umucEmployeeEmail = NewVal
    End Property
    
    Property Let facultyEmail(NewVal)
        umucFacultyEmail = NewVal
    End Property

    Property Let personPrimaryAffiliation(NewVal)
        eduPersonPrimaryAffiliation = NewVal
    End Property

    Property Let vpnGroup(NewVal)
        umucVPNGroup = NewVal
    End Property
    
    Property Let webexGroup(NewVal)
        umucWebexGroup = NewVal
    End Property
    
    Property Let userTitle(NewVal)
        title = NewVal	
    End Property
    
    Property Let userdescription(NewVal)
        description = NewVal
    End Property

    Property Let userdepartment(NewVal)
        department = NewVal
    End Property
    	
	Property Let personPrimaryOrgUnitDN(NewVal)
        eduPersonPrimaryOrgUnitDN = NewVal
    End Property
    
    Property Let managerEmail(NewVal)
        umucManagerEmail = NewVal
    End Property
    
    Property Let employeeAsia(NewVal)
        umucEmployeeAsia = NewVal
    End Property
    
    Property Let employeeEuro(NewVal)
        umucEmployeeEuro = NewVal
    End Property
    
    Property Let employeeUSA(NewVal)
        umucEmployeeUSA = NewVal
    End Property
    
    Property Let nonEmployeeAsia(NewVal)
        umucNonEmployeeAsia = NewVal
    End Property
    
    Property Let nonEmployeeEuro(NewVal)
        umucNonEmployeeEuro = NewVal
    End Property
    
    Property Let nonEmployeeUSA(NewVal)
        umucNonEmployeeUSA = NewVal
    End Property
    
End Class
	
'******************************************************************************
'Constant variables
'******************************************************************************
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNTDISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32
Const ADS_PROPERTY_CLEAR = 1
Const ADS_SECURE_AUTHENTICATION = 1
Const ADS_USE_ENCRYPTION = 2
Const ForAppending = 8
Const ForReading = 1
Const ForWriting = 2

'******************************************************************************
'Global variables
'******************************************************************************
Dim strPassword
Dim duplicateEmail
Dim sDN
Dim sRoot
Dim sUser
Dim dso
Dim oAuth
Dim oConn
Dim usrObj
Dim usrObjClone
Dim acctIsThere
Dim asiaOutput
Dim europeOutput
Dim europeAdjunctOutput
Dim appOpsOutput
Dim usOutput
Dim count
Dim userName
Dim userName2
Dim bPasswordBoxWait
'Initialize variables

Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")

Set outputFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\output.txt",2,true)
outputFile.WriteLine("Username, Password, Email Address")

Set delegateFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\updates.csv",2,true)
delegateFile.WriteLine("Template,Target")

scriptVersion

'******************************************************************************
'User credentials
'******************************************************************************
sUser = "us\wcurry_eu"
'ldapmasterQAT sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
sDN = "umucGUID=umuc-2945987674662519616,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://usv-ldpapp-04.us.umuc.edu/dc=umuc,dc=edu"
'WScript.Echo "Please enter your password"
'strPassword = WScript.StdIn.ReadLine
strPassword = PasswordBox("Password:")

'******************************************************************************
'Get User Input
'******************************************************************************
Input = 0
count = 0
While Input < 13
		
	WScript.Echo ""
	WScript.Echo "Please choose from the following list"
	WScript.Echo "1. Create User Email in LDAP"
	WScript.Echo "2. Create Faculty Email Account"
	WScript.Echo "3. Delete User Mailbox and Print Separation Template"
	WScript.Echo "4. Print Faculty email"
	WScript.Echo "5. Create User AD and Email Account"
	WScript.Echo "6. Copy Group memberships"
	WScript.Echo "7. Print User ID from Email"
	WScript.Echo "8. Print User ID from Faculty Email"
	WScript.Echo "9. Print UMUC Guid"
	WScript.Echo "10. Remove AD Groups"
	WScript.Echo "11. Send Emails"
	WScript.Echo "12. Test"
	WScript.Echo "13. End Program"
	WScript.Echo ""
	
	WScript.StdIn.Read(0)
	Input = WScript.StdIn.ReadLine
	
	If Input = 2 Then
		Set appOps = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\appOps.txt",2,true)
		Set facultyAsiaOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Asia_AD_Faculty.txt",2,true)
		Set facultyUSOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\USA_AD_Faculty.txt",2,true)
		Set facultyEuropeOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty.txt",2,true)
		Set facultyEuropeAdjunctOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty-Adjunct.txt",2,true)
		acctIsThere = 0
		duplicateEmail = 0
		asiaOutput = 0
		europeOutput = 0
		europeAdjunctOutput = 0
		usOutput = 0
		appOpsOutput = 0
	End If
	
	If Input = 4 Then
		Set appOps = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\appOps.txt",2,true)
		Set facultyAsiaOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Asia_AD_Faculty.txt",2,true)
		Set facultyUSOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\USA_AD_Faculty.txt",2,true)
		Set facultyEuropeOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty.txt",2,true)
		Set facultyEuropeAdjunctOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty-Adjunct.txt",2,true)
		acctIsThere = 0
		duplicateEmail = 0
		asiaOutput = 0
		europeOutput = 0
		europeAdjunctOutput = 0
		usOutput = 0
		appOpsOutput = 0
	End If
	
'Call the main logic	
	If Input < 13 then
		
		main(Input)
		
	End If
	
	If Input = 13 Then
		delegateFile.Close
		Set delegateFile = nothing
		outputFile.Close
		Set outputFile = Nothing
		Set oConn = Nothing
	End If
	
	
	If Input = 2 Then
		appOps.Close
		facultyAsiaOutput.Close
		facultyEuropeOutput.Close
		facultyUSOutput.Close
		facultyEuropeAdjunctOutput.Close
	End If
	
	If Input = 4 Then
		appOps.Close
		facultyAsiaOutput.Close
		facultyEuropeOutput.Close
		facultyUSOutput.Close
		facultyEuropeAdjunctOutput.Close
	End If
Wend

'******************************************************************************
'Main Function
'******************************************************************************
Function main(userInput)

Set dso = GetObject("LDAP:")
Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword


Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile("c:\Scratch\Accounts\emplid.txt" , 1, false)

'Run the main logic
While not ousernameFile.AtEndOfStream
	useriden = ousernameFile.ReadLine()
	
	'Create new User	
	Set usrObj = New usrClass
	Set usrObjClone = New usrClass

	If IsNumeric(useriden) Then
		vsearch = "(umucemplid=" & useriden & ")"
	Else
		vsearch = "(uid=" & useriden & ")"
	End If

	Select Case userInput
  		Case 1
  			'Update Staff
	    	ldapExtract(vsearch)
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
				WScript.Echo usrObj.lastName & "|" & usrObj.firstName & "|" & usrObj.userid & "|SSN|" & usrObj.emplID & "|"
			End If
			duplicateEmail = 0
						
  		Case 2
  			'Update Faculty
	    	facultyLdapExtract(vsearch)
			checkFacultyEmail
			If duplicateEmail = 0 Then
				updateFacultyEmail
				WScript.Echo usrObj.firstName & "," & usrObj.prefFirstName & "," & usrObj.lastName & "," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.facultyEmail
				If usrObj.personPrimaryOrgUnitDN = "USA" Then
					facultyUSOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					usOutput = 1
					appOpsOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
					facultyAsiaOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					asiaOutput = 1
					appOpsOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
					facultyEuropeOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					europeOutput = 1
					appOpsOutput = 1
					adjunctTitle = LCase(Mid(usrObj.userTitle,1,3))
					If adjunctTitle = "adj" Then
						facultyEuropeAdjunctOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
						europeAdjunctOutput = 1
						appOpsOutput = 1
					End If
				Else
					WScript.Echo "Missing the eduPersonPrimaryOrgUnitDN"
				End If

				appOps.WriteLine("CREATE," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.firstName & "," & usrObj.lastName & ",,Learner,1," & usrObj.facultyemail)	
			End If
			duplicateEmail = 0
  		Case 3
  			'Separate Account 
  			count = count + 1
  			ldapExtract(vsearch) 			
	   	  	If usrObj.employeeUSA = "Y" Or usrObj.nonEmployeeUSA = "Y" Then
				removeAllADGroupsUS
				disableAccountUS
				removeVPNGroup
				removeWebexGroup
				autoReply
				printSeparation
				
			ElseIf usrObj.employeeAsia = "Y" Or usrObj.nonEmployeeAsia = "Y" Then
				removeAllADGroupsAsia
				disableAccountAsia
				removeVPNGroup
				removeWebexGroup
				autoReply
				printSeparation
				
			ElseIf usrObj.employeeEuro = "Y" Or usrObj.nonEmployeeEuro = "Y" Then
				removeAllADGroupsEurope
				disableAccountEurope
				removeVPNGroup
				removeWebexGroup
				autoReply
				printSeparation
				
			Else
				findInAD
				If usrObj.employeeUSA = "Y" Or usrObj.nonEmployeeUSA = "Y" Then
					removeAllADGroupsUS
					disableAccountUS
					removeVPNGroup
					removeWebexGroup
					autoReply
					printSeparation
				
				ElseIf usrObj.employeeAsia = "Y" Or usrObj.nonEmployeeAsia = "Y" Then
					removeAllADGroupsAsia
					disableAccountAsia
					removeVPNGroup
					removeWebexGroup
					autoReply
					printSeparation
				
				ElseIf usrObj.employeeEuro = "Y" Or usrObj.nonEmployeeEuro = "Y" Then
					removeAllADGroupsEurope
					disableAccountEurope
					removeVPNGroup
					removeWebexGroup
					autoReply
					printSeparation
				Else
					WScript.Echo "There was an error with establishing the users division, use the manual process to disable this account"
				End If
				
			End If
			
			If count = 5 Then
				'pause execution to allow copying of the group memberships
				WScript.Echo "Press Enter to continue"
     			dummy = WScript.StdIn.ReadLine(1)
     			count = 0
			End If
								
  		Case 4
  			'Print Faculty email
			facultyLdapExtract(vsearch)
			WScript.Echo usrObj.firstName & "," & usrObj.prefFirstName & "," & usrObj.lastName & "," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.facultyEmail & "," & usrObj.personPrimaryOrgUnitDN
				If usrObj.personPrimaryOrgUnitDN = "USA" Then
					facultyUSOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					usOutput = 1
					appOpsOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
					facultyAsiaOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					asiaOutput = 1
					appOpsOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
					facultyEuropeOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					europeOutput = 1
					appOpsOutput = 1
					adjunctTitle = LCase(Mid(usrObj.userTitle,1,3))
					If adjunctTitle = "adj" Then
						facultyEuropeAdjunctOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
						europeAdjunctOutput = 1
						appOpsOutput = 1
					End If
				Else
					WScript.Echo "Missing the eduPersonPrimaryOrgUnitDN"
				End If

			appOps.WriteLine("CREATE," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.firstName & "," & usrObj.lastName & ",,Learner,1," & usrObj.facultyemail)

		Case 5
  			'Update Staff
  			
  			InputLine = split(useriden,",")
			newEmployee = InputLine(0)
			oldEmployee = InputLine(1)
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
			If IsNumeric(oldEmployee) Then
				oldemp = "(umucemplid=" & oldEmployee & ")"
			Else
				oldemp = "(uid=" & oldEmployee & ")"
			End If
			
	    	ldapExtract(newemp)  	
	    	ldapExtractClone(oldemp)
	    	
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
			End If

			findAccount
			If accountExists = 0 Then
				If usrObj.personPrimaryOrgUnitDN = "USA" Then
					CreateADAccountUS
					WScript.Sleep 5000
					CopyADGroupsUS
					copyGoogleGroups
					updateVPN
					updateWebex
					WScript.Sleep 5000
					removeHomeDrive
					
				ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
					CreateADAccountEurope
					WScript.Sleep 5000
					CopyADGroupsEurope
					copyGoogleGroups
					updateVPN
					updateWebex
				
				ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
					CreateADAccountAsia
					WScript.Sleep 5000
					CopyADGroupsAsia
					copyGoogleGroups
					updateVPN
					updateWebex
					
				Else
					WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
				End If
				
			Else
				'do nothing	
			End If
			
			accountExists =0
			duplicateEmail = 0

  		Case 6
  			'Copy Groups
			InputLine = split(useriden,",")
			newEmployee = InputLine(0)
			oldEmployee = InputLine(1)
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
			If IsNumeric(oldEmployee) Then
				oldemp = "(umucemplid=" & oldEmployee & ")"
			Else
				oldemp = "(uid=" & oldEmployee & ")"
			End If
			
	    	ldapExtract(newemp)  	
	    	ldapExtractClone(oldemp)

			If usrObj.personPrimaryOrgUnitDN = "USA" Then
				CopyADGroupsUS
				copyGoogleGroups
				updateVPN
				updateWebex
			
			ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
				CopyADGroupsEurope
				copyGoogleGroups
				updateVPN
				updateWebex
	
			ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
				CopyADGroupsAsia
				copyGoogleGroups
				updateVPN
				updateWebex
					
			Else
				WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
			End If
			
  		Case 7
  			'Print user email
			vsearch = "(umucEmployeeEmail= " & useriden & ")"
			on error resume next
			ldapExtract(vsearch)
			'outputFile.Writeline (usrObj.userid & "|" & usrObj.emplID & "|" & usrObj.employeeemail)
			WScript.Echo usrObj.firstName & "," & usrObj.prefFirstName & "," & usrObj.lastName & "," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.managerEmail
						
  		Case 8
  			'Print faculty email
			vsearch = "(uid= " & useriden & ")"
			on error resume next
			facultyLdapExtract(vsearch)
			outputFile.Writeline (usrObj.userid & "|" & usrObj.emplID & "|" & usrObj.facultyemail)
			
  		Case 9
  			'Update Staff
	    	ldapExtract(vsearch)
	    	WScript.Echo usrObj.firstName & "," & usrObj.lastName & "," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.userTitle & "," & usrObj.prefFirstName & "," & usrObj.campusEmail & "," & usrObj.employeeEmail & "," & usrObj.personPrimaryAffiliation & "," & usrObj.ldapGUID & "," & usrObj.personPrimaryOrgUnitDN & "," & usrObj.managerEmail & "," & usrObj.employeeAsia & "," & usrObj.employeeEuro & "," & usrObj.employeeUSA & "," & usrObj.nonEmployeeUSA & "," & usrObj.nonEmployeeAsia & "," & usrObj.nonEmployeeEuro
  		
  		Case 10
  			'Remove Groups
			newEmployee = useriden
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
	    	ldapExtract(newemp)	
	    	
	    	If usrObj.personPrimaryOrgUnitDN = "USA" Then
				removeAllADGroupsUS
				removeVPNGroup
				removeWebexGroup
			
			ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
				removeAllADGroupsEurope
				removeVPNGroup
				removeWebexGroup
	
			ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
				removeAllADGroupsAsia
				removeVPNGroup
				removeWebexGroup
					
			Else
				WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
			End If

  		Case 11
  		
  			If usOutput = 1 Then
				sendFacultySvcsEmailUS
				usOutput = 0
			End If
			If asiaOutput = 1 Then
				sendFacultySvcsEmailAsia
				asiaOutput = 0
			End If
			If europeOutput = 1 Then
				sendFacultySvcsEmailEurope
				europeOutput = 0
			End If
			If europeAdjunctOutput = 1 Then
				sendFacultySvcsEmailEuropeAdjunct
				europeAdjunctOutput = 0
			End If
			
			If appOpsOutput = 1 Then
				sendAppOpsEmail
				appOpsOutput = 0
			End If
  
  		Case 12
  			
  			'If IsNumeric(useriden) Then
			'	newemp = "(umucemplid=" & useriden & ")"
			'Else
			'	newemp = "(uid=" & useriden & ")"
			'End If
			
	    	'ldapExtract(newemp)
	    	'updateAccount
	    	WScript.Echo useriden
	    	facultyLdapExtract(useriden)
	    	WScript.Echo useriden & "," & usrObj.facultyEmail
			
			
	End Select
	
	Set usrobj = nothing
	Set usrObjClone = nothing
	
Wend

End Function


'******************************************************************************
'Populate user object
'******************************************************************************
Function ldapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,title,umucPrefFirstName,umucCampusEmail,umucEmployeeEmail,eduPersonPrimaryAffiliation,umucGUID, eduPersonPrimaryOrgUnitDN, umucManagerEmail, umucEmployeeAsia, umucEmployeeEuro, umucEmployeeUSA, umucNonEmployeeUSA, umucNonEmployeeAsia, umucNonEmployeeEuro, umucNonEmployeeUSA, umucNonEmployeeAsia, umucNonEmployeeEuro;subtree")

If not oRS.BOF and not oRS.EOF then 
	For Each Element In oRS.Fields(0).Value
		var1 = Element
		var1 = Trim(var1)
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.firstName = var2
	Next
	For Each Element In oRS.Fields(1).Value
		var1 = Element
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.lastName = var2
	Next
	For Each Element In oRS.Fields(2).Value
		usrObj.userid = Element
	Next

	usrObj.emplID = oRS.Fields(3).Value

	If IsNull(oRS.Fields(4).Value) Then
		usrObj.userTitle = "TBD"
	Else
		For Each Element In oRS.Fields(4).Value
			usrObj.userTitle = Element
		Next
	End If

	If IsNull(oRS.Fields(5).Value) Then
		usrObj.prefFirstName = usrObj.firstName
	Else
		var1 = oRS.Fields(5).Value
		var1 = Trim(var1)
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.prefFirstName = var2
	End If

	If IsNull(oRS.Fields(6).Value) Then
		usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
	Else
		For Each Element In oRS.Fields(6).Value
			usrObj.campusEmail = Element
		Next
	End If

	If IsNull(oRS.Fields(7).Value) Then
		usrObj.employeeEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
	Else
		usrObj.employeeEmail = oRS.Fields(7).Value
	End If

	usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

	usrObj.ldapGUID = oRS.Fields(9).Value

	If IsNull(oRS.Fields(10).Value) Then
		usrObj.personPrimaryOrgUnitDN = "nothing"
	Else
		For Each Element In oRS.Fields(10).Value
			var1 = Element
			var1 = Trim(var1)
			var2 = split(var1,"=")
			var3 = var2(0)
			var4 = var2(1)
			usrObj.personPrimaryOrgUnitDN = var4
		Next
	End If

	If IsNull(oRS.Fields(11).Value) Then
		usrObj.managerEmail = "nothing"
	Else
		usrObj.managerEmail = oRS.Fields(11).Value
	End If

	If IsNull(oRS.Fields(12).Value) Then
		usrObj.employeeAsia = "N"
	Else
		usrObj.employeeAsia = oRS.Fields(12).Value
	End If

	If IsNull(oRS.Fields(13).Value) Then
		usrObj.employeeEuro = "N"
	Else
		usrObj.employeeEuro = oRS.Fields(13).Value
	End If

	If IsNull(oRS.Fields(14).Value) Then
		usrObj.employeeUSA = "N"
	Else
		usrObj.employeeUSA = oRS.Fields(14).Value
	End If

	If IsNull(oRS.Fields(15).Value) Then
		usrObj.nonEmployeeUSA = "N"
	Else
		usrObj.nonEmployeeUSA = oRS.Fields(15).Value
	End If

	If IsNull(oRS.Fields(16).Value) Then
		usrObj.nonEmployeeAsia = "N"
	Else
		usrObj.nonEmployeeAsia = oRS.Fields(16).Value
	End If

	If IsNull(oRS.Fields(17).Value) Then
		usrObj.nonEmployeeEuro = "N"
	Else
		usrObj.nonEmployeeEuro = oRS.Fields(17).Value
	End If

Else
	WScript.Echo vSearch & " is not provisioned on LDAP, check with HR"
End If

End Function

'******************************************************************************
'Populate faculty user object
'******************************************************************************
Function facultyLdapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucGUID,umucPrefFirstName,umucCampusEmail,umucFacultyEmail,eduPersonPrimaryAffiliation,umucVPNGroup,eduPersonPrimaryOrgUnitDN,umucManagerEmail,title;subtree")

If not oRS.BOF and not oRS.EOF Then
	If IsNull(oRS.Fields(0).Value) Then
		usrObj.firstName = "nothing"
	Else	
		For Each Element In oRS.Fields(0).Value
			var1 = Element
			var1 = Trim(var1)
			var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
			var2 = Replace(var2," ","",1,-1)
			usrObj.firstName = var2
		Next
	End If

	If IsNull(oRS.Fields(1).Value) Then
		usrObj.lastName = "nothing"
	Else
		For Each Element In oRS.Fields(1).Value
			var1 = Element
			var1 = Trim(var1)
			var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
			var2 = Replace(var2," ","",1,-1)
			usrObj.lastName = var2
		Next
	End If
	
	For Each Element In oRS.Fields(2).Value
		usrObj.userid = Element
	Next

	usrObj.emplID = oRS.Fields(3).Value

	usrObj.ldapGUID = oRS.Fields(4).Value

	If IsNull(oRS.Fields(5).Value) Then
		usrObj.prefFirstName = usrObj.firstName
	Else
		var1 = oRS.Fields(5).Value
		var1 = Trim(var1)
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.prefFirstName = var2
	End If

	If IsNull(oRS.Fields(6).Value) Then
		usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
	Else
		For Each Element In oRS.Fields(6).Value
			usrObj.campusEmail = Element
		Next
	End If

	If IsNull(oRS.Fields(7).Value) Then
		usrObj.facultyEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
	Else
		usrObj.facultyEmail = oRS.Fields(7).Value
	End If

	usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

	If IsNull(oRS.Fields(9).Value) Then
		usrObj.vpnGroup = oRS.Fields(9).Value
	Else
		usrObj.vpnGroup = Element
	End If

	If IsNull(oRS.Fields(10).Value) Then
		usrObj.personPrimaryOrgUnitDN = "nothing"
	Else
		For Each Element In oRS.Fields(10).Value
			var1 = Element
			var1 = Trim(var1)
			var2 = split(var1,"=")
			var3 = var2(0)
			var4 = var2(1)
			usrObj.personPrimaryOrgUnitDN = var4
		Next
	End If 

	If IsNull(oRS.Fields(11).Value) Then
		usrObj.managerEmail = "nothing"
	Else
		usrObj.managerEmail = oRS.Fields(11).Value
	End If

	If IsNull(oRS.Fields(12).Value) Then
		usrObj.userTitle = "TBD"
	Else
	For Each Element In oRS.Fields(12).Value
		usrObj.userTitle = Element
	Next
	End If
Else
	WScript.Echo vSearch & " is not provisioned on LDAP, check with faculty appointments"
End If

End Function

'******************************************************************************
'Populate clone object
'******************************************************************************
Function ldapExtractClone(vsearch)

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucEmployeeEmail,umucVPNGroup, eduPersonPrimaryOrgUnitDN, umucWebexGroup;subtree")

If not oRS.BOF and not oRS.EOF Then
	For Each Element In oRS.Fields(0).Value
		var1 = Element
		var2 = ucase(mid(var1,1,1)) & mid(var1,2)
		usrObjClone.firstName = var2
	Next
	For Each Element In oRS.Fields(1).Value
		var1 = Element
		var2 = ucase(mid(var1,1,1)) & mid(var1,2)
		usrObjClone.lastName = var2
	Next

	For Each Element In oRS.Fields(2).Value
		usrObjClone.userid = Element
	Next

	usrObjClone.emplID = oRS.Fields(3).Value

	usrObjClone.employeeEmail = oRS.Fields(4).Value

	If IsNull(oRS.Fields(5).Value) Then
		usrObjClone.vpnGroup = "nothing"
	Else
		For Each Element In oRS.Fields(5).Value
			usrObjClone.vpnGroup = Element
		Next
	End If

	For Each Element In oRS.Fields(6).Value
		var1 = Element
		var1 = Trim(var1)
		var2 = split(var1,"=")
		var3 = var2(0)
		var4 = var2(1)
		usrObj.personPrimaryOrgUnitDN = var4
	Next

	If IsNull(oRS.Fields(7).Value) Then
		usrObjClone.webexGroup = "nothing"
	else
		For Each Element In oRS.Fields(7).Value
			usrObjClone.webexGroup = Element
		Next
	End If

Else
	WScript.Echo vSearch & " is not provisioned on LDAP, check with the IT Liaison"
End If

End Function

'******************************************************************************
'Generate random password
'******************************************************************************
Function RndPassword()

Randomize
dim arrPassA(9)
strResult = ""
arrSymbols = Array("!","#","$","%","&","(",")","*","+",",","-",".",":",";","<","=",">","?","[","]","^","_","{","}","~")
 
do while strResult = ""
    strPass = ""
    do while len(strPass) < 6
        intAsc = int(rnd * 57) + 65
        if intAsc < 91 or intAsc > 96 then
            strPass = strPass & chr(intAsc)
        end if
    loop
 
    strPass = strPass & cstr(int(rnd*10)) & arrSymbols(int(rnd*ubound(arrSymbols)))
 
    for i = 0 to 8
        arrPassA(i) = mid(strPass,i+1,1)
    next
 
    strPass = ""
 
    booMore = TRUE
 
    do
        intIdx = int(rnd*9)
        if arrPassA(intIdx) <> "" then
            strPass = strPass & arrPassA(intIdx)
            arrPassA(intIdx) = ""
        else
            booMore = FALSE
            for x = 0 to 9
                if arrPassA(x) <> "" then
                    booMore = TRUE
                    exit for
                end if
            next
        end if
        if not booMore then
            exit do
        end if
    loop
 
    if lcase(strPass) <> strPass and ucase(strPass) <> strPass then
 		RndPassword = strPass
 		strResult = strPass
    end if
Loop


End Function

'******************************************************************************
' Search for a User Account in Active Directory
'******************************************************************************
Function findAccount

dtStart = TimeValue(Now())

Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = _
    "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & _
         "(samAccountName=" & usrObj.userID & "));samAccountName;subtree"
  
Set objRecordSet = objCommand.Execute
 
If objRecordset.RecordCount = 0 Then
    'do nothing
Else
    WScript.Echo usrObj.userID & " Already Exists in Active Directory."
    acctIsThere = 1
End If
 
objConnection.Close

End Function

'******************************************************************************
'Create user account in Stateside AD
'******************************************************************************
Function createADAccountUS()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@us.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.Put "homeDirectory", "\\ade-opsfps-01\Home$\%UserName%"
objUser.Put "homeDrive", "H"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Create user account in  Europe AD
'******************************************************************************
Function createADAccountEurope()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@europe.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,dc=europe,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/OU=People,dc=europe,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Europe"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College Europe"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Create user account in Asia AD
'******************************************************************************
Function CreateADAccountAsia()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@asia.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,DC=asia,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/OU=People,DC=asia,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Asia"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Remove home drive option in AD object - for Mac users
'******************************************************************************
Function removeHomeDrive()
Dim objUser

Set openDS = GetObject("LDAP:") 

'Retrieve user object
Set objUser = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/CN=" & usrObj.userID & ",OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

objUSer.PutEx ADS_PROPERTY_CLEAR,"homeDirectory",0
objUser.PutEx ADS_PROPERTY_CLEAR,"homeDrive",0
objuser.setInfo

End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function copyADGroupsUS()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=us,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://adeprdcns02.us.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function copyADGroupsEurope()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function copyADGroupsAsia()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsUS()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsEurope()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsAsia()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function


'******************************************************************************
'Check LDAP for email duplicates
'******************************************************************************
Function checkEmail()

vsearch = "(umucEmployeeEmail=" & usrObj.employeeEmail & ")"

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Check LDAP for faculty email duplicates
'******************************************************************************
Function checkFacultyEmail()

vsearch = "(umucFacultyEmail=" & usrObj.facultyEmail & ")"

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Faculty Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Populate faculty email
'******************************************************************************
Function updateFacultyEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucFacultyEmail", usrObj.facultyEmail
obj.setinfo

End Function

'******************************************************************************
'Populate staff email
'******************************************************************************
Function updateEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucEmployeeEmail", usrObj.employeeEmail
obj.setinfo

WScript.Echo "Email address created in LDAP: " & usrObj.employeeEmail

End Function

'******************************************************************************
'Populate vpn group
'******************************************************************************
Function updateVPN()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
If usrObjClone.vpngroup = "nothing" Then
	'do nothing
Else
	obj.put "umucVPNGroup", usrObjClone.vpnGroup
	obj.setinfo
End If

End Function

'******************************************************************************
'Remove vpn group
'******************************************************************************
Function removeVPNGroup()
On Error Resume Next
'LDAP delete vpnGroup
WScript.Echo "Remove VPN Group"

If usrObj.vpngroup = "nothing" Then
	'do nothing
Else
	ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
	Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucVPNGroup", 0
	obj.setinfo
	WScript.Echo usrObj.vpngroup & " VPN Group Removed"
End If

End Function

'******************************************************************************
'Populate UMUCWebExGroup group
'******************************************************************************
Function updateWebex()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
If usrObjClone.webexgroup = "nothing" Then
	'do nothing
Else
	obj.put "umucWebexGroup", usrObjClone.webexGroup
	obj.setinfo
End If

End Function

'******************************************************************************
'Remove UMUCWebExGroup group
'******************************************************************************
Function removeWebexGroup()
On Error Resume Next
'LDAP delete vpnGroup

WScript.Echo "Remove WebexGroup"

If usrObj.webexgroup = "nothing" Then
	'do nothing
Else
	ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
	Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucWebexGroup", 0
	obj.setinfo
	WScript.Echo usrObj.webexgroup &" Webex Group Removed"
End If


End Function

'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccountUS()
sDomain = "us"
Dim localUID, localEmplID, recordExists
WScript.Echo "Disable US Account"
recordExists = 1
vsearch = "(umucemplid=" & usrObj.emplID & ")"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucEmployeeEmail;subtree")

For Each Element In oRS.Fields(0).Value
	localUID = Element
Next

localEmplID = oRS.Fields(1).Value

If IsNull(oRS.Fields(2).Value) Then
	'do nothing
Else
	Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
	obj.setinfo
End If

'Ad Disable account
' Search for a User Account in Active Directory
strUserName = usrObj.userID
dtStart = TimeValue(Now())
Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & "(samAccountName=" & strUserName & "));distinguishedName;subtree"
  
Set objRecordSet = objCommand.Execute

If objRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    recordExists = objRecordset.RecordCount
Else
	userDN = objRecordSet.Fields("distinguishedName").Value
End If

objConnection.Close

If recordExists = 0 Then
	'do nothing
Else
	Set objDSO = GetObject("LDAP:")
	Set objDisable = objDSO.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& userDN,sUser,strPassword, &H0200)
		intUAC = objDisable.Get("userAccountControl") 
  		objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
		objDisable.SetInfo
		WScript.Echo usrObj.userID & " US Account disabled"
	sDestOU = "LDAP://adeprdcns02.us.umuc.edu/OU=Inactive,dc=us,dc=umuc,dc=edu"
	Set objRootDSE = GetObject("LDAP:")
	Set objDestOU = objRootDSE.OpenDSObject(sDestOU, sUser, strPassword, ADS_SECURE_AUTHENTICATION)
	objDestOU.MoveHere "LDAP://adeprdcns02.us.umuc.edu/" & userDn, vbNullString
End If

End Function

'******************************************************************************
'Disable account in Europe AD and delete email from LDAP
'******************************************************************************
Function disableAccountEurope()
sDomain = "Europe"
Dim localUID, localEmplID, recordExists
WScript.Echo "Disable Europe Account"
recordExists = 1
vsearch = "(umucemplid=" & usrObj.emplID & ")"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucEmployeeEmail;subtree")

For Each Element In oRS.Fields(0).Value
	localUID = Element
Next

localEmplID = oRS.Fields(1).Value

If IsNull(oRS.Fields(2).Value) Then
	'do nothing
Else
	Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
	obj.setinfo
End If

'Ad Disable account

' Specify a server (Domain Controller).
strServer = "eui-opsdom-02.europe.umuc.edu"
strUserName = usrObj.userID

' Determine DNS domain name. Use server binding and alternate
' credentials. The value of strDNSDomain can also be hard coded.
Set objNS = GetObject("LDAP:")
Set objRootDSE = objNS.OpenDSObject("LDAP://" & strServer & "/RootDSE", sUser, strPassword, ADS_SERVER_BIND Or ADS_SECURE_AUTHENTICATION)
strDNSDomain = "OU=People,DC=europe,DC=umuc,DC=edu"

' Use ADO to search Active Directory.
' Use alternate credentials.
Set adoCommand = CreateObject("ADODB.Command")
Set adoConnection = CreateObject("ADODB.Connection")
adoConnection.Provider = "ADsDSOObject"
adoConnection.Properties("User ID") = sUser
adoConnection.Properties("Password") = strPassword
adoConnection.Properties("Encrypt Password") = True
adoConnection.Properties("ADSI Flag") = ADS_SERVER_BIND _
     Or ADS_SECURE_AUTHENTICATION
adoConnection.Open "Active Directory Provider"
Set adoCommand.ActiveConnection = adoConnection

' Search entire domain. Use server binding.
strBase = "<LDAP://" & strServer & "/" & strDNSDomain & ">"

' Search for user.
strFilter = "(&(objectCategory=user)(samAccountName=" & strUserName & "))"

' Comma delimited list of attribute values to retrieve.
strAttributes = "distinguishedName"

' Construct the LDAP query.
strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"

' Run the query.
adoCommand.CommandText = strQuery
'adoCommand.Properties("Page Size") = 10000
'adoCommand.Properties("Timeout") = 30
'adoCommand.Properties("Cache Results") = False
Set adoRecordset = adoCommand.Execute


WScript.Echo adoRecordset.RecordCount
userDN = adoRecordset.Fields("distinguishedName").Value
WScript.Echo userDN
' Clean up.

If adoRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    recordExists = adoRecordset.RecordCount
Else
	userDN = adoRecordset.Fields("distinguishedName").Value
End If

If recordExists = 0 Then
	'do nothing
Else
	Set objDSO = GetObject("LDAP:")
	Set objDisable = objDSO.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& userDN,sUser,strPassword, &H0200)
		intUAC = objDisable.Get("userAccountControl") 
  		objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
		objDisable.SetInfo 
		WScript.Echo usrObj.userID & " europe Account disabled"
	sDestOU = "LDAP://EUI-OPSDOM-02.europe.umuc.edu/OU=Inactive,dc=us,dc=umuc,dc=edu"
	
  
	Set objDestOU = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/OU=Inactive,dc=europe,dc=umuc,dc=edu")
	objDestOU.MoveHere "LDAP://" & userDN, vbNullString
End If
WScript.Echo "europe Account disabled"
adoRecordset.Close
adoConnection.Close

End Function

'******************************************************************************
'Disable account in Asia AD and delete email from LDAP
'******************************************************************************
Function disableAccountAsia()

sDomain = "Asia"
Dim localUID, localEmplID, recordExists
WScript.Echo "Disable Asia Account"
recordExists = 1
vsearch = "(umucemplid=" & usrObj.emplID & ")"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucEmployeeEmail;subtree")

For Each Element In oRS.Fields(0).Value
	localUID = Element
Next

localEmplID = oRS.Fields(1).Value

If IsNull(oRS.Fields(2).Value) Then
	'do nothing
Else
	Set obj = dso.OpenDSObject("LDAP://usv-ldpapp-04.us.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
	obj.setinfo
End If

'Ad Disable account

' Specify a server (Domain Controller).
strServer = "eui-opsdom-05.asia.umuc.edu"
strUserName = usrObj.userID

' Determine DNS domain name. Use server binding and alternate
' credentials. The value of strDNSDomain can also be hard coded.
Set objNS = GetObject("LDAP:")
Set objRootDSE = objNS.OpenDSObject("LDAP://" & strServer & "/RootDSE", sUser, strPassword, ADS_SERVER_BIND Or ADS_SECURE_AUTHENTICATION)
strDNSDomain = "OU=People,DC=asia,DC=umuc,DC=edu"

' Use ADO to search Active Directory.
' Use alternate credentials.
Set adoCommand = CreateObject("ADODB.Command")
Set adoConnection = CreateObject("ADODB.Connection")
adoConnection.Provider = "ADsDSOObject"
adoConnection.Properties("User ID") = sUser
adoConnection.Properties("Password") = strPassword
adoConnection.Properties("Encrypt Password") = True
adoConnection.Properties("ADSI Flag") = ADS_SERVER_BIND _
     Or ADS_SECURE_AUTHENTICATION
adoConnection.Open "Active Directory Provider"
Set adoCommand.ActiveConnection = adoConnection

' Search entire domain. Use server binding.
strBase = "<LDAP://" & strServer & "/" & strDNSDomain & ">"

' Search for user.
strFilter = "(&(objectCategory=user)(samAccountName=" & strUserName & "))"

' Comma delimited list of attribute values to retrieve.
strAttributes = "distinguishedName"

' Construct the LDAP query.
strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"

' Run the query.
adoCommand.CommandText = strQuery
'adoCommand.Properties("Page Size") = 10000
'adoCommand.Properties("Timeout") = 30
'adoCommand.Properties("Cache Results") = False
Set adoRecordset = adoCommand.Execute


WScript.Echo adoRecordset.RecordCount
userDN = adoRecordset.Fields("distinguishedName").Value
WScript.Echo userDN
' Clean up.

If adoRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    recordExists = adoRecordset.RecordCount
Else
	userDN = adoRecordset.Fields("distinguishedName").Value
End If

If recordExists = 0 Then
	'do nothing
Else
	Set objDSO = GetObject("LDAP:")
	Set objDisable = objDSO.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& userDN,sUser,strPassword, &H0200)
		intUAC = objDisable.Get("userAccountControl") 
  		objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
		objDisable.SetInfo 
		WScript.Echo usrObj.userID & " Asia Account disabled"
	sDestOU = "LDAP://EUI-OPSDOM-05.asia.umuc.edu/OU=Inactive,dc=us,dc=umuc,dc=edu"
	
  
	Set objDestOU = GetObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/OU=Inactive,dc=asia,dc=umuc,dc=edu")
	objDestOU.MoveHere "LDAP://" & userDN, vbNullString
End If
WScript.Echo "Asia Account disabled"
adoRecordset.Close
adoConnection.Close

End Function

'******************************************************************************
'Print template for HR submission
'******************************************************************************
Function printSeparation()

WScript.Echo "Name: " & usrObj.firstName & " " & usrObj.lastName
WScript.Echo "Login ID: " & usrObj.userID
WScript.Echo "EmplID: " & usrObj.emplID
WScript.Echo

End Function

'******************************************************************************
'Send email to App Ops
'******************************************************************************
Function sendAppOpsEmail()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Faculty.Provisioning@umuc.edu"
objEmail.To = "servicedesk@umuc.edu"
objEmail.BCC = "Faculty.Provisioning@umuc.edu"
objEmail.Subject = "Provision Faculty - " & setDate 
objEmail.Textbody = "LEO account creations."
objEmail.AddAttachment "C:\Scratch\Accounts\appOps.txt"


objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services
'******************************************************************************
Function sendFacultySvcsEmail()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Faculty.Provisioning@umuc.edu"
objEmail.To = "Amanda.Stapleton@umuc.edu;Nakisha.Williams@umuc.edu"
objEmail.CC = "SOP@umuc.edu;Contracts@umuc.edu"
objEmail.BCC = "Faculty.Provisioning@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\USA_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services US
'******************************************************************************
Function sendFacultySvcsEmailUS()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Faculty.Provisioning@umuc.edu"
objEmail.To = "Amanda.Stapleton@umuc.edu;Nakisha.Williams@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;Contracts@umuc.edu"
objEmail.Bcc = "Faculty.Provisioning@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\USA_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services Asia
'******************************************************************************
Function sendFacultySvcsEmailAsia()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Faculty.Provisioning@umuc.edu"
objEmail.To = "HRFaculty-Asia@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;David.Schultz@umuc.edu"
objEmail.Bcc = "Faculty.Provisioning@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\Asia_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services Europe
'******************************************************************************
Function sendFacultySvcsEmailEurope()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Faculty.Provisioning@umuc.edu"
objEmail.To = "HRPS-europe@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;Director-europe@umuc.edu;Operations-europe@umuc.edu;Downrange-europe@umuc.edu;Dean-europe@umuc.edu,Timothy.Holliefield@umuc.edu"
objEmail.Bcc = "Faculty.Provisioning@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\Europe_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services Europe
'******************************************************************************
Function sendFacultySvcsEmailEuropeAdjunct()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Faculty.Provisioning@umuc.edu"
objEmail.To = "AdjunctApplicants@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;Timothy.Holliefield@umuc.edu"
objEmail.Bcc = "Faculty.Provisioning@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\Europe_AD_Faculty-Adjunct.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Setup autoreply on separated users
'******************************************************************************
Function autoReply()

Dim Exec, strDate, strDay, strMonth, strYear, strEndYear

If usrObj.managerEmail = "nothing" Then
	'do nothing
	WScript.Echo "Manager email address is empty on LDAP, auto reply was not set"
Else

	WScript.Echo "Set Autoreply and Remove Google Groups"
	randomPass = RndPassword

	strDate = CDate(Date)
	strDay = DatePart("d", strDate)
	strMonth = DatePart("m", strDate)
	strYear = DatePart("yyyy", strDate)
	If strDay < 10 Then
  		strDay = "0" & strDay
	End If
	If strMonth < 10 Then
  		strMonth = "0" & strMonth
	End If
	strEndYear = strYear + 1
	startdate = strYear & "-" & strMonth & "-" & strDay
	enddate = strEndYear & "-" & strMonth & "-" & strDay

	WScript.Echo startdate &" -> "& enddate


	Set objShell = CreateObject("Wscript.Shell")
	objShell.Run("powershell -executionpolicy bypass -noexit -file C:\Scratch\Accounts\googleSeparation.ps1 " & usrObj.employeeEmail & " " & randomPass & " " & usrObj.managerEmail & " " & startdate & " " & enddate)
End If

End Function

'******************************************************************************
'Create Google Mailbox
'******************************************************************************
Function createGoogleMailbox()

'randomPass = RndPassword
'Set objShell = CreateObject("Wscript.Shell")
'objShell.Run("powershell -executionpolicy bypass -noexit -file C:\Scratch\Accounts\copyGroups.ps1 " & usrObj.employeeEmail & " " & randomPass & " " & usrObj.managerEmail & " " & startdate & " " & enddate)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function copyGoogleGroups()

Set objShell = CreateObject("Wscript.Shell")
objShell.Run("powershell -executionpolicy bypass -noexit -file C:\Scratch\Accounts\copyGroups.ps1 " & usrObjClone.employeeEmail & " " & usrObj.employeeEmail)

End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function testPass(sUser,strPassword)

dim objDSO
dim objUser
dim strPath

On Error Resume Next
set objDSO = GetObject("LDAP:")
set objUser = objDSO.OpenDSObject ("LDAP://adeprdcns02.us.umuc.edu/OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
if Err.Number <> 0 then
    WScript.Echo "Incorrect Password Access Denied"
    fnCheckAccess = False
else
    fnCheckAccess = True
end if
Err.Clear
On Error Goto 0

set objDSO = Nothing
set objUser = Nothing

End Function

'******************************************************************************
'Check Script Version
'******************************************************************************
Function scriptVersion()


Set scrVerOFS = createobject("scripting.filesystemobject")
Set scrVer = scrVerOFS.opentextfile("C:\Scratch\Accounts\ScriptVersion.txt",1,false)

While not scrVer.AtEndOfStream
	version = scrVer.ReadLine()
	WScript.Echo version
Wend

scrVer.Close

End Function

'******************************************************************************
'Create user account in Stateside AD
'******************************************************************************
Function updateAccount()

' Search for a User Account in Active Directory
strUserName = usrObj.userID
dtStart = TimeValue(Now())
Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
WScript.Echo usrObj.userID
WScript.Echo usrObj.personPrimaryAffiliation
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection

objCommand.CommandText = "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & "(samAccountName=" & strUserName & "));distinguishedName;subtree"

Set objRecordSet = objCommand.Execute

If objRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    recordExists = objRecordset.RecordCount
Else
	userDN = objRecordSet.Fields("distinguishedName").Value
	recordExists = 1
End If

objConnection.Close

If recordExists = 0 Then
	WScript.Echo "No record"
Else
	Set objDSO = GetObject("LDAP:")
	WScript.Echo sUser
	WScript.Echo strPassword
	Set objUser = objDSO.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& userDN,sUser,strPassword, &H0200)
	objUser.Put "employeeType", usrObj.personPrimaryAffiliation
	WScript.Echo usrObj.personPrimaryAffiliation
	objUser.SetInfo
End If

End Function


'******************************************************************************
'Find object in forest
'******************************************************************************
Function findInAD()

ldapFilter = "(samAccountName=" & usrObj.userID & ")"
Dim userDN

Set aoi = CreateObject("ADSystemInfo")                     'evaluate global catalog search base
gcBase = aoi.ForestDNSName

Set ado = CreateObject("ADODB.Connection")
ado.Provider = "ADSDSOObject"
ado.Open "ADSearch" 
Set objectList = ado.Execute("<GC://" & gcBase & ">;" & ldapFilter & ";distinguishedName,samAccountName,employeeID,displayname,userPrincipalName;subtree")

userDN = objectList.Fields("distinguishedName")

logonName =  objectList.Fields("samAccountName")

WScript.Echo userDN
division = Split(userDN,",")

var1=Split(division(2),"=")
var2 = var1(1)
WScript.Echo var2

If var2 = "asia" Then
	usrObj.employeeAsia = "Y"
	usrObj.nonEmployeeAsia = "Y"
ElseIf var2 = "europe"Then
	usrObj.employeeEuro = "Y"
	usrObj.nonEmployeeEuro = "Y"
ElseIf var2 = "us"Then
	usrObj.employeeUSA = "Y" 
	usrObj.nonEmployeeUSA = "Y"
Else
	WScript.Echo "There was an error with establishing the users division, use the manual process to disable this account"
End If

End Function

'******************************************************************************
'Get Password
'******************************************************************************

Function PasswordBox(sTitle) 
  set oIE = CreateObject("InternetExplorer.Application") 
  With oIE 
    .FullScreen = False 
    .ToolBar   = False : .RegisterAsDropTarget = False 
    .StatusBar = False : .Navigate("about:blank") 
    While .Busy : WScript.Sleep 100 : Wend 
    With .document 
      With .ParentWindow 
        .resizeto 400,200
        .moveto .screen.width/2-200, .screen.height/2-50
      End With 
      .WriteLn("<html><body bgColor=White><center>") 
      .WriteLn(sTitle) 
      .WriteLn("Password <input type=password id=pass>  " & "<button id=but0>Submit</button>") 
      .WriteLn("</center></body></html>") 
      With .ParentWindow.document.body 
        .scroll="yes" 
        .style.borderStyle = "outset" 
        .style.borderWidth = "3px" 
      End With 
      .all.but0.onclick = getref("PasswordBox_Submit") 
      .all.pass.focus 
      oIE.Visible = True 
      bPasswordBoxOkay = False : bPasswordBoxWait = True 
      On Error Resume Next 
      While bPasswordBoxWait 
        WScript.Sleep 100 
        if oIE.Visible Then bPasswordBoxWait = bPasswordBoxWait 
        if Err Then bPasswordBoxWait = False 
      Wend 
      PasswordBox = .all.pass.value 
    End With ' document 
    .Visible = False 
  End With   ' IE 
End Function 


Sub PasswordBox_Submit() 
  bPasswordBoxWait = False 
End Sub

'******************************************************************************
'END
'******************************************************************************