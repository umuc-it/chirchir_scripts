On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3

usernameFile = wscript.arguments(0)
Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile(usernameFile , 1, false)
 
While not ousernameFile.atEndOfStream
		InputLine = split(ousernameFile.ReadLine(),",")
		sourceusername = InputLine(0)
		destinationusername = InputLine(1)
		wscript.echo "Copying Group Memberships"
		wscript.echo
		wscript.echo "Source User: " & sourceusername
		wscript.echo "Destination User: " & destinationusername
		wscript.echo

		Set objUser = GetObject _
		    ("LDAP://cn=" & sourceusername & ",ou=people,dc=us,dc=umuc,dc=edu")
		 
		arrMemberOf = objUser.GetEx("memberOf")
		 
		If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
		    WScript.Echo "Source User does not have memberOf attribute set."
		Else
		    WScript.Echo "Copying Membership in: "
		    For Each Group in arrMemberOf
		        WScript.Echo Group
						Set objGroup = GetObject _
		    				("LDAP://" & Group)
						objGroup.PutEx ADS_PROPERTY_APPEND, _
		    				"member", Array("cn=" & destinationusername & ",ou=people,dc=us,dc=umuc,dc=edu")
						objGroup.SetInfo
		    Next
		End If
		wscript.echo
Wend

ousernameFile.close()
