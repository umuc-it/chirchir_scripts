open (raw_account_datafile, "AccountSourceFile.txt");
open (primary_ldif, ">PrimaryADImport.ldif");
open (secondary_ldif, ">SecondaryADImport.ldif");
open (member_template, ">MembershipTemplates.txt");

<raw_account_datafile>;
while (<raw_account_datafile>) {
   ($lastname, $firstname, $wtid, $ssn, $psid, $date, $description, $template, $title, $department, $telephonenumber, $ipphone, $streetaddress)  = split(/\|/);
   #$lastname =~ s/\s+//g;
   for( $lastname ){ s/^\s+//; s/\s+$//; }
   $lastname = ucfirst(lc($lastname));
   for( $firstname ){ s/^\s+//; s/\s+$//; }
   $firstname = ucfirst(lc($firstname));
   $wtid = lc($wtid);
   $wtid =~ s/\s+//g;
   $psid =~ s/\s+//g;
   while (length($psid) < 7) {
      $psid = "0" . $psid;
   }
   for( $description ){ s/^\s+//; s/\s+$//; }
   for( $template ){ s/^\s+//; s/\s+$//; }
   chomp($streetaddress);
   $mail = join '', $firstname, '.', $lastname,'@umuc.edu';
   
   if ($wtid && $psid) {
      print primary_ldif "dn: CN=", $wtid, ",OU=People,DC=us,DC=umuc,DC=edu\n";
      print primary_ldif "changetype: add\n";
      print primary_ldif "cn: ", $wtid, "\n";
      print primary_ldif "objectClass: user\n";
      print primary_ldif "sAMAccountName: ", $wtid, "\n";
      print primary_ldif "givenName: ", $firstname, "\n";
      print primary_ldif "sn: ", $lastname, "\n\n";

      print secondary_ldif "dn: CN=", $wtid, ",OU=People,DC=us,DC=umuc,DC=edu\n";
      print secondary_ldif "changetype: modify\n";
      print secondary_ldif "replace: displayName\n";
      print secondary_ldif "displayName: ", $firstname, " ", $lastname, "\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: scriptPath\n";
      print secondary_ldif "scriptPath: logon.bat\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: userPrincipalName\n";
      print secondary_ldif "userPrincipalName: ", $wtid, "\@us.umuc.edu\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: description\n";
      print secondary_ldif "description: ", $description, "\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: employeeID\n";
      print secondary_ldif "employeeID: ", $psid, "\n";
      print secondary_ldif "-\n";
	  
	  print secondary_ldif "dn: CN=", $wtid, ",OU=People,DC=us,DC=umuc,DC=edu\n";
      print secondary_ldif "changetype: modify\n";
      print secondary_ldif "replace: mail\n";
      print secondary_ldif "Mail: ", $mail, "\n";
      print secondary_ldif "-\n";
	  
      print secondary_ldif "replace: company\n";
      print secondary_ldif "company: University of Maryland University College\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: title\n";
      print secondary_ldif "title: ", $title, "\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: department\n";
      print secondary_ldif "department: ", $department, "\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: telephoneNumber\n";
      print secondary_ldif "telephoneNumber: ", $telephonenumber, "\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: ipPhone\n";
      print secondary_ldif "ipPhone: ", $ipphone, "\n";
      print secondary_ldif "-\n";
      print secondary_ldif "replace: streetAddress\n";
      print secondary_ldif "streetAddress: ", $streetaddress, "\n";
      print secondary_ldif "-\n\n";
      
      print member_template $template, ",", $wtid, "\n";
   }
}

close(member_template);
close(primary_ldif);
close(secondary_ldif);
close(raw_account_datafile);
