usernameFile = wscript.arguments(0)

Set ofs = createobject("scripting.filesystemobject")

Set ousernameFile = ofs.opentextfile(usernameFile , 1, false)

While not ousernameFile.atEndOfStream
   InputLine = split(ousernameFile.ReadLine(),",")
   username = InputLine(0)
   password = InputLine(1)
   wscript.echo "Username: " & username
   wscript.echo "Password: " & password
   wscript.echo
   Set objUser = GetObject _
      ("LDAP://cn=" & username & ",ou=people,dc=us,dc=umuc,dc=edu")
   objUser.SetPassword password
   objUser.AccountDisabled = False
	 objUser.Put "pwdLastSet", 0
   objUser.SetInfo
Wend

ousernameFile.close()

