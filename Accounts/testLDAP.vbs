' This code shows how to enable SSL and secure authentication using ADO:

' Constants taken from ADS_AUTHENTICATION_ENUM
ADS_SECURE_AUTHENTICATION = 1
ADS_USE_SSL = 2

set objConn = CreateObject("ADODB.Connection")
objConn.Provider = "ADsDSOObject"
objConn.Properties("umucGUID") = "umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
objConn.Properties("Password") = "1@#mungunimzuri"
'guid = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
'password = "1@#mungunimzuri"
objConn.Properties("Encrypt Password") = True
objConn.Properties("ADSI Flag") = ADS_SECURE_AUTHENTICATION + ADS_USE_SSL
objConn.Open "Active Directory Provider", guid, password
vSearch = "uid=mchirchir"
set objRS = objConn.Execute("<LDAP://usv-ldpapp-02.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

objRS.MoveFirst
while Not objRS.EOF
    Wscript.Echo objRS.Fields(0).Value
    objRS.MoveNext
Wend
