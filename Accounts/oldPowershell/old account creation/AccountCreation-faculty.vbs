'******************************************************************************
'Account Creation
'Created by Mark Chirchir
'Version 8.0
'Updated email creation logic
'Resolved file access issues
'Update VPN logic
'Update check for spaces
'Update faculty info
'Enhancement ENHC0010197
'Enhancement ENHC0010482
'Completed Google group creation and cloning

'******************************************************************************
'******************************************************************************
'Class Declarations
'******************************************************************************
Class usrClass
    Private givenName
    Private sn
    Private uid
    Private umucEmplId
    Private umucGUID
    Private umucPrefFirstName
    Private umucCampusEmail
    Private umucEmployeeEmail
    Private umucFacultyEmail
    Private eduPersonPrimaryAffiliation
    Private umucVPNGroup
    Private title
    Private description
    Private department
	Private eduPersonPrimaryOrgUnitDN
	Private umucManagerEmail
	Private umucEmployeeAsia
	Private umucEmployeeEuro
	Private umucEmployeeUSA
    
    Property Get firstName()
        firstName = givenName
    End Property

    Property Get lastName()
        lastName = sn
    End Property

    Property Get userid()
        userid = uid
    End Property

    Property Get emplID()
        emplID = umucEmplId
    End Property

    Property Get ldapGUID()
        ldapGUID = umucGUID
    End Property

    Property Get prefFirstName()
        prefFirstName = umucPrefFirstName
    End Property

    Property Get campusEmail()
        campusEmail = umucCampusEmail
    End Property

    Property Get employeeEmail()
        employeeEmail = umucEmployeeEmail
    End Property
    
    Property Get facultyEmail()
        facultyEmail = umucFacultyEmail
    End Property

    Property Get personPrimaryAffiliation()
        personPrimaryAffiliation = eduPersonPrimaryAffiliation
    End Property

    Property Get vpnGroup()
        vpnGroup = umucVPNGroup
    End Property

    Property Get webexGroup()
        webexGroup = umucWebexGroup
    End Property
    
    Property Get userTitle()
        userTitle = title
    End Property

    Property Get userDescription()
        userdescription = description
    End Property

    Property Get userDepartment()
        userdepartment = department
    End Property
    
	Property Get personPrimaryOrgUnitDN()
        personPrimaryOrgUnitDN = eduPersonPrimaryOrgUnitDN
    End Property
    
    Property Get managerEmail()
        managerEmail = umucManagerEmail
    End Property
    
    Property Get employeeAsia()
        employeeAsia = umucEmployeeAsia
    End Property
    
    Property Get employeeEuro()
        employeeEuro = umucEmployeeEuro
    End Property
    
    Property Get employeeUSA()
        employeeUSA = umucEmployeeUSA
    End Property
            
    Property Let firstName(NewVal)
        givenName = NewVal
    End Property

    Property Let lastName(NewVal)
        sn = NewVal
    End Property

    Property Let userid(NewVal)
        uid = NewVal
    End Property

    Property Let emplID(NewVal)
        umucEmplId = NewVal
    End Property

    Property Let ldapGUID(NewVal)
        umucGUID = NewVal
    End Property

    Property Let prefFirstName(NewVal)
        umucPrefFirstName = NewVal
    End Property

    Property Let campusEmail(NewVal)
        umucCampusEmail = NewVal
    End Property

    Property Let employeeEmail(NewVal)
        umucEmployeeEmail = NewVal
    End Property
    
    Property Let facultyEmail(NewVal)
        umucFacultyEmail = NewVal
    End Property

    Property Let personPrimaryAffiliation(NewVal)
        eduPersonPrimaryAffiliation = NewVal
    End Property

    Property Let vpnGroup(NewVal)
        umucVPNGroup = NewVal
    End Property
    
    Property Let webexGroup(NewVal)
        umucWebexGroup = NewVal
    End Property
    
    Property Let userTitle(NewVal)
        title = NewVal	
    End Property
    
    Property Let userdescription(NewVal)
        description = NewVal
    End Property

    Property Let userdepartment(NewVal)
        department = NewVal
    End Property
    	
	Property Let personPrimaryOrgUnitDN(NewVal)
        eduPersonPrimaryOrgUnitDN = NewVal
    End Property
    
    Property Let managerEmail(NewVal)
        umucManagerEmail = NewVal
    End Property
    
    Property Let employeeAsia(NewVal)
        umucEmployeeAsia = NewVal
    End Property
    
    Property Let employeeEuro(NewVal)
        umucEmployeeEuro = NewVal
    End Property
    
    Property Let employeeUSA(NewVal)
        umucEmployeeUSA = NewVal
    End Property
    
End Class
	
'******************************************************************************
'Constant variables
'******************************************************************************
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNTDISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32
Const ADS_PROPERTY_CLEAR = 1
Const ADS_SECURE_AUTHENTICATION = 1
Const ADS_USE_ENCRYPTION = 2
Const ForAppending = 8
Const ForReading = 1
Const ForWriting = 2

'******************************************************************************
'Global variables
'******************************************************************************
Dim strPassword
Dim duplicateEmail
Dim sDN
Dim sRoot
Dim sUser
Dim dso
Dim oAuth
Dim oConn
Dim usrObj
Dim usrObjClone
Dim acctIsThere
Dim asiaOutput
Dim europeOutput
Dim europeAdjunctOutput
Dim usOutput

'Initialize variables

Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")

Set outputFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\output.txt",2,true)
outputFile.WriteLine("Username, Password, Email Address")

Set delegateFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\updates.csv",2,true)
delegateFile.WriteLine("Template,Target")


'******************************************************************************
'User credentials
'******************************************************************************
sUser = "us\mchirchir_eu"
'ldapmasterQAT sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
sDN = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
'sRoot = "LDAP://ldapmaster.umuc.edu/dc=umuc,dc=edu"
sRoot = "LDAP://usv-ldpapp-04.us.umuc.edu"
Password("Password:")

'******************************************************************************
'Get User Input
'******************************************************************************
Input = 0
While Input < 13
		
	WScript.Echo ""
	WScript.Echo "Please choose from the following list"
	WScript.Echo "1. Create User Email in LDAP"
	WScript.Echo "2. Create Faculty Email Account"
	WScript.Echo "3. Delete User Mailbox and Print Separation Template"
	WScript.Echo "4. Print Faculty email"
	WScript.Echo "5. Create User AD and Email Account"
	WScript.Echo "6. Copy Group memberships"
	WScript.Echo "7. Print User ID from Email"
	WScript.Echo "8. Print User ID from Faculty Email"
	WScript.Echo "9. Print UMUC Guid"
	WScript.Echo "10. Remove AD Groups"
	WScript.Echo "11. Send Emails"
	WScript.Echo "12. Test"
	WScript.Echo "13. End Program"
	WScript.Echo ""
	
	WScript.StdIn.Read(0)
	Input = WScript.StdIn.ReadLine
	
	If Input = 2 Then
		Set appOps = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\appOps.txt",2,true)
		Set facultyAsiaOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Asia_AD_Faculty.txt",2,true)
		Set facultyUSOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\USA_AD_Faculty.txt",2,true)
		Set facultyEuropeOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty.txt",2,true)
		Set facultyEuropeAdjunctOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty-Adjunct.txt",2,true)
		acctIsThere = 0
		duplicateEmail = 0
		asiaOutput = 0
		europeOutput = 0
		europeAdjunctOutput = 0
		usOutput = 0
	End If
	
	If Input = 4 Then
		Set appOps = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\appOps.txt",2,true)
		Set facultyAsiaOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Asia_AD_Faculty.txt",2,true)
		Set facultyUSOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\USA_AD_Faculty.txt",2,true)
		Set facultyEuropeOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty.txt",2,true)
		Set facultyEuropeAdjunctOutput = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\Europe_AD_Faculty-Adjunct.txt",2,true)
		acctIsThere = 0
		duplicateEmail = 0
		asiaOutput = 0
		europeOutput = 0
		europeAdjunctOutput = 0
		usOutput = 0
	End If
	
'Call the main logic	
	If Input < 13 then
		
		main(Input)
		
	End If
	
	If Input = 13 Then
		delegateFile.Close
		Set delegateFile = nothing
		outputFile.Close
		Set outputFile = nothing
	End If
	
	
	If Input = 2 Then
		appOps.Close
		facultyAsiaOutput.Close
		facultyEuropeOutput.Close
		facultyUSOutput.Close
		facultyEuropeAdjunctOutput.Close
	End If
	
	If Input = 4 Then
		appOps.Close
		facultyAsiaOutput.Close
		facultyEuropeOutput.Close
		facultyUSOutput.Close
		facultyEuropeAdjunctOutput.Close
	End If
Wend

'******************************************************************************
'Main Function
'******************************************************************************
Function main(userInput)

Set dso = GetObject("LDAP:")
Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword

Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile("c:\Scratch\Accounts\emplid.txt" , 1, false)

'Run the main logic
While not ousernameFile.AtEndOfStream
	useriden = ousernameFile.ReadLine()
	
	'Create new User	
	Set usrObj = New usrClass
	Set usrObjClone = New usrClass
	vsearch = "(umucemplid=" & useriden & ")"

	Select Case userInput
  		Case 1
  			'Update Staff
	    	ldapExtract(vsearch)
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
				WScript.Echo usrObj.lastName & "|" & usrObj.firstName & "|" & usrObj.userid & "|SSN|" & usrObj.emplID & "|"
			End If
			duplicateEmail = 0
						
  		Case 2
  			'Update Faculty
	    	facultyLdapExtract(vsearch)
			checkFacultyEmail
			If duplicateEmail = 0 Then
				updateFacultyEmail
				WScript.Echo usrObj.firstName & "," & usrObj.prefFirstName & "," & usrObj.lastName & "," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.facultyEmail
				If usrObj.personPrimaryOrgUnitDN = "USA" Then
					facultyUSOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					usOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
					facultyAsiaOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					asiaOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
					facultyEuropeOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					europeOutput = 1
					adjunctTitle = LCase(Mid(usrObj.userTitle,1,3))
					If adjunctTitle = "adj" Then
						facultyEuropeAdjunctOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
						europeAdjunctOutput = 1
					End If
				Else
					WScript.Echo "Missing the eduPersonPrimaryOrgUnitDN"
				End If	
				
				appOps.WriteLine("CREATE," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.firstName & "," & usrObj.lastName & ",,Learner,1," & usrObj.facultyemail)	
			End If
			duplicateEmail = 0
  		Case 3
  			'Separate Account
	    	ldapExtract(vsearch)
	    	If Not (usrObj.personPrimaryAffiliation = "employee") Then
	    	  	If usrObj.employeeUSA = "Y" Then
					removeAllADGroupsUS
					disableAccountUS
					removeVPNGroup
					removeWebexGroup
					autoReply
					printSeparation
				ElseIf usrObj.employeeAsia = "Y" Then
					removeAllADGroupsAsia
					disableAccountAsia
					removeVPNGroup
					removeWebexGroup
					autoReply
					printSeparation
				ElseIf usrObj.employeeEuro = "Y" Then
					removeAllADGroupsEurope
					disableAccountEurope
					removeVPNGroup
					removeWebexGroup
					autoReply
					printSeparation
				Else
					WScript.Echo "User is not a member of any Employee related divisions"
				End If
			End If
			
  		Case 4
  			'Print Faculty email
			facultyLdapExtract(vsearch)
			WScript.Echo usrObj.firstName & "," & usrObj.prefFirstName & "," & usrObj.lastName & "," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.facultyEmail & "," & usrObj.personPrimaryOrgUnitDN
				If usrObj.personPrimaryOrgUnitDN = "USA" Then
					facultyUSOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					usOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
					facultyAsiaOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					asiaOutput = 1
				ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
					facultyEuropeOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
					europeOutput = 1
					adjunctTitle = LCase(Mid(usrObj.userTitle,1,3))
					If adjunctTitle = "adj" Then
						facultyEuropeAdjunctOutput.WriteLine(usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail)
						europeAdjunctOutput = 1
					End If
				Else
					WScript.Echo "Missing the eduPersonPrimaryOrgUnitDN"
				End If	
			
			appOps.WriteLine("CREATE," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.firstName & "," & usrObj.lastName & ",,Learner,1," & usrObj.facultyemail)

		Case 5
  			'Update Staff
  			
  			InputLine = split(useriden,",")
			newEmployee = InputLine(0)
			oldEmployee = InputLine(1)
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
			If IsNumeric(oldEmployee) Then
				oldemp = "(umucemplid=" & oldEmployee & ")"
			Else
				oldemp = "(uid=" & oldEmployee & ")"
			End If
			
	    	ldapExtract(newemp)  	
	    	ldapExtractClone(oldemp)
	    	
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
			End If

			findAccount
			If accountExists = 0 Then
				If usrObj.personPrimaryOrgUnitDN = "USA" Then
					CreateADAccountUS
					WScript.Sleep 5000
					CopyADGroupsUS
					copyGoogleGroups
					updateVPN
					updateWebex
					
				ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
					CreateADAccountEurope
					WScript.Sleep 5000
					CopyADGroupsEurope
					copyGoogleGroups
					updateVPN
					updateWebex
				
				ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
					CreateADAccountAsia
					WScript.Sleep 5000
					CopyADGroupsAsia
					copyGoogleGroups
					updateVPN
					updateWebex
					
				Else
					WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
				End If
				
			Else
				'do nothing	
			End If
			
			accountExists =0
			duplicateEmail = 0

  		Case 6
  			'Copy Groups
			InputLine = split(useriden,",")
			newEmployee = InputLine(0)
			oldEmployee = InputLine(1)
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
			If IsNumeric(oldEmployee) Then
				oldemp = "(umucemplid=" & oldEmployee & ")"
			Else
				oldemp = "(uid=" & oldEmployee & ")"
			End If
			
	    	ldapExtract(newemp)  	
	    	ldapExtractClone(oldemp)

			If usrObj.personPrimaryOrgUnitDN = "USA" Then
				CopyADGroupsUS
				copyGoogleGroups
				updateVPN
				updateWebex
			
			ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
				CopyADGroupsEurope
				copyGoogleGroups
				updateVPN
				updateWebex
	
			ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
				CopyADGroupsAsia
				copyGoogleGroups
				updateVPN
				updateWebex
					
			Else
				WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
			End If
			
  		Case 7
  			'Print user email
			vsearch = "(umucEmployeeEmail= " & useriden & ")"
			on error resume next
			ldapExtract(vsearch)
			'outputFile.Writeline (usrObj.userid & "|" & usrObj.emplID & "|" & usrObj.employeeemail)
			WScript.Echo usrObj.firstName & "," & usrObj.prefFirstName & "," & usrObj.lastName & "," & usrObj.userid & "," & usrObj.emplID & "," & usrObj.managerEmail
						
  		Case 8
  			'Print faculty email
			vsearch = "(umucFacultyEmail= " & useriden & ")"
			on error resume next
			facultyLdapExtract(vsearch)
			outputFile.Writeline (usrObj.userid & "|" & usrObj.emplID & "|" & usrObj.facultyemail)
			
  		Case 9
  			'Update Staff
	    	ldapExtract(vsearch)
  		
  		Case 10
  			'Remove Groups
			newEmployee = useriden
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
	    	ldapExtract(newemp)	
	    	
	    	If usrObj.personPrimaryOrgUnitDN = "USA" Then
				removeAllADGroupsUS
				removeVPNGroup
				removeWebexGroup
			
			ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
				removeAllADGroupsEurope
				removeVPNGroup
				removeWebexGroup
	
			ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
				removeAllADGroupsAsia
				removeVPNGroup
				removeWebexGroup
					
			Else
				WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
			End If

  		Case 11
  		
  			'sendAppOpsEmail Need to work with Kevin to create inbound rules to create a service request
  			If usOutput = 1 Then
				sendFacultySvcsEmailUS
				usOutput = 0
			End If
			If asiaOutput = 1 Then
				sendFacultySvcsEmailAsia
				asiaOutput = 0
			End If
			If europeOutput = 1 Then
				sendFacultySvcsEmailEurope
				europeOutput = 0
			End If
			If europeAdjunctOutput = 1 Then
				sendFacultySvcsEmailEuropeAdjunct
				europeAdjunctOutput = 0
			End If
  
  		Case 12
			If europeAdjunctOutput = 1 Then
				sendFacultySvcsEmailEuropeAdjunct
				europeAdjunctOutput = 0			
			End if
  			
			
	End Select
	
	Set usrobj = nothing
	Set usrObjClone = nothing
	
Wend

End Function

'******************************************************************************
'Get User Password
'******************************************************************************
Function Password(myPrompt)
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = nothing
End Function

'******************************************************************************
'Populate user object
'******************************************************************************
Function ldapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,title,umucPrefFirstName,umucCampusEmail,umucEmployeeEmail,eduPersonPrimaryAffiliation,umucGUID, eduPersonPrimaryOrgUnitDN, umucManagerEmail, umucEmployeeAsia, umucEmployeeEuro, umucEmployeeUSA;subtree")

For Each Element In oRS.Fields(0).Value
	var1 = Element
	var1 = Trim(var1)
	var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
	var2 = Replace(var2," ","",1,-1)
	usrObj.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
	var2 = Replace(var2," ","",1,-1)
	usrObj.lastName = var2
Next
For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

If IsNull(oRS.Fields(4).Value) Then
	usrObj.userTitle = "TBD"
Else
For Each Element In oRS.Fields(4).Value
	usrObj.userTitle = Element
Next
End If

If IsNull(oRS.Fields(5).Value) Then
	usrObj.prefFirstName = usrObj.firstName
Else
	var1 = oRS.Fields(5).Value
	var1 = Trim(var1)
	var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
	var2 = Replace(var2," ","",1,-1)
	usrObj.prefFirstName = var2
End If

If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
Else
For Each Element In oRS.Fields(6).Value
	usrObj.campusEmail = Element
Next
End If

If IsNull(oRS.Fields(7).Value) Then
	usrObj.employeeEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
Else
	usrObj.employeeEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

usrObj.ldapGUID = oRS.Fields(9).Value

If IsNull(oRS.Fields(10).Value) Then
	usrObj.personPrimaryOrgUnitDN = "nothing"
Else
	For Each Element In oRS.Fields(10).Value
		var1 = Element
		var1 = Trim(var1)
		var2 = split(var1,"=")
		var3 = var2(0)
		var4 = var2(1)
		usrObj.personPrimaryOrgUnitDN = var4
	Next
End If

If IsNull(oRS.Fields(11).Value) Then
	usrObj.managerEmail = "nothing"
Else
	usrObj.managerEmail = oRS.Fields(11).Value
End If

If IsNull(oRS.Fields(12).Value) Then
	usrObj.employeeAsia = "N"
Else
	usrObj.employeeAsia = oRS.Fields(12).Value
End If


If IsNull(oRS.Fields(13).Value) Then
	usrObj.employeeEuro = "N"
Else
	usrObj.employeeEuro = oRS.Fields(13).Value
End If
WScript.Echo usrObj.employeeEuro

If IsNull(oRS.Fields(14).Value) Then
	usrObj.employeeUSA = "N"
Else
	usrObj.employeeUSA = oRS.Fields(14).Value
End If

End Function

'******************************************************************************
'Populate faculty user object
'******************************************************************************
Function facultyLdapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucGUID,umucPrefFirstName,umucCampusEmail,umucFacultyEmail,eduPersonPrimaryAffiliation,umucVPNGroup,eduPersonPrimaryOrgUnitDN,umucManagerEmail,title;subtree")

If IsNull(oRS.Fields(0).Value) Then
	usrObj.firstName = "nothing"
Else	
	For Each Element In oRS.Fields(0).Value
		var1 = Element
		var1 = Trim(var1)
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.firstName = var2
	Next
End If

If IsNull(oRS.Fields(1).Value) Then
	usrObj.lastName = "nothing"
Else
	For Each Element In oRS.Fields(1).Value
		var1 = Element
		var1 = Trim(var1)
		var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
		var2 = Replace(var2," ","",1,-1)
		usrObj.lastName = var2
	Next
End If

For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

usrObj.ldapGUID = oRS.Fields(4).Value

If IsNull(oRS.Fields(5).Value) Then
	usrObj.prefFirstName = usrObj.firstName
Else
	var1 = oRS.Fields(5).Value
	var1 = Trim(var1)
	var2 = Ucase(mid(var1,1,1)) & LCase(mid(var1,2))
	var2 = Replace(var2," ","",1,-1)
	usrObj.prefFirstName = var2
End If


If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
Else
	For Each Element In oRS.Fields(6).Value
		usrObj.campusEmail = Element
	Next
End If

If IsNull(oRS.Fields(7).Value) Then
	usrObj.facultyEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
Else
	usrObj.facultyEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

If IsNull(oRS.Fields(9).Value) Then
	usrObj.vpnGroup = oRS.Fields(9).Value
else
	usrObj.vpnGroup = Element
End If

If IsNull(oRS.Fields(10).Value) Then
	usrObj.personPrimaryOrgUnitDN = "nothing"
Else
	For Each Element In oRS.Fields(10).Value
		var1 = Element
		var1 = Trim(var1)
		var2 = split(var1,"=")
		var3 = var2(0)
		var4 = var2(1)
		usrObj.personPrimaryOrgUnitDN = var4
	Next
End If 

If IsNull(oRS.Fields(11).Value) Then
	usrObj.managerEmail = "nothing"
Else
	usrObj.managerEmail = oRS.Fields(11).Value
End If

If IsNull(oRS.Fields(12).Value) Then
	usrObj.userTitle = "TBD"
Else
For Each Element In oRS.Fields(12).Value
	usrObj.userTitle = Element
Next
End If

End Function

'******************************************************************************
'Populate clone object
'******************************************************************************
Function ldapExtractClone(vsearch)

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucEmployeeEmail,umucVPNGroup, eduPersonPrimaryOrgUnitDN, umucWebexGroup;subtree")


For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObjClone.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObjClone.lastName = var2
Next

For Each Element In oRS.Fields(2).Value
	usrObjClone.userid = Element
Next

usrObjClone.emplID = oRS.Fields(3).Value

usrObjClone.employeeEmail = oRS.Fields(4).Value

If IsNull(oRS.Fields(5).Value) Then
	usrObjClone.vpnGroup = "nothing"
else
	For Each Element In oRS.Fields(5).Value
		usrObjClone.vpnGroup = Element
	Next
End If

For Each Element In oRS.Fields(6).Value
	var1 = Element
	var1 = Trim(var1)
	var2 = split(var1,"=")
	var3 = var2(0)
	var4 = var2(1)
	usrObj.personPrimaryOrgUnitDN = var4
Next

If IsNull(oRS.Fields(7).Value) Then
	usrObjClone.webexGroup = "nothing"
else
	For Each Element In oRS.Fields(7).Value
		usrObjClone.webexGroup = Element
	Next
End If

End Function

'******************************************************************************
'Generate random password
'******************************************************************************
randomize
dim arrPassA(9)
strResult = ""
arrSymbols = Array("!","#","$","%","&","(",")","*","+",",","-",".",":",";","<","=",">","?","[","]","^","_","{","}","~")
 
do while strResult = ""
    strPass = ""
    do while len(strPass) < 6
        intAsc = int(rnd * 57) + 65
        if intAsc < 91 or intAsc > 96 then
            strPass = strPass & chr(intAsc)
        end if
    loop
 
    strPass = strPass & cstr(int(rnd*10)) & arrSymbols(int(rnd*ubound(arrSymbols)))
 
    for i = 0 to 8
        arrPassA(i) = mid(strPass,i+1,1)
    next
 
    strPass = ""
 
    booMore = TRUE
 
    do
        intIdx = int(rnd*9)
        if arrPassA(intIdx) <> "" then
            strPass = strPass & arrPassA(intIdx)
            arrPassA(intIdx) = ""
        else
            booMore = FALSE
            for x = 0 to 9
                if arrPassA(x) <> "" then
                    booMore = TRUE
                    exit for
                end if
            next
        end if
        if not booMore then
            exit do
        end if
    loop
 
    if lcase(strPass) <> strPass and ucase(strPass) <> strPass then
 		RndPassword = strPass
 		strResult = strPass
    end if
Loop


End Function

'******************************************************************************
' Search for a User Account in Active Directory
'******************************************************************************
Function findAccount

dtStart = TimeValue(Now())

Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = _
    "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & _
         "(samAccountName=" & usrObj.userID & "));samAccountName;subtree"
  
Set objRecordSet = objCommand.Execute
 
If objRecordset.RecordCount = 0 Then
    'do nothing
Else
    WScript.Echo usrObj.userID & " Already Exists in Active Directory."
    acctIsThere = 1
End If
 
objConnection.Close

End Function

'******************************************************************************
'Create user account in AD
'******************************************************************************
Function CreateADAccount()

If usrObj.personPrimaryOrgUnitDN = USA Then
    CreateADAccountUS
ElseIf usrObj.personPrimaryOrgUnitDN = ASIA Then
	CreateADAccountAS
ElseIf usrObj.personPrimaryOrgUnitDN = EURO Then
	CreateADAccountAS
Else
	'do nothing
End If
	

End Function


'******************************************************************************
'Create user account in Staeside AD
'******************************************************************************
Function CreateADAccountUS()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@us.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Create user account in  Europe AD
'******************************************************************************
Function CreateADAccountEurope()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@europe.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,dc=europe,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/OU=People,dc=europe,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Europe"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College Europe"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Create user account in Asia AD
'******************************************************************************
Function CreateADAccountAsia()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@asia.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,DC=asia,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/OU=People,DC=asia,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Asia"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function CopyADGroupsUS()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=us,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://adeprdcns02.us.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function CopyADGroupsEurope()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function CopyADGroupsAsia()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsUS()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsEurope()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsAsia()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function


'******************************************************************************
'Check LDAP for email duplicates
'******************************************************************************
Function checkEmail()

vsearch = "(umucEmployeeEmail=" & usrObj.employeeEmail & ")"

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Check LDAP for faculty email duplicates
'******************************************************************************
Function checkFacultyEmail()

vsearch = "(umucFacultyEmail=" & usrObj.facultyEmail & ")"

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Faculty Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Populate faculty email
'******************************************************************************
Function updateFacultyEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucFacultyEmail", usrObj.facultyEmail
obj.setinfo

End Function

'******************************************************************************
'Populate staff email
'******************************************************************************
Function updateEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucEmployeeEmail", usrObj.employeeEmail
obj.setinfo

WScript.Echo "Email address created in LDAP: " & usrObj.employeeEmail

End Function

'******************************************************************************
'Populate vpn group
'******************************************************************************
Function updateVPN()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
If usrObjClone.vpngroup = "nothing" Then
	'do nothing
Else
	obj.put "umucVPNGroup", usrObjClone.vpnGroup
End If

obj.setinfo

End Function

'******************************************************************************
'Populate vpn group
'******************************************************************************
Function removeVPNGroup()
On Error Resume Next
'LDAP delete vpnGroup
WScript.Echo "Remove VPN Group"

If usrObj.vpngroup = "nothing" Then
	'do nothing
Else
	ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
	Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucVPNGroup", 0
	obj.setinfo
	WScript.Echo usrObj.vpngroup
End If

WScript.Echo "VPN Group Removed"

End Function

'******************************************************************************
'Populate UMUCWebExGroup group
'******************************************************************************
Function updateWebex()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
If usrObjClone.webexgroup = "nothing" Then
	'do nothing
Else
	obj.put "umucWebexGroup", usrObjClone.webexGroup
End If

obj.setinfo

End Function

'******************************************************************************
'Populate UMUCWebExGroup group
'******************************************************************************
Function removeWebexGroup()
On Error Resume Next
'LDAP delete vpnGroup

WScript.Echo "Remove WebexGroup"

If usrObj.webexgroup = "nothing" Then
	'do nothing
Else
	ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
	Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucWebexGroup", 0
	obj.setinfo
End If

WScript.Echo "Webex Group Removed"

End Function

'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccountUS()
sDomain = "us"
Dim localUID, localEmplID, recordExists
WScript.Echo "Disable US Account"
recordExists = 1
vsearch = "(umucemplid=" & usrObj.emplID & ")"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucEmployeeEmail;subtree")

For Each Element In oRS.Fields(0).Value
	localUID = Element
Next

localEmplID = oRS.Fields(1).Value

If IsNull(oRS.Fields(2).Value) Then
	'do nothing
Else
	Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
	obj.setinfo
End If

'Ad Disable account




' Search for a User Account in Active Directory
strUserName = usrObj.userID
dtStart = TimeValue(Now())
Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & "(samAccountName=" & strUserName & "));distinguishedName;subtree"
  
Set objRecordSet = objCommand.Execute

If objRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    recordExists = objRecordset.RecordCount
Else
	userDN = objRecordSet.Fields("distinguishedName").Value
End If

objConnection.Close

userDN = "CN=" & usrObj.userID & ",OU=People,dc=us,dc=umuc,dc=edu"

If recordExists = 0 Then
	'do nothing
Else
	Set objDSO = GetObject("LDAP:")
	Set objDisable = objDSO.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& userDN,sUser,strPassword, &H0200)
		intUAC = objDisable.Get("userAccountControl") 
  		objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
  		 WScript.Echo "3"
		objDisable.SetInfo 
	sDestOU = "LDAP://adeprdcns02.us.umuc.edu/OU=Inactive,dc=us,dc=umuc,dc=edu"
	Set objRootDSE = GetObject("LDAP:")
	Set objDestOU = objRootDSE.OpenDSObject(sDestOU, sUser, strPassword, ADS_SECURE_AUTHENTICATION)
	objDestOU.MoveHere "LDAP://adeprdcns02.us.umuc.edu/" & userDn, vbNullString
End If
WScript.Echo "US Account disabled"
End Function

'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccountEurope()
sDomain = "europe"
Dim localUID, localEmplID, recordExists
WScript.Echo "Disable Europe Account"
recordExists = 1
vsearch = "(umucemplid=" & usrObj.emplID & ")"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucEmployeeEmail;subtree")

For Each Element In oRS.Fields(0).Value
	localUID = Element
Next

localEmplID = oRS.Fields(1).Value

If IsNull(oRS.Fields(2).Value) Then
	'do nothing
Else
	Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
	obj.setinfo
End If

'Ad Disable account




' Search for a User Account in Active Directory
strUserName = usrObj.userID
dtStart = TimeValue(Now())
Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = "<LDAP://dc=europe,dc=umuc,dc=edu>;(&(objectCategory=User)" & "(samAccountName=" & strUserName & "));distinguishedName;subtree"
  
Set objRecordSet = objCommand.Execute

If objRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    recordExists = objRecordset.RecordCount
Else
	userDN = objRecordSet.Fields("distinguishedName").Value
End If

objConnection.Close

userDN = "CN=" & usrObj.userID & ",OU=People,dc=europe,dc=umuc,dc=edu"

If recordExists = 0 Then
	'do nothing
Else
	Set objDSO = GetObject("LDAP:")
	Set objDisable = objDSO.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& userDN,sUser,strPassword, &H0200)
		intUAC = objDisable.Get("userAccountControl") 
  		objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
  		 WScript.Echo "3"
		objDisable.SetInfo 
	sDestOU = "LDAP://EUI-OPSDOM-02.europe.umuc.edu/OU=Inactive,dc=us,dc=umuc,dc=edu"
	Set objRootDSE = GetObject("LDAP:")
	Set objDestOU = objRootDSE.OpenDSObject(sDestOU, sUser, strPassword, ADS_SECURE_AUTHENTICATION)
	objDestOU.MoveHere "LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & userDn, vbNullString
End If
WScript.Echo "US Account disabled"

End Function

'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccountAsia()
sDomain = "europe"
Dim localUID, localEmplID, recordExists
WScript.Echo "Disable Europe Account"
recordExists = 1
vsearch = "(umucemplid=" & usrObj.emplID & ")"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set oRS = oConn.Execute("<LDAP://ldap.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";uid,umucEmplID,umucEmployeeEmail;subtree")

For Each Element In oRS.Fields(0).Value
	localUID = Element
Next

localEmplID = oRS.Fields(1).Value

If IsNull(oRS.Fields(2).Value) Then
	'do nothing
Else
	Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
	obj.setinfo
End If

'Ad Disable account

' Search for a User Account in Active Directory
strUserName = usrObj.userID
dtStart = TimeValue(Now())
Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = "<LDAP://dc=asia,dc=umuc,dc=edu>;(&(objectCategory=User)" & "(samAccountName=" & strUserName & "));distinguishedName;subtree"
  
Set objRecordSet = objCommand.Execute

If objRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
    recordExists = objRecordset.RecordCount
Else
	userDN = objRecordSet.Fields("distinguishedName").Value
End If

objConnection.Close

userDN = "CN=" & usrObj.userID & ",OU=People,dc=asia,dc=umuc,dc=edu"

If recordExists = 0 Then
	'do nothing
Else
	Set objDSO = GetObject("LDAP:")
	Set objDisable = objDSO.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& userDN,sUser,strPassword, &H0200)
		intUAC = objDisable.Get("userAccountControl") 
  		objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
  		 WScript.Echo "3"
		objDisable.SetInfo 
	sDestOU = "LDAP://EUI-OPSDOM-05.asia.umuc.edu/OU=Inactive,dc=us,dc=umuc,dc=edu"
	Set objRootDSE = GetObject("LDAP:")
	Set objDestOU = objRootDSE.OpenDSObject(sDestOU, sUser, strPassword, ADS_SECURE_AUTHENTICATION)
	objDestOU.MoveHere "LDAP://EUI-OPSDOM-05.asia.umuc.edu/" & userDn, vbNullString
End If
WScript.Echo "US Account disabled"

End Function

'******************************************************************************
'Print template for HR submission
'******************************************************************************
Function printSeparation()

WScript.Echo "Name: " & usrObj.firstName & " " & usrObj.lastName
WScript.Echo "Login ID: " & usrObj.userID
WScript.Echo "EmplID: " & usrObj.emplID
WScript.Echo

End Function

'******************************************************************************
'Send email to App Ops
'******************************************************************************
Function sendAppOpsEmail()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Mark.Chirchir@umuc.edu"
objEmail.To = "servicedesk@umuc.edu"
objEmail.Subject = "Provision Faculty - " & setDate 
objEmail.Textbody = "Please assign this ticket to Applications Operations for LEO account creations."
objEmail.AddAttachment "C:\Scratch\Accounts\appOps.txt"


objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services
'******************************************************************************
Function sendFacultySvcsEmail()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Mark.Chirchir@umuc.edu"
objEmail.To = "Amanda.Stapleton@umuc.edu;Nakisha.Williams@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;Contracts@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\USA_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services US
'******************************************************************************
Function sendFacultySvcsEmailUS()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Mark.Chirchir@umuc.edu"
objEmail.To = "Amanda.Stapleton@umuc.edu;Nakisha.Williams@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;Contracts@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\USA_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services Asia
'******************************************************************************
Function sendFacultySvcsEmailAsia()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Mark.Chirchir@umuc.edu"
objEmail.To = "HRFaculty-Asia@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;David.Schultz@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\Asia_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services Europe
'******************************************************************************
Function sendFacultySvcsEmailEurope()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Mark.Chirchir@umuc.edu"
objEmail.To = "HRPS-europe@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;Director-europe@umuc.edu;Operations-europe@umuc.edu;Downrange-europe@umuc.edu;Dean-europe@umuc.edu,Timothy.Holliefield@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\Europe_AD_Faculty.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Send email to Faculty services Europe
'******************************************************************************
Function sendFacultySvcsEmailEuropeAdjunct()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Mark.Chirchir@umuc.edu"
objEmail.To = "AdjunctApplicants@umuc.edu"
objEmail.Cc = "SOP@umuc.edu;Timothy.Holliefield@umuc.edu"
objEmail.Subject = "Provision Faculty and TA - " & setDate 
objEmail.Textbody = "Attached are the provision results for today."
objEmail.AddAttachment "C:\Scratch\Accounts\Europe_AD_Faculty-Adjunct.txt"


objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item _
    ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Setup autoreply on separated users
'******************************************************************************
Function autoReply()

WScript.Echo "Set Autoreply and Remove Google Groups"

Set objShell = CreateObject("Wscript.Shell")
objShell.Run("powershell -executionpolicy bypass -noexit -file C:\Scratch\Accounts\googleSeparation.ps1 " & usrObj.employeeEmail & " " & randomPass & " " & usrObj.managerEmail & " " & startdate & " " & enddate)


End Function

'******************************************************************************
'Test sebd email to App Ops
'******************************************************************************
Function testEmail()

Dim setDate
Set objEmail = CreateObject("CDO.Message")

setDate = date
objEmail.From = "Mark.Chirchir@umuc.edu"
objEmail.To = "mchirchir@umuc.edu"
objEmail.Subject = "Provision Faculty - " & setDate 
objEmail.Textbody = "Please assign this ticket to Applications Operations for LEO account creations."
objEmail.AddAttachment "C:\Scratch\Accounts\appOps.txt"


objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "bulkmail.umuc.edu" 
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
objEmail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30 
objEmail.Configuration.Fields.Update

objEmail.Send

End Function

'******************************************************************************
'Create Google Mailbox
'******************************************************************************
Function createGoogleMailbox()

Set objShell = CreateObject("Wscript.Shell")
objShell.Run("powershell -executionpolicy bypass -noexit -file C:\Scratch\Accounts\copyGroups.ps1 " & usrObj.employeeEmail & " " & randomPass & " " & usrObj.managerEmail & " " & startdate & " " & enddate)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function copyGoogleGroups()

Set objShell = CreateObject("Wscript.Shell")
objShell.Run("powershell -executionpolicy bypass -noexit -file C:\Scratch\Accounts\copyGroups.ps1 " & usrObj.employeeEmail & " " & randomPass & " " & usrObj.managerEmail & " " & startdate & " " & enddate)

End Function

'******************************************************************************
'END
'******************************************************************************