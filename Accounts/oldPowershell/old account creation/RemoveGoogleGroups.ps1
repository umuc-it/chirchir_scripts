cd C:\Scratch\Accounts
$email = $args[0]
$passwd = $args[1]
$managerEmail = $args[2]
$startDate = $args[3]
$endDate = $args[4]

#Remove all Google groups from account
Write-Host "==============Remove $tar_usr groups=============" -ForegroundColor Cyan
$tem = .\gam.exe info user $email
$tem_chunk = $tem | Select-String -Pattern "<"
if ($tem_chunk.length -gt 0)
{
	foreach ($item in $tem_chunk)
	{	
		$grpaddress=$item.tostring().split("<")[1]
		$grpaddress= $grpaddress.Trim(">")
		Write-Host $grpaddress
		.\gam.exe update group $grpaddress remove member $email 
	}
}

#Setup AutoReply
Write-Host "==============Setup AutoReply=============" -ForegroundColor Green
.\gam.exe update user $email suspended off 
.\gam.exe update user $email org  "Inactive Accounts" 
.\gam.exe update user $email password $passwd
.\gam.exe user $email vacation on subject "Out of Office" message "Thank you for contacting $email at University of Maryland University College. Please redirect all business-related correspondence to $managerEmail. Thank you."
.\gam.exe user $email profile unshared

Write-Host "==============Done=============" -ForegroundColor Cyan
