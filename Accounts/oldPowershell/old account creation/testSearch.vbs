' Search for a User Account in Active Directory


strUserName = "mchirchir"
dtStart = TimeValue(Now())
Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = _
    "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & _
         "(samAccountName=" & strUserName & "));distinguishedName;subtree"
  
Set objRecordSet = objCommand.Execute
 
If objRecordset.RecordCount = 0 Then
    WScript.Echo "sAMAccountName: " & strUserName & " does not exist."
Else
    WScript.Echo objRecordSet.Fields("distinguishedName").Value    
End If
 
objConnection.Close
objRecordSet.Fields("distinguishedName").Value