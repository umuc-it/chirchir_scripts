
# GAM API access modules required: 0,2,3,7,9
# Script assumes Gam executable is located in c:\gam
#
#
cd C:\Scratch\Accounts

Write-Host "Would you like to clone, purge, or list groups for a user?"
Write-Host " " 
Write-Host "1. Clone"
Write-Host "2. Purge"
Write-Host "3. List"
Write-Host " " 
$result = 1
$list = Import-Csv updates.csv
$delegate_list = Import-Csv delegates.csv
switch ($result)
    {
        1 {
        foreach ($entry in $list)
  {
Write-Host "==============CLONE=============" -ForegroundColor Green
$tem_usr = $($entry.template)
Write-Host "Template user: $tem_usr "
$tar_usr = $($entry.target)
Write-Host "Target user: $tar_usr "





Write-Host "==============Adding $tem_usr groups to $tar_usr =============" -ForegroundColor Cyan
$tem= .\gam.exe info user $tem_usr
$tem_chunk= $tem | Select-String "Groups:" -context 0,100
$tem_grps=$tem_chunk.tostring().split(")")
foreach ($line in $tem_grps)
{
$grpaddress=$line.tostring().split("<")[-1].Trim(">")
    if ($grpaddress.contains("(direct member")) 
    {
    $targrp=$grpaddress.replace("> (direct member","") 
    
	if ($targrp -eq "executivecommittee@umuc.edu")
	{
		# Do not add
	}
	elseif ($targrp -eq "presidentscabinet@umuc.edu")
	{
		# Do not add
	}
	elseif ($targrp -eq "aptdirectreports@umuc.edu")
	{
		# Do not add
	}
	elseif ($targrp -eq "cio@umuc.edu")
	{
		# Do not add
	}
	else
	{
		Write-Host "$targrp"
		.\gam.exe update group $targrp add member $tar_usr 
	}
    }
}
Write-Host "==============Adding $tem_usr delegate access to $tar_usr =============" -ForegroundColor Yellow
foreach ($item In $delegate_list)
{
$delegate = $($item.delegate)
$delegator = $($item.delegator)

	If($tem_usr -eq $delegate)
	{
		.\gam.exe user $delegator delegate to $tar_usr
	}
}
}

}
2 {
Write-Host "==============PURGE!!!!!=============" -ForegroundColor Red
$purge_usr = Read-Host "What is the email of the user to be purged from all groups?"
Write-Host "Purge user: $purge_usr "
Write-Host " " 
Write-Host "Are you sure?" -ForegroundColor Red
Write-Host "1. Yes"
Write-Host "2. No"

Write-Host " " 
$a = Read-Host "Select 1 or 2: "
switch ($a) 
{
1{
Write-Host "==============Removing $purge_usr from all groups=============" -ForegroundColor Cyan
$purge= .\gam.exe info user $purge_usr
$purge_chunk= $purge | Select-String "Groups:" -context 0,100
$purge_grps=$purge_chunk.tostring().split(")")
foreach ($line in $purge_grps)
{
$grpaddresspurge=$line.tostring().split("<")[-1].Trim(">")
    if ($grpaddresspurge.contains("(direct member")) 
    {  
    $purgegrp=$grpaddresspurge.replace("> (direct member","") 
    Write-Host "$purgegrp"
    .\gam.exe update group $purgegrp remove owner $purge_usr
    .\gam.exe update group $purgegrp remove member $purge_usr
    }
}
}

2 { 
Write-Host "==============Canceled=============" -ForegroundColor Cyan
}
}
}
3{
Write-Host "==============LIST Groups=============" -ForegroundColor Green
$tem_usr = Read-Host "What is the email of the user?"
Write-Host "Query user: $tem_usr "

Write-Host "==============Writing $tem_usr groups to $tem_usr-groups.csv =============" -ForegroundColor Cyan
$tem= .\gam.exe info user $tem_usr
$tem_chunk= $tem | Select-String "Groups:" -context 0,100
$tem_grps=$tem_chunk.tostring().split(")")
foreach ($line in $tem_grps)
{
$grpaddress=$line.tostring().split("<")[-1].Trim(">")
    if ($grpaddress.contains("(direct member")) 
    {
    $targrp=$grpaddress.replace("> (direct member","") 
    Write-Host "$targrp"
    
    $o = new-object PSObject
$o | add-member NoteProperty Group $targrp
$o | export-csv "$tem_usr-groups.csv" -notypeinformation -Append

    }
}
}
default {
Write-Host "=============The selection could not be determined==============" -ForegroundColor Cyan
break;
}
}


Write-Host "==============Done=============" -ForegroundColor Cyan
