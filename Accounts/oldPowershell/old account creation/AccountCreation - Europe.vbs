'******************************************************************************
'Account Creation
'Created by Mark Chirchir
'Version 4.0
'Added Europe and Asia account creation
'Resolved file access issues
'Update VPN logic
'******************************************************************************
'******************************************************************************
'Class Declarations
'******************************************************************************
Class usrClass
    Private givenName
    Private sn
    Private uid
    Private umucEmplId
    Private umucGUID
    Private umucPrefFirstName
    Private umucCampusEmail
    Private umucEmployeeEmail
    Private umucFacultyEmail
    Private eduPersonPrimaryAffiliation
    Private umucVPNGroup
    Private title
    Private description
    Private department
	Private eduPersonPrimaryOrgUnitDN
	Private umucManagerEmail
    
    Property Get firstName()
        firstName = givenName
    End Property

    Property Get lastName()
        lastName = sn
    End Property

    Property Get userid()
        userid = uid
    End Property

    Property Get emplID()
        emplID = umucEmplId
    End Property

    Property Get ldapGUID()
        ldapGUID = umucGUID
    End Property

    Property Get prefFirstName()
        prefFirstName = umucPrefFirstName
    End Property

    Property Get campusEmail()
        campusEmail = umucCampusEmail
    End Property

    Property Get employeeEmail()
        employeeEmail = umucEmployeeEmail
    End Property
    
    Property Get facultyEmail()
        facultyEmail = umucFacultyEmail
    End Property

    Property Get personPrimaryAffiliation()
        personPrimaryAffiliation = eduPersonPrimaryAffiliation
    End Property

    Property Get vpnGroup()
        vpnGroup = umucVPNGroup
    End Property

    Property Get userTitle()
        userTitle = title
    End Property

    Property Get userdescription()
        userdescription = description
    End Property

    Property Get userdepartment()
        userdepartment = department
    End Property
	
	Property Get personPrimaryOrgUnitDN()
        personPrimaryOrgUnitDN = eduPersonPrimaryOrgUnitDN
    End Property
    
    Property Get managerEmail()
        managerEmail = umucManagerEmail
    End Property
    
    Property Let firstName(NewVal)
        givenName = NewVal
    End Property

    Property Let lastName(NewVal)
        sn = NewVal
    End Property

    Property Let userid(NewVal)
        uid = NewVal
    End Property

    Property Let emplID(NewVal)
        umucEmplId = NewVal
    End Property

    Property Let ldapGUID(NewVal)
        umucGUID = NewVal
    End Property

    Property Let prefFirstName(NewVal)
        umucPrefFirstName = NewVal
    End Property

    Property Let campusEmail(NewVal)
        umucCampusEmail = NewVal
    End Property

    Property Let employeeEmail(NewVal)
        umucEmployeeEmail = NewVal
    End Property
    
    Property Let facultyEmail(NewVal)
        umucFacultyEmail = NewVal
    End Property

    Property Let personPrimaryAffiliation(NewVal)
        eduPersonPrimaryAffiliation = NewVal
    End Property

    Property Let vpnGroup(NewVal)
        umucVPNGroup = NewVal
    End Property
    
    Property Let userTitle(NewVal)
        title = NewVal	
    End Property
    
    Property Let userdescription(NewVal)
        description = NewVal
    End Property

    Property Let userdepartment(NewVal)
        department = NewVal
    End Property
	
	Property Let personPrimaryOrgUnitDN(NewVal)
        eduPersonPrimaryOrgUnitDN = NewVal
    End Property
    
    Property Let managerEmail(NewVal)
        umucManagerEmail = NewVal
    End Property
    
End Class
	
'******************************************************************************
'Constant variables
'******************************************************************************
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3
Const ADS_UF_ACCOUNTDISABLE = 2
Const ADS_UF_PASSWD_NOTREQD = 32
Const ADS_PROPERTY_CLEAR = 1
Const ADS_SECURE_AUTHENTICATION = 1
Const ADS_USE_ENCRYPTION = 2
Const ForReading = 1
Const ForAppending = 8

'******************************************************************************
'Global variables
'******************************************************************************
Dim strPassword
Dim duplicateEmail
Dim sDN
Dim sRoot
Dim sUser
Dim dso
Dim oAuth
Dim oConn
Dim usrObj
Dim usrObjClone
Dim accountExists

'Initialize variables
accountExists = 0
duplicateEmail = 0

Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")


Set outputFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\output.txt",2,true)
outputFile.WriteLine("Username, Password, Email Address")

Set delegateFile = CreateObject("Scripting.FileSystemObject").OpenTextFile("C:\Scratch\Accounts\updates.csv",2,true)
delegateFile.WriteLine("Template,Target")


'******************************************************************************
'User credentials
'******************************************************************************
sUser = "us\mchirchir"
'ldapmasterQAT sDN = "umucGUID=umuc-6649226697673586415,ou=people,dc=umuc,dc=edu"
sDN = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
sRoot = "LDAP://usv-ldpapp-04.us.umuc.edu"
Password("Password:")	

'******************************************************************************
'Get User Input
'******************************************************************************
Input = 0
While Input < 13
	
	WScript.Echo ""
	WScript.Echo "Please choose from the following list"
	WScript.Echo "1. Create User Email in LDAP"
	WScript.Echo "2. Create Faculty Email Account"
	WScript.Echo "3. Delete User Mailbox and Print Separation Template"
	WScript.Echo "4. Print Faculty email"
	WScript.Echo "5. Create User AD and Email Account"
	WScript.Echo "6. Copy Group memberships"
	WScript.Echo "7. Print User ID from Email"
	WScript.Echo "8. Print User ID from Faculty Email"
	WScript.Echo "9. Print UMUC Guid"
	WScript.Echo "10. Remove AD Groups"
	WScript.Echo "11. Test"
	WScript.Echo "12. Test"
	WScript.Echo "13. End Program"
	WScript.Echo ""
	
	WScript.StdIn.Read(0)
	Input = WScript.StdIn.ReadLine


'Call the main logic	
	If Input < 13 then
		main(Input)
	End If
	
	If Input = 13 Then
		delegateFile.Close
		Set delegateFile = Nothing
		outputFile.Close
		Set outputFile = Nothing		
	End If
	
Wend

'******************************************************************************
'Main Function
'******************************************************************************
Function main(userInput)

Set dso = GetObject("LDAP:")
Set oAuth = dso.OpenDSObject(sRoot, sDN, strPassword, &H0200)

Set oConn = CreateObject("ADODB.Connection")
oConn.Provider = "ADSDSOObject"
oConn.Open "Ads Provider", sDN, strPassword

Set ofs = createobject("scripting.filesystemobject")
Set ousernameFile = ofs.opentextfile("c:\Scratch\Accounts\emplid.txt" , 1, false)

'Run the main logic
While not ousernameFile.AtEndOfStream
	useriden = ousernameFile.ReadLine()
	
	'Create new User	
	Set usrObj = New usrClass
	Set usrObjClone = New usrClass
	
	If IsNumeric(useriden) Then
		vsearch = "(umucemplid=" & useriden & ")"
	Else
		vsearch = "(uid=" & useriden & ")"
	End If
			
	Select Case userInput
  		Case 1
  			'Update Staff
	    	ldapExtract(vsearch)
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
				WScript.Echo usrObj.lastName & "|" & usrObj.firstName & "|" & usrObj.userid & "|SSN|" & usrObj.emplID & "|"
			End If
			duplicateEmail = 0
						
  		Case 2
  			'Update Faculty
	    	facultyLdapExtract(vsearch)
			checkFacultyEmail
			If duplicateEmail = 0 Then
				updateFacultyEmail
				WScript.Echo usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail
			End If
			duplicateEmail = 0
  		Case 3
  			'Separate Account
	    	ldapExtract(vsearch)
			disableAccountUS
			printSeparation
			
  		Case 4
  			'Print Faculty email
			facultyLdapExtract(vsearch)
			WScript.Echo usrObj.lastName & "," & usrObj.firstName & "," & usrObj.userid & ",SSN," & usrObj.emplID & "," & usrObj.facultyEmail
			'delegateFile.Writeline(usrObj.facultyEmail)

  		Case 5
  			'Update Staff
  			
  			InputLine = split(useriden,",")
			newEmployee = InputLine(0)
			oldEmployee = InputLine(1)
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
			If IsNumeric(oldEmployee) Then
				oldemp = "(umucemplid=" & oldEmployee & ")"
			Else
				oldemp = "(uid=" & oldEmployee & ")"
			End If
			
	    	ldapExtract(newemp)  	
	    	ldapExtractClone(oldemp)
	    	
			checkEmail
			If duplicateEmail = 0 Then
				updateEmail
			End If

			findAccount
			If accountExists = 0 Then
				If usrObj.personPrimaryOrgUnitDN = "USA" Then
					CreateADAccountUS
					WScript.Sleep 5000
					CopyADGroupsUS
					updateVPN
					
				ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
					CreateADAccountEurope
					WScript.Sleep 5000
					CopyADGroupsEurope
					updateVPN
				
				ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
					CreateADAccountAsia
					WScript.Sleep 5000
					CopyADGroupsAsia
					updateVPN
					
				Else
					WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
				End If
				
			Else
				'do nothing	
			End If
			
			accountExists =0
			duplicateEmail = 0

  		Case 6
  			'Copy Groups
			InputLine = split(useriden,",")
			newEmployee = InputLine(0)
			oldEmployee = InputLine(1)
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
			If IsNumeric(oldEmployee) Then
				oldemp = "(umucemplid=" & oldEmployee & ")"
			Else
				oldemp = "(uid=" & oldEmployee & ")"
			End If
			
	    	ldapExtract(newemp)  	
	    	ldapExtractClone(oldemp)


			If usrObj.personPrimaryOrgUnitDN = "USA" Then
				CopyADGroupsUS
				updateVPN
			
			ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
				CopyADGroupsEurope
				updateVPN
	
			ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
				CopyADGroupsAsia
				updateVPN
					
			Else
				WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
			End If
			
  		Case 7
  			'Print user email
			vsearch = "(umucEmployeeEmail=" & useriden & ")"
			on error resume next
			ldapExtract(vsearch)
			outputFile.Writeline (usrObj.userid & "|" & usrObj.emplID & "|" & usrObj.employeeemail)
			
  		Case 8
  			'Print faculty email
			vsearch = "(umucFacultyEmail=" & useriden & ")"
			on error resume next
			facultyLdapExtract(vsearch)
			outputFile.Writeline (usrObj.userid & "|" & usrObj.emplID & "|" & usrObj.facultyemail)
			
  		Case 9
  			'Update Staff
	    	ldapExtract(vsearch)
			WScript.Echo ("umucGUID=" & usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu")
  		
  		Case 10
  			'Remove Groups
			newEmployee = useriden
			
			If IsNumeric(newEmployee) Then
				newemp = "(umucemplid=" & newEmployee & ")"
			Else
				newemp = "(uid=" & newEmployee & ")"
			End If
			
	    	ldapExtract(newemp)	
	    	
	    	If usrObj.personPrimaryOrgUnitDN = "USA" Then
				removeAllADGroupsUS
			
			ElseIf usrObj.personPrimaryOrgUnitDN = "EURO" Then
				removeAllADGroupsEurope
	
			ElseIf usrObj.personPrimaryOrgUnitDN = "ASIA" Then
				removeAllADGroupsAsia
					
			Else
				WScript.Echo "The user's organization location couldnt be established, check LDAP eduPersonPrimaryOrgUnitDN"
			End If
			
		Case 11
			'do nothing
			
		Case 12
			'do nothing
			
			
	End Select
	
	Set usrobj = Nothing
	Set usrObjClone = Nothing
	
Wend

End Function

'******************************************************************************
'Get User Password
'******************************************************************************
Function Password(myPrompt)
' This function uses Internet Explorer to
' create a dialog and prompt for a password.
'
' Version:             2.11
' Last modified:       2010-09-28
'
' Argument:   [string] prompt text, e.g. "Please enter password:"
' Returns:    [string] the password typed in the dialog screen
    Dim objIE
    ' Create an IE object
    Set objIE = CreateObject( "InternetExplorer.Application" )
    ' specify some of the IE window's settings
    objIE.Navigate "about:blank"
    objIE.Document.Title = "Password " & String( 100, "." )
    objIE.ToolBar        = False
    objIE.Resizable      = False
    objIE.StatusBar      = False
    objIE.Width          = 320
    objIE.Height         = 180
    ' Center the dialog window on the screen
    With objIE.Document.ParentWindow.Screen
        objIE.Left = (.AvailWidth  - objIE.Width ) \ 2
        objIE.Top  = (.Availheight - objIE.Height) \ 2
    End With
    ' Wait till IE is ready
    Do While objIE.Busy
        WScript.Sleep 200
    Loop
    ' Insert the HTML code to prompt for a password
    objIE.Document.Body.InnerHTML = "<div align=""center""><p>" & myPrompt _
                                  & "</p><p><input type=""password"" size=""20"" " _
                                  & "id=""Password""></p><p><input type=" _
                                  & """hidden"" id=""OK"" name=""OK"" value=""0"">" _
                                  & "<input type=""submit"" value="" OK "" " _
                                  & "onclick=""VBScript:OK.Value=1""></p></div>"
    ' Hide the scrollbars
    objIE.Document.Body.Style.overflow = "auto"
    ' Make the window visible
    objIE.Visible = True
    ' Set focus on password input field
    objIE.Document.All.Password.Focus

    ' Wait till the OK button has been clicked
    On Error Resume Next
    Do While objIE.Document.All.OK.Value = 0
        WScript.Sleep 200
        ' Error handling code by Denis St-Pierre
        If Err Then    'user clicked red X (or alt-F4) to close IE window
            IELogin = Array( "", "" )
            objIE.Quit
            Set objIE = Nothing
            Exit Function
        End if
    Loop
    On Error Goto 0

    ' Read the password from the dialog window
    strPassword = objIE.Document.All.Password.Value

    ' Close and release the object
    objIE.Quit
    Set objIE = Nothing
End Function

'******************************************************************************
'Populate user object
'******************************************************************************
Function ldapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-04.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,title,umucPrefFirstName,umucCampusEmail,umucEmployeeEmail,eduPersonPrimaryAffiliation,umucGUID, eduPersonPrimaryOrgUnitDN, umucManagerEmail;subtree")

For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.lastName = var2
Next
For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

If IsNull(oRS.Fields(4).Value) Then
	usrObj.userTitle = "TBD"
Else
For Each Element In oRS.Fields(4).Value
	usrObj.userTitle = Element
Next
End If

If IsNull(oRS.Fields(5).Value) Then
	var1 = usrObj.firstName
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.prefFirstName = var2
Else
	var1 = oRS.Fields(5).Value
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)	
	usrObj.prefFirstName = var2
End If

If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
Else
For Each Element In oRS.Fields(6).Value
	usrObj.campusEmail = Element
Next
End If

If IsNull(oRS.Fields(7).Value) Then
	usrObj.employeeEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@umuc.edu" 
Else
	usrObj.employeeEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

usrObj.ldapGUID = oRS.Fields(9).Value


For Each Element In oRS.Fields(10).Value
	ppoudn = Element
Next
var1 = Split(ppoudn, "=")
var2 = var1(1)
usrObj.personPrimaryOrgUnitDN = var2

If IsNull(oRS.Fields(11).Value) Then
	usrObj.managerEmail = "noreply@umuc.edu"
	WScript.Echo "No Manager Email on LDAP"
Else
	usrObj.managerEmail = oRS.Fields(11).Value
End If

End Function

'******************************************************************************
'Populate faculty user object
'******************************************************************************
Function facultyLdapExtract(vsearch)

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-04.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucGUID,umucPrefFirstName,umucCampusEmail,umucFacultyEmail,eduPersonPrimaryAffiliation,umucVPNGroup,eduPersonPrimaryOrgUnitDN, umucManagerEmail;subtree")

For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.lastName = var2
Next
For Each Element In oRS.Fields(2).Value
	usrObj.userid = Element
Next

usrObj.emplID = oRS.Fields(3).Value

usrObj.ldapGUID = oRS.Fields(4).Value

If IsNull(oRS.Fields(7).Value) Then
	var1 = usrObj.firstName
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObj.prefFirstName = var2
Else
	var1 = oRS.Fields(5).Value
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)	
	usrObj.prefFirstName = var2
End If


If IsNull(oRS.Fields(6).Value) Then
	usrObj.campusEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
Else
For Each Element In oRS.Fields(6).Value
	usrObj.campusEmail = Element
Next
End If

If IsNull(oRS.Fields(7).Value) Then
	usrObj.facultyEmail = usrObj.prefFirstName & "." & usrObj.lastName & "@faculty.umuc.edu" 
	
Else
	usrObj.facultyEmail = oRS.Fields(7).Value
End If

usrObj.personPrimaryAffiliation = oRS.Fields(8).Value

If IsNull(oRS.Fields(9).Value) Then
	usrObj.vpnGroup = "Nothing"
else
	For Each Element In oRS.Fields(9).Value
		usrObj.vpnGroup = Element
	Next
End If

For Each Element In oRS.Fields(10).Value
	'read the edupersonPrimaryOrgUnitDN 
	ppoudn = Element
Next
var1 = Split(ppoudn, "=")
var2 = var1(1)
usrObj.personPrimaryOrgUnitDN = var2

If IsNull(oRS.Fields(11).Value) Then
	usrObj.managerEmail = "noreply@umuc.edu"
	WScript.Echo "No Manager Email on LDAP"
Else
	usrObj.managerEmail = oRS.Fields(11).Value
End If

End Function

'******************************************************************************
'Populate clone object
'******************************************************************************
Function ldapExtractClone(vsearch)

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-04.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID,umucEmployeeEmail,umucVPNGroup;subtree")


For Each Element In oRS.Fields(0).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObjClone.firstName = var2
	
Next
For Each Element In oRS.Fields(1).Value
	var1 = Element
	var2 = ucase(mid(var1,1,1)) & mid(var1,2)
	usrObjClone.lastName = var2
Next

For Each Element In oRS.Fields(2).Value
	usrObjClone.userid = Element
Next

usrObjClone.emplID = oRS.Fields(3).Value

usrObjClone.employeeEmail = oRS.Fields(4).Value

If IsNull(oRS.Fields(5).Value) Then
	usrObjClone.vpnGroup = "Nothing"
else
	For Each Element In oRS.Fields(5).Value
		usrObjClone.vpnGroup = Element
	Next
End If

End Function

'******************************************************************************
'Generate random password
'******************************************************************************
Function RndPassword()

randomize
dim arrPassA(9)
strResult = ""
arrSymbols = Array("!","#","$","%","&","(",")","*","+",",","-",".",":",";","<","=",">","?","[","]","^","_","{","}","~")
 
do while strResult = ""
    strPass = ""
    do while len(strPass) < 6
        intAsc = int(rnd * 57) + 65
        if intAsc < 91 or intAsc > 96 then
            strPass = strPass & chr(intAsc)
        end if
    loop
 
    strPass = strPass & cstr(int(rnd*10)) & arrSymbols(int(rnd*ubound(arrSymbols)))
 
    for i = 0 to 8
        arrPassA(i) = mid(strPass,i+1,1)
    next
 
    strPass = ""
 
    booMore = TRUE
 
    do
        intIdx = int(rnd*9)
        if arrPassA(intIdx) <> "" then
            strPass = strPass & arrPassA(intIdx)
            arrPassA(intIdx) = ""
        else
            booMore = FALSE
            for x = 0 to 9
                if arrPassA(x) <> "" then
                    booMore = TRUE
                    exit for
                end if
            next
        end if
        if not booMore then
            exit do
        end if
    loop
 
    if lcase(strPass) <> strPass and ucase(strPass) <> strPass then
 		RndPassword = strPass
 		strResult = strPass
    end if
Loop

End Function

'******************************************************************************
' Password Upper Case
'******************************************************************************

function pcase(input)
    pcase = ucase(left(input,1)) & right(input,len(input)-1)
    
End Function

'******************************************************************************
' Search for a User Account in Active Directory
'******************************************************************************
Function findAccount

dtStart = TimeValue(Now())
Dim recordCount

Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.CommandText = "<LDAP://dc=us,dc=umuc,dc=edu>;(&(objectCategory=User)" & "(samAccountName=" & usrObj.userID & "));samAccountName;subtree"
Set objRecordSet = objCommand.Execute
	recordCount = objRecordset.RecordCount
	
If recordCount = 0 Then
    'do nothing
Else
    WScript.Echo usrObj.userID & " Already Exists in Active Directory."
    accountExists = 1
End If
 
objConnection.Close

End Function

'******************************************************************************
'Create user account in AD
'******************************************************************************
Function CreateADAccountUS()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@us.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/OU=People,DC=us,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "US"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Create user account in AD
'******************************************************************************
Function CreateADAccountEurope()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@europe.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,dc=europe,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/OU=People,dc=europe,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Europe"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College Europe"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Create user account in AD
'******************************************************************************
Function CreateADAccountAsia()
Dim objRootLDAP, objContainer, objUser, objShell, strContainer
Dim uPN, displayName, mail
Dim tempDesc, tempDept, intUAc, randomPass
'create variables
uPN = usrObj.userID & "@asia.umuc.edu"
displayName = usrObj.prefFirstName & " " & usrObj.lastName


Set openDS = GetObject("LDAP:") 

'Get description and department from the template
Set objUserTemplate = openDS.OpenDSObject("LDAP://CN=" & usrObjClone.userID & ",OU=People,DC=asia,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)
tempDesc = objUserTemplate.Get("description")
tempDept = objUserTemplate.Get("department")

' Build the actual User.
Set objContainer = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/OU=People,DC=asia,DC=umuc,DC=edu",sUser,strPassword,ADS_SECURE_AUTHENTICATION)

Set objUser = objContainer.Create("user", "cn=" & usrObj.userID)
objUser.Put "sAMAccountName", usrObj.userid
objUser.SetInfo

objUser.Put "userPrincipalName", uPN
objUser.Put "givenName", usrObj.firstName
objUser.Put "sn", usrObj.lastName
objUser.Put "displayName", displayName
objUser.Put "scriptPath", "logon.bat"
objUser.Put "physicalDeliveryOfficeName", "Asia"
objUser.Put "description", tempDesc
objUser.Put "employeeID", usrObj.emplID
objUser.Put "mail", usrObj.employeeEmail
objUser.Put "company", "University of Maryland University College"
objUser.Put "title", usrObj.userTitle
objUser.Put "department", tempDept
objUser.Put "telephoneNumber", "TBD"
objUser.Put "ipPhone", "TBD"
objUser.Put "employeeType", usrObj.personPrimaryAffiliation
objUser.Put "streetAddress", "TBD"
objUser.SetInfo

'set password and enable the account
intUAc = 512
randomPass = RndPassword()

objUser.SetPassword randomPass
objUser.AccountDisabled = False
objUser.put "userAccountControl", intUAc And (Not ADS_UF_PASSWD_NOTREQD) And (Not ADS_UF_ACCOUNT_DISABLE)
objUser.Put "pwdLastSet", 0
objUser.SetInfo

WScript.Echo "Account created in Active Directory: " & usrObj.userID
WScript.Echo "Active Directory Password: " & randomPass

outputFile.WriteLine(usrObj.userID & "," & randomPass & "," & usrObj.employeeEmail)
delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)

End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function CopyADGroupsUS()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=us,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://adeprdcns02.us.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function CopyADGroupsEurope()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Copy user groups from clone
'******************************************************************************
Function CopyADGroupsAsia()
On Error Resume Next
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Const ADS_PROPERTY_APPEND = 3


WScript.echo "Copying Group Memberships"
WScript.echo
WScript.echo "Source User: " & usrObjClone.userID
WScript.echo "Destination User: " & usrObj.userID
WScript.echo

Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/cn=" & usrObjClone.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")

arrMemberOf = objUser.GetEx("memberOf")


If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "Source User does not have memberOf attribute set."
Else
    WScript.Echo "Copying Membership in: "
    For Each Group in arrMemberOf
        WScript.Echo Group
				'Set objGroup = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & Group)
				Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
				objGroup.PutEx ADS_PROPERTY_APPEND,	"member", Array("cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
				objGroup.SetInfo
    Next
End If

delegateFile.WriteLine(usrObjClone.employeeEmail & "," & usrObj.employeeEmail)


End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsUS()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://adeprdcns02.us.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=us,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsEurope()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=europe,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function

'******************************************************************************
'Remove user from all groups
'******************************************************************************
Function removeAllADGroupsAsia()
On Error Resume Next

Const ADS_PROPERTY_DELETE = 4
Const E_ADS_PROPERTY_NOT_FOUND  = &h8000500D
Set openDS = GetObject("LDAP:")

Set objUser = GetObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
 
If Err.Number = E_ADS_PROPERTY_NOT_FOUND Then
    WScript.Echo "This account is not a member of any security groups."
    WScript.Quit
End If 
arrMemberOf = objUser.GetEx("memberOf")
 
For Each Group in arrMemberOf
	WScript.Echo Group
    Set objGroup = openDS.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& Group,sUser,strPassword,ADS_SECURE_AUTHENTICATION)
    objGroup.PutEx ADS_PROPERTY_DELETE, "member", Array("cn=" & usrObj.userID & ",ou=people,dc=asia,dc=umuc,dc=edu")
    objGroup.SetInfo
Next

End Function

'******************************************************************************
'Check LDAP for email duplicates
'******************************************************************************
Function checkEmail()

vsearch = "(umucEmployeeEmail=" & usrObj.employeeEmail & ")"

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-04.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Check LDAP for faculty email duplicates
'******************************************************************************
Function checkFacultyEmail()

vsearch = "(umucFacultyEmail=" & usrObj.facultyEmail & ")"

Set oRS = oConn.Execute("<LDAP://usv-ldpapp-04.us.umuc.edu/ou=people,dc=umuc,dc=edu>;" & vSearch & ";givenName,sn,uid,umucEmplID;subtree")

If oRs.RecordCount = 1 Then

	For Each Element In oRS.Fields(0).Value
		givenName = Element
	Next
	For Each Element In oRS.Fields(1).Value
		sn = Element
	Next
	For Each Element In oRS.Fields(2).Value
		uid = Element
	Next
	WScript.Echo "*****************************************"
	WScript.Echo "Duplicate Faculty Email Address"
	WScript.Echo uid
	WScript.Echo "*****************************************"
	duplicateEmail = 1
End If

End Function

'******************************************************************************
'Populate faculty email
'******************************************************************************
Function updateFacultyEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"

Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucFacultyEmail", usrObj.facultyEmail
obj.setinfo

End Function

'******************************************************************************
'Populate staff email
'******************************************************************************
Function updateEmail()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
'usrObj.employeeEmail = usrObj.prefFirstName &"."& usrObj.lastName &"@umuc.edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.put "umucEmployeeEmail", usrObj.employeeEmail
If usrObjClone.vpngroup = "Nothing" Then
	'do nothing
Else
	obj.put "umucVPNGroup", usrObjClone.vpnGroup
End If

obj.setinfo

WScript.Echo "Email address created in LDAP: " & usrObj.employeeEmail
End Function


'******************************************************************************
'Populate vpn group
'******************************************************************************
Function updateVPN()

ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
'usrObj.employeeEmail = usrObj.prefFirstName &"."& usrObj.lastName &"@umuc.edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
If usrObjClone.vpngroup = "Nothing" Then
	'do nothing
Else
	obj.put "umucVPNGroup", usrObjClone.vpnGroup
End If

obj.setinfo

End Function

'******************************************************************************
'Populate vpn group
'******************************************************************************
Function removeVPNGroup()
On Error Resume Next
'LDAP delete vpnGroup


If usrObj.vpngroup = "Nothing" Then
	'do nothing
Else
	ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
	Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
	obj.PutEx ADS_PROPERTY_CLEAR, "umucVPNGroup", 0
	obj.setinfo
	WScript.Echo usrObj.vpngroup
End If


End Function

'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccountUS()
sDomain = "us"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
obj.setinfo


'Ad Disable account
userDN = "CN=" & usrObj.userID & ",OU=People,dc=us,dc=umuc,dc=edu"

Set objDSO = GetObject("LDAP:")
Set objDisable = objDSO.OpenDSObject("LDAP://adeprdcns02.us.umuc.edu/"& userDN,sUser,strPassword, &H0200)

	intUAC = objDisable.Get("userAccountControl") 
  	objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
	objDisable.SetInfo 

sDestOU = "LDAP://adeprdcns02.us.umuc.edu/OU=Inactive,dc=us,dc=umuc,dc=edu"
Set objRootDSE = GetObject("LDAP:")

Set objDestOU = objRootDSE.OpenDSObject(sDestOU, sUser, strPassword, ADS_SECURE_AUTHENTICATION)

objDestOU.MoveHere "LDAP://adeprdcns02.us.umuc.edu/" & userDn, vbNullString

'gam tool


End Function

'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccountEurope()

sDomain = "europe"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
obj.setinfo


'Ad Disable account
userDN = "CN=" & usrObj.userID & ",OU=People,dc=europe,dc=umuc,dc=edu"

Set objDSO = GetObject("LDAP:")
Set objDisable = objDSO.OpenDSObject("LDAP://EUI-OPSDOM-02.europe.umuc.edu/"& userDN,sUser,strPassword, &H0200)

	intUAC = objDisable.Get("userAccountControl") 
  	objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
	objDisable.SetInfo 

sDestOU = "LDAP://EUI-OPSDOM-02.europe.umuc.edu/OU=Inactive,dc=europe,dc=umuc,dc=edu"
Set objRootDSE = GetObject("LDAP:")

Set objDestOU = objRootDSE.OpenDSObject(sDestOU, sUser, strPassword, ADS_SECURE_AUTHENTICATION)

objDestOU.MoveHere "LDAP://EUI-OPSDOM-02.europe.umuc.edu/" & userDN, vbNullString

End Function


'******************************************************************************
'Disable account in AD and delete email from LDAP
'******************************************************************************
Function disableAccountAsia()

sDomain = "asia"

'On Error Resume Next

'LDAP delete email address
ldapObj = "umucGUID="& usrObj.ldapGUID & ",ou=people,dc=umuc,dc=edu"
Set obj = dso.OpenDSObject("LDAP://ldapmaster.umuc.edu/"& ldapobj,sDN,strPassword, 0)
obj.PutEx ADS_PROPERTY_CLEAR, "umucEmployeeEmail", 0
obj.setinfo


'Ad Disable account
userDN = "CN=" & usrObj.userID & ",OU=People,dc=asia,dc=umuc,dc=edu"

Set objDSO = GetObject("LDAP:")
Set objDisable = objDSO.OpenDSObject("LDAP://EUI-OPSDOM-05.asia.umuc.edu/"& userDN,sUser,strPassword, &H0200)

	intUAC = objDisable.Get("userAccountControl") 
  	objDisable.Put "userAccountControl", intUAC Or ADS_UF_ACCOUNTDISABLE
	objDisable.SetInfo 

sDestOU = "LDAP://EUI-OPSDOM-05.asia.umuc.edu/OU=Inactive,dc=asia,dc=umuc,dc=edu"
Set objRootDSE = GetObject("LDAP:")

Set objDestOU = objRootDSE.OpenDSObject(sDestOU, sUser, strPassword, ADS_SECURE_AUTHENTICATION)

objDestOU.MoveHere "LDAP://EUI-OPSDOM-05.asia.umuc.edu/" & userDn, vbNullString

End Function

'******************************************************************************
'Print template for HR submission
'******************************************************************************
Function printSeparation()

WScript.Echo "Name: " & usrObj.firstName & " " & usrObj.lastName
WScript.Echo "Login ID: " & usrObj.userID
WScript.Echo "EmplID: " & usrObj.emplID
WScript.Echo

End Function

'******************************************************************************
'END
'******************************************************************************