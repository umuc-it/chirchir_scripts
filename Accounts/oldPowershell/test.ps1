#Mike Burr
#Script Connects to and Searches OpenLDAP directory

#Load the assemblies
[System.Reflection.Assembly]::LoadWithPartialName("System.DirectoryServices.Protocols")
[System.Reflection.Assembly]::LoadWithPartialName("System.Net")


#Connects to myopenldap.mikesblog.lan using SSL on a non-standard port
$c = New-Object System.DirectoryServices.Protocols.LdapConnection "usv-ldpapp-02.us.umuc.edu:389"
          
#Set session options
$c.SessionOptions.SecureSocketLayer = $true;
           
# Pick Authentication type:
# Anonymous, Basic, Digest, DPA (Distributed Password Authentication),
# External, Kerberos, Msn, Negotiate, Ntlm, Sicily
$c.AuthType = [System.DirectoryServices.Protocols.AuthType]::Basic
           
# Gets username and password.
$user = "umucGUID=umuc-6079109008302321838,ou=people,dc=umuc,dc=edu"
$pass = Read-Host -AsSecureString "Password"

$credentials = new-object "System.Net.NetworkCredential" -ArgumentList $user,$pass

# Bind with the network credentials. Depending on the type of server,
# the username will take different forms. Authentication type is controlled
# above with the AuthType
$c.Bind($credentials);

$basedn = "ou=People,dc=umuc,dc=edu"
$filter = "(uid=mchirchir)"
$scope = [System.DirectoryServices.Protocols.SearchScope]::Subtree
$attrlist = ,"*"

$r = New-Object System.DirectoryServices.Protocols.SearchRequest -ArgumentList `
                $basedn,$filter,$scope,$attrlist

#$re is a System.DirectoryServices.Protocols.SearchResponse
$re = $c.SendRequest($r);

#How many results do we have?
write-host $re.Entries.Count

foreach ($i in $re.Entries)
{
   #Do something with each entry here, such as read attributes
} 