Option Explicit

Dim objRootDSE, strDNSDomain, adoCommand, adoConnection
Dim strBase, strFilter, strAttributes, strQuery, adoRecordset
Dim strDN, strUser, strPassword, objNS, strServer, strUserName, userDN

Const ADS_SECURE_AUTHENTICATION = &H1
Const ADS_SERVER_BIND = &H200

' Specify a server (Domain Controller).
strServer = "eui-opsdom-06.asia.umuc.edu"
strUserName = "acaponcello"

' Specify or prompt for credentials.
strUser = "us\mchirchir"
strPassword = "1@#manymore"

' Determine DNS domain name. Use server binding and alternate
' credentials. The value of strDNSDomain can also be hard coded.
Set objNS = GetObject("LDAP:")
Set objRootDSE = objNS.OpenDSObject("LDAP://" & strServer & "/RootDSE", _
     strUser, strPassword, _
     ADS_SERVER_BIND Or ADS_SECURE_AUTHENTICATION)
strDNSDomain = "OU=People,DC=asia,DC=umuc,DC=edu"

' Use ADO to search Active Directory.
' Use alternate credentials.
Set adoCommand = CreateObject("ADODB.Command")
Set adoConnection = CreateObject("ADODB.Connection")
adoConnection.Provider = "ADsDSOObject"
adoConnection.Properties("User ID") = strUser
adoConnection.Properties("Password") = strPassword
adoConnection.Properties("Encrypt Password") = True
adoConnection.Properties("ADSI Flag") = ADS_SERVER_BIND _
     Or ADS_SECURE_AUTHENTICATION
adoConnection.Open "Active Directory Provider"
Set adoCommand.ActiveConnection = adoConnection

' Search entire domain. Use server binding.
strBase = "<LDAP://" & strServer & "/" & strDNSDomain & ">"
WScript.Echo strBase
' Search for all users.
strFilter = "(&(objectCategory=user)(samAccountName=" & strUserName & "))"

' Comma delimited list of attribute values to retrieve.
strAttributes = "distinguishedName"

' Construct the LDAP query.
strQuery = strBase & ";" & strFilter & ";" & strAttributes & ";subtree"

' Run the query.
adoCommand.CommandText = strQuery
'adoCommand.Properties("Page Size") = 10000
'adoCommand.Properties("Timeout") = 30
'adoCommand.Properties("Cache Results") = False
Set adoRecordset = adoCommand.Execute


WScript.Echo adoRecordset.RecordCount
userDN = adoRecordset.Fields("distinguishedName").Value
WScript.Echo userDN
' Clean up.
adoRecordset.Close
adoConnection.Close