cd C:\Scratch\Accounts
$email = $args[0]
$passwd = $args[1]
$managerEmail = $args[2]
$startDate = $args[3]
$endDate = $args[4]
$file = $args[5]

Write-host $email $managerEmail $passwd
#Remove all Google groups from account
Write-Host "============== Remove " $email " groups =============" -ForegroundColor Cyan
$tem = .\gam.exe info user $email
$tem_chunk = $tem | Select-String -Pattern "<"
if ($tem_chunk.length -gt 0)
{
	foreach ($item in $tem_chunk)
	{	
		$grpaddress=$item.tostring().split("<")[1]
		$grpaddress= $grpaddress.Trim(">")
		Write-Host $grpaddress
		.\gam.exe update group $grpaddress remove member $email
	}
}

Write-Host "============== Remove " $email " delegates =============" -ForegroundColor Cyan

$delegateFile = Get-Content $file
foreach ($line in $delegateFile) {
  $User, $delegateName, $delegateAddress, $delegationStatus = $line -split ','
  if ($delegateAddress -eq $email)
		{
			.\gam.exe user  $User delete delegate $email

}
}


#Setup AutoReply
Write-Host "============== Setup " $email " AutoReply =============" -ForegroundColor Cyan
.\gam.exe update user $email suspended off 
.\gam.exe update user $email org  "Inactive Accounts" 
.\gam.exe update user $email password $passwd
.\gam.exe user $email vacation on subject "Out of Office" message "Thank you for contacting $email at University of Maryland University College. Please redirect all business-related correspondence to $managerEmail. Thank you." startDate $startDate endDate $endDate
.\gam.exe user $email profile unshared
.\gam.exe user $email deprovision

Write-Host "============== Done =============" -ForegroundColor Green
