'Update the file "deleteFolders" with the new folder that you need
'files greater than a month to be deleted
'you can also change the line with Modified.Delete with the number
'of days you would like to store the data

Dim Fso
Dim Directory
Dim Modified
Dim Files
Dim foderName

Set Fso = CreateObject("Scripting.FileSystemObject")
Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set inPutFile = FileSystem.OpenTextFile("C:\Scratch\deleteLogs\deleteFolders.txt",ForReading, False)

delOneMonth
Set FileSystem = Nothing
Set Fso = Nothing

Sub delOneMonth()
While Not inPutFile.AtEndOfStream
	folderName = inPutFile.ReadLine()
	Set Directory = Fso.GetFolder(folderName)
	Set Files = Directory.Files
	For Each Modified in Files
		If DateDiff("D", Modified.DateLastModified, Now) > 30 Then Modified.Delete
	Next
Wend

End Sub