GroupName
"CN=em_goldmine_admin,OU=Security Groups,DC=us,DC=umuc,DC=edu"
ga_af_Budget Office
ga_ap_Arts Bi-Annual
ga_ap_Arts Program
ga_ap_Friends of Arts
ga_as_Communication Studies
ga_asia_portal-prd
ga_cite_Computing Dept
ga_comm_UMUC Marketing Outreach Team
ga_comm_UMUC Social Network
ga_comm_UMUC Webmaster
ga_comm_Web Comm
ga_dod_Military Help
ga_dod_Travis Testing Center
ga_ed_Broadcast IT Asia Permission
ga_ed_Europe Helpdesk Group
ga_em_Enrollment Management Training Sessions
ga_em_GM Leads
ga_em_GoldMineAdmin-OutlookAccess
ga_em_Graduate Advising Office Fax
ga_em_Largo Student Services access via Outlook
ga_em_Student Admissions
ga_em_Student Admissions Fax
ga_em_Students Relocation
ga_em_UMUC Veterans Advising Team
ga_em_Undergrad Advising Fax
ga_fa_Adjunct References
ga_fa_Business Enablement Team
ga_fa_Documents
ga_fa_Emailed Exams
ga_fa_Faculty Parking
ga_fa_Faculty Recruit
ga_fa_Faculty References
ga_fa_Faculty Services Parking
ga_fa_Faculty Services Tier Two
ga_fa_Makeup Exams
ga_fa_Proctors
ga_fa_Registration Receipts
ga_fa_SA Registrar 360
ga_fa_Text Books
ga_fa_Textbooks Affordability
ga_fin_Finance & Accounting Discussion Forum
ga_fin_Finance Tuition Remission Forms
ga_fin_UMUC Travel
ga_fm_Shared Facilites Management - Editor
ga_fm_Shared Facilites Management - Read
ga_ga MBA Department
ga_grad_Center for Security Studies
ga_grad_Chair MAF
ga_grad_Education Department
ga_grad_Grad NOL faculty
ga_grad_Graduate Dean
ga_grad_Graduate Marketing & Fulfillment
ga_grad_Graduate School Student Relations
ga_grad_Graduate Student Success
ga_grad_Information Assurance Scholarship Program - Grad
ga_grad_Information Assurance Scholarship Program - Undergrad
ga_grad_UMUC China-US Forum 2010
ga_gsmt_411register
ga_gsmt_DM Program
ga_gsmt_Doctoral Management Community College
ga_gsmt_Teacher Education Symposium
ga_gsmt_UMUC 411
ga_gsmt_UMUC101_UMUC123
ga_gsmt_umuc111_111register
ga_gsmt_Virtual Asia
ga_gsmt_Virtual Europe
ga_hr_Adjunct Benefits
ga_hr_Benefits
ga_hr_Data Management
ga_hr_Employment
ga_hr_Furlough Questions
ga_hr_HR in Adelphi
ga_hr_HR Information Systems
ga_hr_HR Jobs
ga_hr_HR Verifications
ga_hr_HRIS Reports
ga_hr_Job Postings
ga_hr_Overseas Retirement
ga_hr_Position Management
ga_hr_Special Recruitment
ga_hr_UMUC Contingent II Renewals
ga_hr_UMUC Unemployment Claims
ga_ia_Alumni Relations
ga_ia_Edwards Course Evaluations
ga_ia_Sponsorship Events
ga_ils_Career Center
ga_ils_Digital Repository
ga_ils_Tutor Company Members
ga_ils_UMUC Document Management
ga_ils_UMUC Electronic Reserves
ga_ils_UMUC Ingenta Services
ga_ils_UMUC Library Chat Service
ga_ils_UMUC Library Circulation Services
ga_ils_UMUC Library Electronic Reserves-Access-Send As
ga_ils_UMUC Library Instruction Service
ga_it_A Shared Folder
ga_it_Event Laptops
ga_it_Guardium Audit
ga_it_Help Desk - TAD Support
ga_it_HelpDesk-Editor
ga_it_HelpDesk-PatchPilotFolder
ga_it_HelpDesk-Read
ga_it_PeopleSoft Helpdesk Temp
ga_it_PS Security Administrator
ga_it_TAD Center
ga_it_Verisign Certificate Management
ga_mo_Military Outreach--Orkand
ga_mo_SOCGAE
ga_nli_National Leadership Institute
ga_nli_NFOTS Group
ga_nli_NLI Open Enrollment
ga_odell_Center for Intellectual Property Membership
ga_odell_Cip Repository
ga_odell_Cip Socialmedia
ga_odell_UMUC Copyright
ga_oem_Achieve
ga_oem_Enrollment Management Team Fax
ga_oem_Office of Enrollment Managemet
ga_oem_UMUC Connect
ga_ops_UMUC Gear
ga_pres_Booz Allen
ga_pres_China-US 2011
ga_pres_Commencement 2011
ga_pres_Disability Services
ga_pres_Diversity Initiatives
ga_pres_Gala 2011
ga_pres_Geico
ga_pres_Leak RSVP
ga_pres_Office of Communications
ga_pres_President's Cabinet Permission
ga_pres_UMUC Commencement Tickets 2010
ga_pres_UMUC PRR
ga_pres_UMUC Tigers Softball Team
ga_pres_UMUC Virtual Dragon Boat Team
ga_pres_Web Comm Forms
ga_pro_Learning Assessment
ga_pro_Provost Office
ga_sa_Academic Audits
ga_sa_Divisional Transfer
ga_sa_DMS Functional Support
ga_sa_Graduate Tentative Evaluations
ga_sa_SA Administration
ga_sa_SA Appeals Undergraduate
ga_sa_SA Bio Demo Updates
ga_sa_SA Catalog Updates
ga_sa_SA Code of Conduct
ga_sa_SA DASB
ga_sa_SA DAT Graduate
ga_sa_SA DAT Undergraduate
ga_sa_SA DAT Undergraduate Owner
ga_sa_SA DCC
ga_sa_SA Duplicate IDs
ga_sa_SA File Management
ga_sa_SA Graduate Appeals
ga_sa_SA Graduation Services
ga_sa_SA Historical Course
ga_sa_SA ITOA QA
ga_sa_SA Military Appeals
ga_sa_SA Office Automation
ga_sa_SA Outgoing Transcripts
ga_sa_SA Production Support
ga_sa_SA Program Plan Stack
ga_sa_SA Reconstruction
ga_sa_SA Registrar Office
ga_sa_SA Reinstatements
ga_sa_SA Review DAT
ga_sa_SA Rules
ga_sa_SA Scheduling
ga_sa_SA Security
ga_sa_SA SOC
ga_sa_SA Student Complaints
ga_sa_SA Student Reinstatements
ga_sa_SA Tentative Evaluations Graduate
ga_sa_SA Tentative Evaluations Undergraduate
ga_sa_SA Undergraduate Student Academic Standing
ga_sa_SA Verifications
ga_sa_SA Veterans Affairs
ga_sa_UMUC Degree progress report
ga_sa_UMUC Degree report read only
ga_sa_UMUC Residency
ga_sa_UMUC Third Party Operations
ga_sa_Veterans Affairs
ga_sa_Web Transcripts
ga_sfs_Bursar Fax
ga_sfs_Bursars Office
ga_sfs_Financial Aid Fax
ga_sfs_Financial Aid Team
ga_sfs_FWS Finaid
ga_sfs_UMUC Collections
ga_sfs_UMUC Merchandise
ga_sus_Better Opportunities
ga_sus_Business and Professional Programs
ga_sus_Comm SUS Unit
ga_sus_Computer Based Testing
ga_sus_Coop Inquiries
ga_sus_Copy Requests
ga_sus_Course Challenge
ga_sus_DE Exams
ga_sus_Dean UnderGrad
ga_sus_Faculty Appointments
ga_sus_International Programs
ga_sus_Military Advising E fax
ga_sus_Prior Learning
ga_sus_Prior Learning Applications
ga_sus_Prior Learning -Forwards to group
ga_sus_SEGUE
ga_sus_Student Success
ga_sus_SUS Test Bank
ga_sus_Testing Center
ga_sus_UGP Market
ga_sus_UMUC Transfer Credit
ga_sus_Undergraduate Financial Document
ga_sus_Writing Center
ga_ue_UMUC Arts
ga_ue_UMUC Events
