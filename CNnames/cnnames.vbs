
    ' Function:     SearchDistinguishedName
    ' Description:  Searches the DistinguishedName for a given SamAccountName
    ' Parameters:   ByVal vSAN - The SamAccountName to search
    ' Returns:      The DistinguishedName Name
    Dim oRootDSE, oConnection, oCommand, oRecordSet



'Initialize global variables
Set FileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set InPutFile = FileSystem.OpenTextFile("C:\Scratch\CNnames\userList.txt",ForReading, False)


Dim objFSO 'As FileSystemObject
Set objFSO = CreateObject("Scripting.FileSystemObject")
Dim strDirectory 'As String
strDirectory = "C:\Scratch\CNnames\"

Dim objDirectory 'As Object
If objFSO.FolderExists(strDirectory) Then
Set objDirectory = objFSO.GetFolder(strDirectory)
Else
Set objDirectory = objFSO.CreateFolder(strDirectory)
End If

Dim strFile 'As String
strFile = "outputFile.txt"

Dim objTextFile 'As Object
Dim blnOverwrite 'As Boolean
blnOverwrite = True
Set objTextFile = objFSO.CreateTextFile(strDirectory & "\" & strFile, blnOverwrite)


'Import Users into Container
ImportUsers

'Clean up
InPutFile.Close
Set FileSystem = Nothing
Set oContainer = Nothing

WScript.Echo "Finished"
WScript.Quit(0)

Sub ImportUsers
 while not InPutFile.atEndOfStream
	VSAN = InPutFile.ReadLine()
    Set oRootDSE = GetObject("LDAP://rootDSE")
    Set oConnection = CreateObject("ADODB.Connection")
    oConnection.Open "Provider=ADsDSOObject;"
    Set oCommand = CreateObject("ADODB.Command")
    oCommand.ActiveConnection = oConnection
    oCommand.CommandText = "<LDAP://" & oRootDSE.get("defaultNamingContext") & _
        ">;(&(objectCategory=User)(samAccountName=" & vSAN & "));distinguishedName;subtree"
    Set oRecordSet = oCommand.Execute
    On Error Resume Next
    SearchDistinguishedName = oRecordSet.Fields("DistinguishedName")
    On Error GoTo 0
    oConnection.Close
    Set oRecordSet = Nothing
    Set oCommand = Nothing
    Set oConnection = Nothing
    Set oRootDSE = Nothing

objTextFile.WriteLine(SearchDistinguishedName)
wend

End Sub