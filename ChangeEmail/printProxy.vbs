'This script will take input from a comma delimited file (specified in the InputFile variable) in the form:
' firstName,lastname,displayName,Email,userName
'and updates the email address, firstname, lastname and displayname

'Global variables
Const ADS_PROPERTY_UPDATE = 2

Dim DomainArray (2)
Dim inputFile
Dim fileSystem
Dim getUser
Dim objectList
Dim email
Dim i
Dim csv

'Initialize global variables
Set fileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set inputFile = FileSystem.OpenTextFile("C:\Scratch\ChangeEmail\userList.txt",ForReading, False)
Set outputFile = FileSystem.CreateTextFile("C:\Scratch\ChangeEmail\proxyadd.csv",True)

While Not inputFile.AtEndOfStream
	userName= inputFile.ReadLine()
	wscript.echo userName		
	Set objUser = GetObject ("LDAP://adeprdcns02.us.umuc.edu/CN="& userName &",OU=People,DC=us,DC=umuc,DC=edu")
    nProxyAddresses = 0
    On Error Resume Next
	vProxyAddresses = objUser.ProxyAddresses
  	nProxyAddresses = UBound(vProxyAddresses)
	csv = userName    
	i = 0
	If nProxyAddresses = 0 Then
			'Do Nothing
	Else
		Do While i <= nProxyAddresses
   			email = vProxyAddresses(i)
   			email = Mid (email,6)
  			csv = csv & "," & email
	
			i = i + 1
   		Loop
   	End If
   	outputFile.writeLine csv

	
	
Wend

WScript.Echo "Finished"
