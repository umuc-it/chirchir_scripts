'This script will take input from a comma delimited file (specified in the InputFile variable) in the form:
' firstName,lastname,displayName,Email,userName
'and updates the email address, firstname, lastname and displayname

'Global variables
Const ADS_PROPERTY_UPDATE = 2

Dim DomainArray (2)
Dim inputFile
Dim fileSystem
Dim getUser
Dim objectList
Dim email
Dim i
Dim csv

'Initialize global variables
Set fileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set inputFile = FileSystem.OpenTextFile("C:\Scratch\ChangeEmail\userList.txt",ForReading, False)
Set outputFile = FileSystem.CreateTextFile("C:\Scratch\ChangeEmail\printStaff.csv",True)

While Not inputFile.AtEndOfStream
	userName = inputFile.ReadLine()
				
	Set objUser = GetObject ("LDAP://adeprdcns02.us.umuc.edu/CN="& userName &",OU=Generic Accounts,OU=Exchange,DC=us,DC=umuc,DC=edu")
	csv = ""
	vProxyAddresses = objUser.ProxyAddresses
   	nProxyAddresses = UBound(vProxyAddresses)
	csv = userName
	csv = csv &","& objUser.EmailAddress 
	i=0
	Do While i <= nProxyAddresses
   		email = vProxyAddresses(i)
   		
   		If Right (email,9) = "@umuc.edu" Then
			csv = csv &","& Mid (email,6)

   		End If
		i = i + 1
   	Loop
	wscript.echo csv
	outputFile.writeLine csv

Wend

WScript.Echo "Finished"
