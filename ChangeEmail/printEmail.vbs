'This script will take input from a comma delimited file (specified in the InputFile variable) in the form:
' firstName,lastname,displayName,Email,userName
'and updates the email address, firstname, lastname and displayname

'Global variables
Const ADS_PROPERTY_UPDATE = 2

Dim DomainArray (2)
Dim inputFile
Dim fileSystem
Dim getUser
Dim objectList
Dim email
Dim i
Dim csv

'Initialize global variables
Set fileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set inputFile = FileSystem.OpenTextFile("C:\Scratch\ChangeEmail\userList.txt",ForReading, False)
'Set outputFile = FileSystem.CreateTextFile("C:\Scratch\ChangeEmail\priEmail.csv",True)

While Not inputFile.AtEndOfStream
	
	
'opt1	
	'userName= inputFile.ReadLine()
	'Set objUser = GetObject ("LDAP://adeprdcns02.us.umuc.edu/CN="& userName &",OU=People,DC=us,DC=umuc,DC=edu")
	'WScript.echo objUser.samaccountname & "," & objUser.displayname & "," & objUser.emailaddress
	
	
'opt2
	userName= split(inputFile.ReadLine(),",")
	var1 = userName(0)			
	var2 = userName(1)
	Set srcUser = GetObject ("LDAP://adeprdcns02.us.umuc.edu/CN="& var1 &",OU=People,DC=us,DC=umuc,DC=edu")
	Set objUser = GetObject ("LDAP://adeprdcns02.us.umuc.edu/CN="& var2 &",OU=People,DC=us,DC=umuc,DC=edu")
	WScript.echo srcUser.emailaddress &","& objUser.emailaddress
Wend

WScript.Echo "Finished"
