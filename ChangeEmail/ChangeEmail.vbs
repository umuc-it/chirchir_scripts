'This script will take input from a comma delimited file (specified in the InputFile variable) in the form:
' firstName,lastname,displayName,Email,userName
'and updates the email address, firstname, lastname and displayname

'Global variables
Const ADS_PROPERTY_UPDATE = 2

Dim DomainArray (2)
Dim inputFile
Dim fileSystem
Dim getUser
Dim objectList
Dim email
Dim i

'Initialize global variables
Set fileSystem = WScript.CreateObject("Scripting.FileSystemObject")
ForReading = 1
Set inputFile = FileSystem.OpenTextFile("C:\Scratch\ChangeEmail\userList.txt",ForReading, False)


While Not inputFile.AtEndOfStream
	getUser = inputFile.ReadLine()
	userArray = Split(getUser,",")
	userName = userArray(0)
	facultyName = userArray(1)
				
	Set objUser = GetObject ("LDAP://adeprdcns02.us.umuc.edu/CN="& userName &",OU=People,DC=us,DC=umuc,DC=edu")
	Set objFaculty = GetObject ("LDAP://adeprdcns02.us.umuc.edu/CN="& facultyName &",OU=People,DC=us,DC=umuc,DC=edu")

	vProxyAddresses = objUser.ProxyAddresses
    nProxyAddresses = UBound(vProxyAddresses)
    
	fProxyAddresses = objFaculty.ProxyAddresses
	qProxyAddresses = UBound(fProxyAddresses)
	i = 0
	Do While i <= nProxyAddresses
   		email = vProxyAddresses(i)
   	
   		If Right (email,17) = "@faculty.umuc.edu" Then
			qProxyAddresses = qProxyAddresses + 1    		
    		ReDim Preserve fProxyAddresses(qProxyAddresses)
    		fProxyAddresses(qProxyAddresses)= email
    		
			vProxyAddresses(i) = vProxyAddresses(nProxyAddresses)
			nProxyAddresses = nProxyAddresses - 1
			ReDim Preserve vProxyAddresses(nProxyAddresses)
   		End If
		i = i + 1
   	Loop
	wScript.Echo userName
	objUser.ProxyAddresses = vProxyAddresses
	objUser.PutEx ADS_PROPERTY_UPDATE, "msExchPoliciesExcluded", Array("{26491CFC-9E50-4857-861B-0CB8DF22B5D7}")
	objUser.SetInfo
	WScript.Echo userName
	
	objFaculty.ProxyAddresses = fProxyAddresses
	objFaculty.PutEx ADS_PROPERTY_UPDATE, "msExchPoliciesExcluded", Array("{26491CFC-9E50-4857-861B-0CB8DF22B5D7}")
	objFaculty.SetInfo
	WScript.Echo facultyName
	
Wend

WScript.Echo "Finished"
