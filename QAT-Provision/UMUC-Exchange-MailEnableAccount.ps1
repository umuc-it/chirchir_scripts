# Variable Declarations
$ADDomainController = "adedcns01.us.umuc.edu"
$ADPrivilegedUserAccount = "US\mchirchir_eu"
$LogFile = "C:\Scratch\Provision\UMUC-Exchange-MailEnableAccount.log"
$SourceFile = "C:\Scratch\Provision\AccountSourceFile.txt"

Set-Content $LogFile ("INFO - Start Date and Time: " + (Get-Date))

# Check for Quest
If ((get-pssnapin -Name Quest.ActiveRoles.ADManagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

# Establish Active Directory Connection
$ADCredentials = Get-Credential $ADPrivilegedUserAccount
$ADConnection = Connect-QADService -Service $ADDomainController -Credential $ADCredentials

# Read list of accounts from input file
$SourceList = Get-Content $SourceFile

ForEach ($SourceLine in $SourceList) {
	If (-not $SourceLine.Startswith("Lastname")) {
		$SourceElements = $SourceLine.Split('`|')
		$TargetAccountUserID = $SourceElements[2].ToLower()
		If ($SourceElements[8].ToLower() -eq "fac") {
			Add-Content $LogFile "INFO - Processing Faculty Account: $TargetAccountUserID"
			#Search for Account by SamAccountName and Proceed if Found
			Add-Content $LogFile "INFO - Checking for User: $TargetAccountUserID"
			$TargetAccount = Get-QADUser -IncludedProperties legacyExchangeDN -SamAccountName $TargetAccountUserID
			If ($TargetAccount -ne $null) {
				Add-Content $LogFile ("INFO - Account Found: " + $TargetAccount.DN)
				# Check to make sure account doesn't have mail attribute defined already.
				# Likely indicates account is Exchange enabled.
				If ($TargetAccount.mail -eq $null) {
					# Check for legacyExchangeDN atribute with value of ADCDisabledMail
					# If found, we will clear the value to prevent problems
					If ($TargetAccount.legacyExchangeDN -eq "ADCDisabledMail") {
						Add-Content $LogFile "WARNING - legacyExchangeDN set to ADCDisabledMail"
						Add-Content $LogFile "WARNING - Account may have previously been Exchange enabled"
						Add-Content $LogFile "WARNING - Will Clear legacyExchangeDN attribute"
						Set-QADUser $TargetAccount -ObjectAttributes @{legacyExchangeDN=''}
					}
					
					# Setting mailNickname and msExchHomeServerName is enough for the RUS to populate
					# the remaining values needed for Exchange to work.  We also set homeMDB to
					# specify the mail store.
					
					# We need to specify which storage group and mail store are used and Exchange server
					# We extract the first character of the SamAccountName and make decision based on it
					Switch -regex ($TargetAccount.SamAccountName.ToLower().Substring(0,1)) {
						"[a-g]" {
							Add-Content $LogFile "INFO - Setting Home Server: /o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX01"
							Add-Content $LogFile "INFO - Matched User to Store: CN=FacultyAG,CN=SG2-Users A-G Mailstore,CN=InformationStore,CN=ADEEX01,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu"
							#All three Exchange attributs are set in a single command to try and address timing issues
							Set-QADUser $TargetAccount -ObjectAttributes @{
								mailNickname=($TargetAccount.SamAccountName);
								msExchHomeServerName='/o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX01';
								homeMDB='CN=FacultyAG,CN=SG2-Users A-G Mailstore,CN=InformationStore,CN=ADEEX01,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu'
								}
						}
						"[h-m]" {
							Add-Content $LogFile "INFO - Setting Home Server: /o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02"
							Add-Content $LogFile "INFO - Matched User to Store: CN=FacultyHM,CN=SG1-Users H-M Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu"
							#All three Exchange attributs are set in a single command to try and address timing issues
							Set-QADUser $TargetAccount -ObjectAttributes @{
								mailNickname=($TargetAccount.SamAccountName);
								msExchHomeServerName='/o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02';
								homeMDB='CN=FacultyHM,CN=SG1-Users H-M Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu'
								}
						}
						"[n-z]" {
							Add-Content $LogFile "INFO - Setting Home Server: /o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02"
							Add-Content $LogFile "INFO - Matched User to Store: CN=FacultyNZ,CN=SG2-Users N-Z Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu"
							#All three Exchange attributs are set in a single command to try and address timing issues
							Set-QADUser $TargetAccount -ObjectAttributes @{
								mailNickname=($TargetAccount.SamAccountName);
								msExchHomeServerName='/o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02';
								homeMDB='CN=FacultyNZ,CN=SG2-Users N-Z Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu'
								}
						}
						default {Add-Content $LogFile "ERROR - Unable to Match User to Exchange Store."}
					}
				} Else {
					Add-Content $LogFile ("WARNING - Found Existing Value in Mail Attribute: " + $TargetAccount.mail)
					Add-Content $LogFile "WARNING - Will not Attempt to Exchange Enable Account"
				}
			} Else {
				Add-Content $LogFile "ERROR - Account Not Found: $TargetAccountUserID"
			}
		} ElseIf ($SourceElements[8].ToLower() -eq "ta") {
			Add-Content $LogFile "INFO - Processing TA Account: $TargetAccountUserID"
			#Search for Account by SamAccountName and Proceed if Found
			Add-Content $LogFile "INFO - Checking for User: $TargetAccountUserID"
			$TargetAccount = Get-QADUser -IncludedProperties legacyExchangeDN -SamAccountName $TargetAccountUserID
			If ($TargetAccount -ne $null) {
				Add-Content $LogFile ("INFO - Account Found: " + $TargetAccount.DN)
				# Check to make sure account doesn't have mail attribute defined already.
				# Likely indicates account is Exchange enabled.
				If ($TargetAccount.mail -eq $null) {
					# Check for legacyExchangeDN atribute with value of ADCDisabledMail
					# If found, we will clear the value to prevent problems
					If ($TargetAccount.legacyExchangeDN -eq "ADCDisabledMail") {
						Add-Content $LogFile "WARNING - legacyExchangeDN set to ADCDisabledMail"
						Add-Content $LogFile "WARNING - Account may have previously been Exchange enabled"
						Add-Content $LogFile "WARNING - Will Clear legacyExchangeDN attribute"
						Set-QADUser $TargetAccount -ObjectAttributes @{legacyExchangeDN=''}
					}
					
					# Setting mailNickname and msExchHomeServerName is enough for the RUS to populate
					# the remaining values needed for Exchange to work.  We also set homeMDB to
					# specify the mail store.
					
					# We need to specify which storage group and mail store are used and Exchange server
					# We extract the first character of the SamAccountName and make decision based on it
					Switch -regex ($TargetAccount.SamAccountName.ToLower().Substring(0,1)) {
						"[a-g]" {
							Add-Content $LogFile "INFO - Setting Home Server: /o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX01"
							Add-Content $LogFile "INFO - Matched User to Store: CN=UserAG,CN=SG2-Users A-G Mailstore,CN=InformationStore,CN=ADEEX01,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu"
							#All three Exchange attributs are set in a single command to try and address timing issues
							Set-QADUser $TargetAccount -ObjectAttributes @{
								mailNickname=($TargetAccount.SamAccountName);
								msExchHomeServerName='/o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX01';
								homeMDB='CN=UserAG,CN=SG2-Users A-G Mailstore,CN=InformationStore,CN=ADEEX01,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu'
								}
						}
						"[h-m]" {
							Add-Content $LogFile "INFO - Setting Home Server: /o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02"
							Add-Content $LogFile "INFO - Matched User to Store: CN=UserHM,CN=SG1-Users H-M Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu"
							#All three Exchange attributs are set in a single command to try and address timing issues
							Set-QADUser $TargetAccount -ObjectAttributes @{
								mailNickname=($TargetAccount.SamAccountName);
								msExchHomeServerName='/o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02';
								homeMDB='CN=UserHM,CN=SG1-Users H-M Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu'
								}
						}
						"[n-z]" {
							Add-Content $LogFile "INFO - Setting Home Server: /o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02"
							Add-Content $LogFile "INFO - Matched User to Store: CN=UserNZ,CN=SG2-Users N-Z Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu"
							#All three Exchange attributs are set in a single command to try and address timing issues
							Set-QADUser $TargetAccount -ObjectAttributes @{
								mailNickname=($TargetAccount.SamAccountName);
								msExchHomeServerName='/o=UMUC/ou=US Administrative Group/cn=Configuration/cn=Servers/cn=ADEEX02';
								homeMDB='CN=UserNZ,CN=SG2-Users N-Z Mailstore,CN=InformationStore,CN=ADEEX02,CN=Servers,CN=US Administrative Group,CN=Administrative Groups,CN=UMUC,CN=Microsoft Exchange,CN=Services,CN=Configuration,DC=origin,DC=umuc,DC=edu'
								}
						}
						default {Add-Content $LogFile "ERROR - Unable to Match User to Exchange Store."}
					}
				} Else {
					Add-Content $LogFile ("WARNING - Found Existing Value in Mail Attribute: " + $TargetAccount.mail)
					Add-Content $LogFile "WARNING - Will not Attempt to Exchange Enable Account"
				}
			} Else {
				Add-Content $LogFile "ERROR - Account Not Found: $TargetAccountUserID"
			}
		} Else {
			Add-Content $LogFile "INFO - Skipping Non-Faculty/Non-TA Account: $TargetAccountUserID"
		}
	}
}

Add-Content $LogFile ("INFO - End Date and Time: " + (Get-Date))

# Disconnect from Active Directory
Disconnect-QADService -Connection $ADConnection