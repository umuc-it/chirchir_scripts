open (newadfile, "NewADUsers.txt");

while (<newadfile>) {
#while (<>) {
	chop;
   ($lastname, $firstname, $userid, $ssn, $emplid, $bday, $email, $title, $empfac)  = split(/\,/);
   for( $lastname ){ s/^\s+//; s/\s+$//; }
   $lastname =~ s/(\w+)/\u\L$1/g;
   for( $firstname ){ s/^\s+//; s/\s+$//; }
   $firstname =~ s/(\w+)/\u\L$1/g;
   for( $email ){ s/^\s+//; s/\s+$//; }
   for( $emplid ){ s/^\s+//; s/\s+$//; }
   while (length($emplid) < 7) {
      $emplid = "0" . $emplid
   }
	$userid = lc($userid);
   for( $title ){ s/^\s+//; s/\s+$//; }

   $lastnames{$userid} = $lastname;
   $firstnames{$userid} = $firstname;
   #$ssns{$userid} = $ssn;
   $bdays{$userid} = $bday;
   $emails{$userid} = $email;
   $emplids{$userid} = $emplid;
   $titles{$userid} = $title;
   $empfacs{$userid} = $empfac;
}
close(newadfile);

open (exchangefile, "AccountOutputFile.csv");

while (<exchangefile>) {
	chop;
   ($userid, $exchangemail)  = split(/\,/);
   for( $exchangemail ){ s/^\s+//; s/\s+$//; }
	 $userid = lc($userid);

   $exchangemails{$userid} = $exchangemail;
}
close(exchangefile);

open (passfile, "Passwords.csv");
open (bothfile, ">USA_AD_Pending_Results.txt");
open (facfile, ">USA_AD_Faculty.txt");
open (tafile, ">USA_AD_TA.txt");

while (<passfile>) {
	chop;
	($userid, $password, $pwempfac)  = split(/,/);
   for( $userid ){ s/^\s+//; s/\s+$//; }
   for( $password ){ s/^\s+//; s/\s+$//; }

   print bothfile $lastnames{$userid}, "\|", $firstnames{$userid}, "\|", $userid, "\|", $password, "\|SSN\|", $emplids{$userid}, "\|", $bdays{$userid}, "\|", $emails{$userid}, "\|", $exchangemails{$userid}, "\|", $titles{$userid}, "\|", $empfacs{$userid}, "\n";
	if ($pwempfac eq "FAC") {
   	print facfile $lastnames{$userid}, "\|", $firstnames{$userid}, "\|", $userid, "\|", $password, "\|SSN\|", $emplids{$userid}, "\|", $bdays{$userid}, "\|", $emails{$userid}, "\|", $exchangemails{$userid}, "\|", $titles{$userid}, "\|", $empfacs{$userid}, "\n";
   }
	if ($pwempfac eq "TA") {
   	print tafile $lastnames{$userid}, "\|", $firstnames{$userid}, "\|", $userid, "\|", $password, "\|SSN\|", $emplids{$userid}, "\|", $bdays{$userid}, "\|", $emails{$userid}, "\|", $exchangemails{$userid}, "\|", $titles{$userid}, "\|", $empfacs{$userid}, "\n";
   }

}
close(passfile);
