Dim strOutputBuffer
Dim DomainArray (2)

DomainArray (0) = "dc=asia,dc=umuc,dc=edu"
DomainArray (1) = "dc=europe,dc=umuc,dc=edu"
DomainArray (2) = "dc=us,dc=umuc,dc=edu"

' objInputFileName = WScript.arguments(0)

strInputFileName = "US_AD_PENDING_120321.csv"
strInputFilePath = "Batch120321"

Set objFSO = CreateObject("Scripting.FileSystemObject")

Set objInputFile = objFSO.OpenTextFile("C:\Scratch\Provision\" & strInputFilePath & "\" & strInputFileName , 1, False)
Set objOutputFileNew = objFSO.CreateTextFile("C:\Scratch\Provision\" & strInputFilePath & "\NewADUsers.txt" , True, False)
Set objOutputFileExisting = objFSO.CreateTextFile("C:\Scratch\Provision\" & strInputFilePath & "\ExistingADUsers.txt" , True, False)

' strUserName = "ccastore"
dtStart = TimeValue(Now())

Set objConnection = CreateObject("ADODB.Connection")
objConnection.Open "Provider=ADsDSOObject;"
 
Set objCommand = CreateObject("ADODB.Command")
objCommand.ActiveConnection = objConnection
 
objCommand.Properties("Size Limit") = 64000
objCommand.Properties("Page Size") = 64000
objCommand.Properties("Cache Results") = False

While not objInputFile.atEndOfStream
	strOutputBuffer = ""
   strInputLine = objInputFile.ReadLine()
   InputLineArray = split(strInputLine, ",")
   strUserName = InputLineArray(2)
   strOutputBuffer = strInputLine

	blnUserFound = False
	For Each strDomain In DomainArray
		' WScript.Echo "Searching: " & strDomain
	
		objCommand.CommandText = _
		    "<GC://" & strDomain & ">;(&(objectCategory=User)" & _
		         "(samAccountName=" & strUserName & "));distinguishedName,displayname,samAccountName;subtree"
		  
		Set objRecordSet = objCommand.Execute
		 
		' WScript.Echo "BOF: " & objRecordSet.BOF
		' WScript.Echo "EOF: " & objRecordSet.EOF
		' WScript.Echo "Records Found: " & objRecordSet.RecordCount & vbCRLF
		'If objRecordSet.RecordCount = 0 Then
		If (objRecordSet.BOF = True And objRecordSet.EOF = True) Then
			' WScript.Echo "sAMAccountName: " & strUserName & " Not Found in " & strDomain & vbCRLF
		Else
			blnUserFound = True
			' WScript.Echo "sAMAccountName: " & strUserName & " Found in " & strDomain & vbCRLF
			While Not objRecordSet.EOF
				strOutputBuffer = strOutputBuffer & vbCRLF
				strOutputBuffer = strOutputBuffer & "Distinguished Name: " & objRecordSet.Fields("distinguishedName") & vbCRLF
				strOutputBuffer = strOutputBuffer & "Display Name: " & objRecordSet.Fields("displayname") & vbCRLF
				strOutputBuffer = strOutputBuffer & "SAM Account Name: " & objRecordSet.Fields("samAccountName") & vbCRLF & vbCRLF
				objRecordset.MoveNext
			Wend
		End If
		
		objRecordSet.Close
	Next
	If blnUserFound Then
		objOutputFileExisting.WriteLine strOutputBuffer
	Else
		objOutputFileNew.WriteLine strOutputBuffer
	End If
Wend

objConnection.Close

objInputFile.Close()
objOutputFileNew.Close()
objOutputFileExisting.Close()