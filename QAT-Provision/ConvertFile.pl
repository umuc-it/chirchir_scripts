open (sourcefile, ">AccountSourceFile.txt");

print sourcefile "Lastname\|Firstname\|WTID\|SSN\|EmplID\|Date\|Description\|Template User\n";

while (<>) {
   #($lastname, $firstname, $wtid, $ssn, $psid, $email, $title) = split(/\|/);
   chop;
   ($lastname, $firstname, $wtid, $ssn, $psid, $dob, $email, $title, $empfac) = split(/\,/);
   for( $lastname ){ s/^\s+//; s/\s+$//; }
   $lastname =~ s/(\w+)/\u\L$1/g;
   for( $firstname ){ s/^\s+//; s/\s+$//; }
   $firstname =~ s/(\w+)/\u\L$1/g;
   $wtid =~ s/\s+//g;
   $ssn =~ s/\s+//g;
   $psid =~ s/\s+//g;
   for( $title ){ s/^\s+//; s/\s+$//; }
   print sourcefile "$lastname\|$firstname\|$wtid\|SSN\|$psid\|BirthDay\|$email\|$title\|$empfac|\n";
}

close (sourcefile);