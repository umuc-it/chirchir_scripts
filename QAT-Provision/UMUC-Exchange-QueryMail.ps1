# Variable Declarations
$ADDomainController = "adedcns01.us.umuc.edu"
$ADPrivilegedUserAccount = "US\mchirchir_eu"
$LogFile = "C:\Scratch\Provision\UMUC-Exchange-QueryMail.log"
$SourceFile = "C:\Scratch\Provision\AccountSourceFile.txt"
$OutputFile = "C:\Scratch\Provision\AccountOutputFile.csv"
$MaxSearchAttempts = 10
$DelayBetweenSearches = 60

Set-Content $LogFile (Get-Date)
$null | Set-Content $OutputFile

# Check for Quest
If ((get-pssnapin -Name Quest.ActiveRoles.ADManagement -ErrorAction SilentlyContinue) -eq $null) {add-pssnapin Quest.ActiveRoles.ADManagement}

# Establish Active Directory Connection
$ADCredentials = Get-Credential $ADPrivilegedUserAccount
$ADConnection = Connect-QADService -Service $ADDomainController -Credential $ADCredentials

# Read list of accounts from input file
$SourceList = Get-Content $SourceFile

# Build the Hash Table and Seed the Search Array with list of Valid User IDs

$UserList = New-Object System.Collections.ArrayList
$UserHash = @{}

ForEach ($SourceLine in $SourceList) {
	If (-not $SourceLine.Startswith("Lastname")) {
		$SourceElements = $SourceLine.Split('`|')
		$TargetAccountUserID = $SourceElements[2].ToLower()
		#Search for Account by SamAccountName and Proceed if Found
		Add-Content $LogFile "Checking for User: $TargetAccountUserID"
		$TargetAccount = Get-QADUser -SamAccountName $TargetAccountUserID
		If ($TargetAccount -ne $null) {
			Add-Content $LogFile ("Account Found: " + $TargetAccount.DN)
			$UserList.Add($TargetAccountUserID) | out-null
			Add-Content $LogFile ("Adding Account to Array, Current Array Size: " + $UserList.Count)
			$UserHash.Add($TargetAccountUserID, "NoMailAttribute")
			Add-Content $LogFile ("Adding Account to Hash, Current Hash Size: " + $UserHash.Count)
		} Else {
			Add-Content $LogFile "ERROR - Account Not Found: $TargetAccountUserID"
			$UserHash.Add($TargetAccountUserID, "NoActiveDirectoryAccount")
			Add-Content $LogFile ("Adding Account to Hash, Current Hash Size: " + $UserHash.Count)
		}
	}
}

$CurrentSearchAttempt = 1
Add-Content $LogFile ("We found " + $UserList.Count + " valid accounts in our input file.")
Write-Host "We found " $UserList.Count " valid accounts in our input file."
If ($UserList.Count -gt 0) {
	Add-Content $LogFile "Beginning Search for Email Addresses."
	Write-Host "Beginning Search for Email Addresses."
} Else {
	Add-Content $LogFile "Nothing to do."
	Write-Host "Nothing to do."
}

While (($CurrentSearchAttempt -le $MaxSearchAttempts) -and ($UserList.Count -gt 0)) {
	Add-Content $LogFile "Attempt $CurrentSearchAttempt of $MaxSearchAttempts"
	Write-Host "Attempt " $CurrentSearchAttempt "of" $MaxSearchAttempts
	$FoundUsers = New-Object System.Collections.ArrayList
	ForEach ($User in $UserList) {
		# Create Holding Area for Mail Enabled Accounts We Find
		# We do this to later remove them from our search array
		# Hopefully this will improve search times for large lists
		Add-Content $LogFile "Checking for Mail Attribute for User: $User"
		$UserQueryResults = Get-QADUser -SamAccountName $User -IncludedProperties Mail
		If ($UserQueryResults.Mail -ne $null) {
			Add-Content $LogFile ("Mail Attribute Exists: " + $UserQueryResults.Mail)
			Write-Host ("Found Mail Attribute: $User - " + $UserQueryResults.Mail)
			Add-Content $LogFile ("Updating Hash - Name: $User Value: " + $UserQueryResults.Mail.ToLower())
			$UserHash.Set_Item($User, $UserQueryResults.Mail.ToLower())
			# Store Account for later removal from search array
			# We can't remove entries from search array while we are using it in ForEach loop
			$FoundUsers.Add($User) | out-null
			Add-Content $LogFile ("Adding Account to Found Users Array, Current Array Size: " + $FoundUsers.Count)
		}
	}
	ForEach ($FoundUser in $FoundUsers) {
		$UserList.Remove($FoundUser)
		Add-Content $LogFile ("Removing Account from Array, Current Array Size: " + $UserList.Count)
	}
	$CurrentSearchAttempt += 1
	Add-Content $LogFile ("Remaining Entries in Array: " + $UserList.Count)
	Write-Host "Users Remaining in Search Array: " $UserList.Count
	If (($CurrentSearchAttempt -le $MaxSearchAttempts) -and ($UserList.Count -gt 0)) {
		Add-Content $LogFile "Pausing $DelayBetweenSearches Seconds Before Beginning Next Search Attempt."
		Write-Host "Pausing $DelayBetweenSearches Seconds Before Beginning Next Search Attempt."
		Start-Sleep $DelayBetweenSearches
	}
}

ForEach ($User in $UserHash.Keys) {
	Add-Content $OutputFile ($User + "," + $UserHash.Get_Item($User))
}

# Disconnect from Active Directory
Disconnect-QADService -Connection $ADConnection
