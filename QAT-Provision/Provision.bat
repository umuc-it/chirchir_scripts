set mm=%date:~4,2%
set dd=%date:~7,2%
set yy=%date:~12,2%
mkdir Batch%yy%%mm%%dd%
copy Search-SAM-AllDomains.vbs C:\Scratch\Provision\Batch%yy%%mm%%dd%
pause
cd C:\Scratch\Provision\Batch%yy%%mm%%dd%
cscript Search-SAM-AllDomains.vbs
copy NewADUsers.txt C:\Scratch\Provision
cd ..
cat NewADUsers.txt | Perl  ConvertFile.pl
perl CreateADAccount.pl
perl CreatePasswordCSV.pl
Ldifde -s adedcns01.us.umuc.edu  -i -f PrimaryADImport.ldif
Ldifde -s adedcns01.us.umuc.edu -i -f SecondaryADImport.ldif
cscript AD-SetPass-EMPFAC.vbs Passwords.csv
Pause
perl MassMail.pl
pause
move USA_AD_Faculty.txt C:\Scratch\Provision\Batch%yy%%mm%%dd%
move USA_AD_Faculty.zip C:\Scratch\Provision\Batch%yy%%mm%%dd%
move ProvisionBatch111024.zip C:\Scratch\Provision\Batch%yy%%mm%%dd%
move USA_AD_Pending_Results.txt C:\Scratch\Provision\Batch%yy%%mm%%dd%
move USA_AD_TA.txt C:\Scratch\Provision\Batch%yy%%mm%%dd%
move AccountOutputFile.csv C:\Scratch\Provision\Batch%yy%%mm%%dd%
move UMUC-Exchange-MailEnableAccount.log C:\Scratch\Provision\Batch%yy%%mm%%dd%
move UMUC-Exchange-QueryMail.log C:\Scratch\Provision\Batch%yy%%mm%%dd%
move AccountSourceFile.txt C:\Scratch\Provision\Batch%yy%%mm%%dd%
move MembershipTemplates.txt C:\Scratch\Provision\Batch%yy%%mm%%dd%
move NewADUsers.txt C:\Scratch\Provision\Batch%yy%%mm%%dd%
move Passwords.csv C:\Scratch\Provision\Batch%yy%%mm%%dd%
move PrimaryADImport.ldif C:\Scratch\Provision\Batch%yy%%mm%%dd%
move SecondaryADImport.ldif C:\Scratch\Provision\Batch%yy%%mm%%dd%
move USA_AD_Faculty.txt C:\Scratch\Provision\Batch%yy%%mm%%dd%

xcopy Batch%yy%%mm%%dd% "G:\Account Creation\Completed Batches\"

%yy%%mm%%dd%