if %1. == . Goto Usage
if %2. == . Goto Usage
logparser -i:iisw3c "select date, time, c-ip, cs-username, cs-method, sc-status, cs-uri-stem, cs-uri-query, cs(user-agent) into c:\scratch\lp-output.csv from \\adeexbe01.us.umuc.edu\c$\windows\system32\logfiles\w3svc100\ex*.log,\\adeexbe02.us.umuc.edu\c$\windows\system32\logfiles\w3svc101\ex*.log where c-ip not in ('131.171.34.220';'131.171.34.221') and to_timestamp(date,time) between timestamp('%1 00:00:00', 'yyyy/MM/dd hh:mm:ss') and timestamp('%2 23:59:59', 'yyyy/MM/dd hh:mm:ss') order by date, time"
Goto End
:Usage
Echo This script requires a start and end date as parameters in the format of YYYY/MM/DD
:End