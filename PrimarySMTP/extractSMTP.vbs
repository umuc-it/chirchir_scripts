'================================================================================================== 
' 
' VBScript Source File 
' 
' NAME: LISTPROXYADDRESSES.VBS 
' VERSION: 0.9.1 
' AUTHOR: Bharat Suneja , Nick Phipps 
' Hacked by Tony Tibbenham to add rudimentary error handling 
' CREATE DATE : 05/06/2004 
' LAST MODIFIED : 19/04/2011 
'================================================================================================== 
' COMMENT: 
' 
'==================================================================================================

'Set up constant for deleting values from multivalued attribute memberOf

option explicit 

on error resume next

Const ADS_PROPERTY_NOT_FOUND = &h8000500D 
Const ADS_UF_ACCOUNTDISABLE = 2 'For UserAccountControl 
Const strX400Search = "X400"

dim strDomain 
dim strADPath 
dim objDomain 
dim strOutput 
dim boolTestMode 
dim objRootDSE 
dim objConnection 
dim objCommand 
dim objRecordSet 
dim strResult 
dim AddressCount 
dim strUserDN 
dim objUser 
dim ArrProxyAddresses 
dim proxyAddress

'______________________________________________________

'### Change the following line to change the output file path and name 
strOutput = "c:\scratch\email_addresses.txt"

'### Change the following line to "True" to change output to a dialog instead of a TXT file 
boolTestMode = True

'Set RootDSE 
Set objRootDSE = GetObject("LDAP://rootDSE") 
strDomain = objRootDSE.Get("defaultNamingContext") 
strADPath = "LDAP://" & strDomain 
wscript.Echo strADPath 
Set objDomain = GetObject(strADPath) 
wscript.echo "objDomain: " & objDomain.distinguishedName

'Setup ADODB connection 
Set objConnection = CreateObject("ADODB.Connection") 
objConnection.Open "Provider=ADsDSOObject;" 
Set objCommand = CreateObject("ADODB.Command") 
objCommand.ActiveConnection = objConnection

'Execute search command to look for Contacts & Groups 
objCommand.CommandText = _ 
"<" & strADPath & ">" & ";(&(|(objectClass=contact)(objectClass=group))(mail=*))" & ";distinguishedName,displayName,mail,proxyAddresses;subtree"

'Execute search to get Recordset 
Set objRecordSet = objCommand.Execute 


'Start procedure 

strResult = strResult & VbCrLf & "Domain: " & strDomain

strResult = strResult & VbCrlf & "#Total Records Found (other accounts): " & objRecordSet.RecordCount & VbCrlf 
AddressCount = 0

While Not objRecordSet.EOF 'Iterate through the search results 

strUserDN = objRecordSet.Fields("distinguishedName") 'Get User's distinguished name from Recordset into a string 
wscript.Echo "strUserDN: "& strUserDN 

set objUser= GetObject("LDAP://"& strUserDN & "") 'Use string to bind to user object 
If Err.Number <> 0 Then 
Wscript.Echo Computer & " " & Err.Description 
Err.Clear 
Else 
strResult = strResult & VbCrlf & "cn: " & objUser.cn 
strResult = strResult & VbCrlf & "mail: " & objUser.mail 
arrProxyAddresses = objRecordSet.Fields("proxyAddresses") 
If IsArray(objRecordSet.Fields("proxyAddresses")) Then 
strResult = strResult & VbCrLf & "Proxy Addresses" 

For Each ProxyAddress in arrProxyAddresses 

'Sub: Check X400 
If InStr(ProxyAddress, strX400Search) <> 0 Then 
'Wscript.Echo "#This was an x400" 
Else 
strResult = strResult & VbCrlf & proxyAddress 
End If 'Ends loop for X400 address 
Next

Else 
strResult = strResult & VbCrlf & "#Object does not have proxy addresses" 
End If 
strResult = strResult & VbCrLf 
end if 
objRecordSet.MoveNext 
Wend

'************************************* 
'Begin second query for users 
varDisabledCounter = 0 

'Execute search command to look for user 
objCommand.CommandText = _ 
"<" & strADPath & ">" & ";(&(objectClass=user)(mail=*))" & ";distinguishedName,displayName,mail,proxyAddresses;subtree"

'Execute search to get Recordset 
Set objRecordSet = objCommand.Execute 

strResult = strResult & vbCrlf & "#Users" 
strResult = strResult & VbCrlf & "#Total Records Found (users): " & objRecordSet.RecordCount & VbCrlf 

' Wscript.Echo strResult

While Not objRecordSet.EOF 'Iterate through the search results 
strUserDN = objRecordSet.Fields("distinguishedName") 'Get User's distinguished name from Recordset into a string 
set objUser= GetObject("LDAP://"& strUserDN & "") 'Use string to bind to user object 
If Err.Number <> 0 Then 
Wscript.Echo Computer & " " & err.number & " " & Err.Description 
Err.Clear 
Else 

If objUser.AccountDisabled = TRUE Then 'If User account disabled, then skip proxy address enum 
varDisabledCounter = varDisabledCounter + 1 
strResult2 = strResult2 & VbCrLf & varDisabledCounter & " " & objUser.displayName & VbCrLf 

strResult2 = strResult2 & "cn: " & objUser.cn 
strResult2 = strResult2 & VbCrlf & "mail: " & objUser.mail 
arrProxyAddresses = objRecordSet.Fields("proxyAddresses") 
If IsArray(objRecordSet.Fields("proxyAddresses")) Then 
strResult2 = strResult2 & VbCrLf & "Proxy Addresses" 


For Each ProxyAddress in arrProxyAddresses 
'Sub: Check X400 
If InStr(ProxyAddress, strX400Search) <> 0 Then 
'Wscript.Echo "#This was an x400" 
Else 
strResult2 = strResult2 & VbCrlf & proxyAddress 
AddressCount = AddressCount + 1 
End If 'Ends loop for X400 address 
Next 
Else 
strResult2 = strResult2 & VbCrLf & "#Object does not have proxy addresses" 
End If 
strResult2 = strResult2 & VbCrLf 





Else 


strResult = strResult & VbCrlf & "cn: " & objUser.cn 
strResult = strResult & VbCrlf & "mail: " & objUser.mail 
arrProxyAddresses = objRecordSet.Fields("proxyAddresses") 
If IsArray(objRecordSet.Fields("proxyAddresses")) Then 
strResult = strResult & VbCrLf & "Proxy Addresses" 

For Each ProxyAddress in arrProxyAddresses 
'Sub: Check X400 
If InStr(ProxyAddress, strX400Search) <> 0 Then 
'Wscript.Echo "#This was an x400" 
Else 
strResult = strResult & VbCrlf & proxyAddress 
AddressCount = AddressCount + 1 
End If 'Ends loop for X400 address 
Next 
Else 
strResult = strResult & VbCrLf & "#Object does not have proxy addresses" 
End If 
strResult = strResult & VbCrLf 

End If 'End check for disabled user 
end if ' error 
objRecordSet.MoveNext 
Wend 'End second query for users


strResult = "Users, Groups & Contacts" & VbCrLf & "-------------------------" & VbCrLf & strResult 
strResult = strResult & VbCrLf & "Disabled Users" & VbCrLf & "-------------------------" & VbCrLf & strResult2

'Output results 
' if boolTestMode then 
'WScript.echo "strResult contains:" 
'WScript.Echo strResult 
'wscript.echo "strResult2 contains:" 
'wscript.echo strResult2 
'wscript.echo "End of strResults" 

' else 
'Output to a text file 
Set objFileSystem = CreateObject("Scripting.FileSystemObject") 
Set objOutputFile = objFileSystem.CreateTextFile(strOutput) 
objOutputFile.Write strResult 
objoutputfile.close 
' end if

WScript.Echo "Done."

