'The following lines are commented for use in VBScript
Dim oConnection 'As ADODB.Connection
Dim oRecordset 'As ADODB.Recordset
Dim strQuery 'As String
Dim strUPN 'As String
Dim oCont 'As IADsContainer
Dim oGC 'As IADs
Dim strADsPath 'As String

'TO DO : Change the UPN to fit your environment
'strMail = "jennifer.thompson@umuc.edu"

'Find the Global Catalog server
Set oCont = GetObject("GC:")
For Each oGC In oCont
  strADsPath = oGC.ADsPath
Next

Set oConnection = CreateObject("ADODB.Connection")
Set oRecordset = CreateObject("ADODB.Recordset")
oConnection.Provider = "ADsDSOObject"  'The ADSI OLE-DB provider

oConnection.Open "ADs Provider"
strQuery = "<" & strADsPath & ">;(&(objectClass=user)(objectCategory=person));mail,samAccountName,cn,distinguishedName;subtree"
Set oRecordset = oConnection.Execute(strQuery)
For Each obj In oRecordset
     WScript.Echo oRecordset.Fields("mail")&"," & oRecordset.Fields("samAccountName") &","& oRecordset.Fields("distinguishedName")
     oRecordset.MoveNext
next
Set oCont = Nothing
Set oGC = Nothing
Set oRecordset = Nothing
Set oConnection = Nothing
				