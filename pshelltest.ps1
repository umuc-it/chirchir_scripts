﻿#Load the assemblies
[System.Reflection.Assembly]::LoadWithPartialName("System.DirectoryServices.Protocols")
[System.Reflection.Assembly]::LoadWithPartialName("System.Net")


#Connects to myopenldap.mikesblog.lan using SSL on a non-standard port
$ldap = New-Object System.DirectoryServices.Protocols.LdapConnection "ldapmaster.umuc.edu:636"
          
#Set session options
$ldap.SessionOptions.SecureSocketLayer = $true;
           
# Pick Authentication type:
# Anonymous, Basic, Digest, DPA (Distributed Password Authentication),
# External, Kerberos, Msn, Negotiate, Ntlm, Sicily
$ldap.AuthType = [System.DirectoryServices.Protocols.AuthType]::Basic
           
# Gets username and password.
$user = Read-Host -Prompt "Username"
$pass = Read-Host -AsSecureString "Password"

$credentials = new-object "System.Net.NetworkCredential" -ArgumentList $user,$pass

# Bind with the network credentials. Depending on the type of server,
# the username will take different forms. Authentication type is controlled
# above with the AuthType
$ldap.Bind($credentials);

$basedn = "ou=people,dc=umuc,dc=edu"
$filter = "(uid=mchirchir)"
$scope = [System.DirectoryServices.Protocols.SearchScope]::Subtree
$attrlist = ,"*"

$r = New-Object System.DirectoryServices.Protocols.SearchRequest -ArgumentList `
                $basedn,$filter,$scope,$attrlist

#$re is a System.DirectoryServices.Protocols.SearchResponse
$re = $ldap.SendRequest($r);

#How many results do we have?
write-host $re.Entries.Count

foreach ($i in $re.Entries)
{
   #Do something with each entry here, such as read attributes
} 